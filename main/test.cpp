#include "../audio/jackclient.h"
#include "../files/audio.h"
#include "../input/input.h"
#include "../util/args.h"
#include "../util/string.h"

#include <signal.h>

static void inputTest() {
  while (true) {
    readMouseInputData();
    readKeyboardInputData();
    readJoystickInputData();

    if (input[INPUT_MOUSE].changed) {
      // printAllData( input[ INPUT_MOUSE ] );

      if (mouseEvents.x | mouseEvents.y)
        printf("mouse : %d %d\n", mouseEvents.x, mouseEvents.y);

      if (mouseEvents.btn & MOUSE_MIDDLE)
        printf("Middle mouse\n");

      if ((mouseEvents.btn & MOUSE_LEFT) && (mouseEvents.btn & MOUSE_RIGHT))
        printf("Middle mouse (on laptop)\n");
      else if (mouseEvents.btn & MOUSE_LEFT)
        printf("Left Button\n");
      else if (mouseEvents.btn & MOUSE_RIGHT)
        printf("Right Button\n");
    }

    if (input[INPUT_KEYBOARD].changed) {
      printAllData(input[INPUT_KEYBOARD]);

      for (uint8_t btn = keyboardEvents.iter(0); btn != 0;
           btn = keyboardEvents.iter(btn)) {
        if (btn == KEY_ESC) {
          printf("Pressed the escape key\n");
          printf("Exitting input test...\n\n");
          return;
        }

        printf("key pressed : %d\n", btn);
      }
    }

    if (input[INPUT_JOYSTICK].changed) {
      // printAllData( input[ INPUT_JOYSTICK ] );

      if (joystickEvents.analog)
        printf("analog axis %d : %d\n", joystickEvents.btn,
               joystickEvents.analog_signal);
      if (joystickEvents.pressed)
        printf("joy button : %d\n", joystickEvents.btn);
    }
  }
}

void audioTest(const char *path) {
  printf("Loading file '%s'\n", path);
  SoundRAW sound(44100, 16, 2);
  openRAWAudioFile("sounds/stereo.wav");
  openRAWFile(&sound, path);
  if (sound.num_samples == 0) {
    printf("Couldn't load sound '%s'\n", path);
    return;
  }
  char *path2 = (char *)malloc(strlen(path) + 5);
  memcpy(path2, path, strlen(path));
  strcat(path2, ".wav");
  printAudioSpecs(sound);
  sound.save(path2);
  openRAWAudioFile(path2);
}

int main(int argc, char *argv[]) {
  ArgcHandler(argc == 2, argc, argv, "[input/audio]", NULL);

  if (strEqual(argv[1], "input")) {
    printf("\nThis program only reads specific input devices data\n");
    printf(
        "If several input devices are available, you should choose the correct "
        "ones\n\n");

    initKeyboardInput();
    initMouseInput();
    initJoystickInput();

    printf("Finished initialization\n");

    inputTest();

    shutdownInputs();
  } else if (strEqual(argv[1], "audio")) {
    audioTest("2019-10-19_22:15:42.bmp");
  } else {
    printf("Unrecognized option '%s', use '-h' to display help\n", argv[1]);
    return 1;
  }

  return 0;
}