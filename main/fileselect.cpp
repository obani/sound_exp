#include "../input/input.h"

#include "../files/walker.h"

#include "../util/args.h"
#include "../util/string.h"

#include <fcntl.h>
#include <stdio.h>
#include <time.h>

static int fd;

void fileselector(const char *file, const char *suffix) {
  const char *NEWLINE = "\n";

  FileWalker walker;
  String file_path = "";
  int fd;

  String p = getCurrentDirectory();
  walker.setPath(p.str());
  walker.suffix = suffix;

  if ((fd = open(file, O_CREAT | O_APPEND | O_WRONLY, 0644)) == -1) {
    printf("An error occured when creating the file\n");
    return;
  }

  struct timespec time_sleep = {0, 10000000};
  walker.getFilesList();

  readKeyboardInputData();
  keyboardEvents.clear();

  while (true) {
    readKeyboardInputData();

    if (keyboardEvents.pressed(KEY_ESC)) {
      return;
    }

    walker.select(
        keyboardEvents.pressed(KEY_LEFT), keyboardEvents.pressed(KEY_RIGHT),
        keyboardEvents.pressed(KEY_UP), keyboardEvents.pressed(KEY_DOWN),
        keyboardEvents.pressed(KEY_ENTER), &file_path);

    keyboardEvents.clear();

    if (file_path.size() != 0) {
      printf("%s%sAdding file [%s%s%s]\n", ANSI_MOVE_UP_DELETE,
             ANSI_MOVE_UP_DELETE, ANSI_BOLD_INTENSE_GREEN, file_path.str(),
             ANSI_RESET);

      file_path += NEWLINE;

      if (write(fd, file_path.str(), file_path.size()) == -1) {
        printf("Error on writing %s\n", file_path.str());
        return;
      }

      file_path = "";
    }

    nanosleep(&time_sleep, &time_sleep);
  }
}

int main(int argc, char *argv[]) {
  ArgcHandler(argc > 1 && argc < 4, argc, argv, "<outfile> [suffix]", NULL);

  initKeyboardInput();

  const char *file = argv[1];
  const char *suffix = argc == 3 ? argv[2] : "";
  printf("Starting file selector on file %s with search suffix %s\n", file,
         suffix);
  fileselector(file, suffix);

  printf("Exitting...\n");

  const char *ZERO = "\0";
  if (fd != -1) {
    write(fd, ZERO, 1);
  }

  close(fd);
  shutdownInputs();

  return 0;
}