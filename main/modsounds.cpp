#include "../files/audio.h"
#include "../util/args.h"
#include "../util/string.h"

#include <stdlib.h>

template <typename T>
static void norm_aux(T *array, uint64_t samples, int64_t low_limit,
                     int64_t high_limit) {
  int32_t max = 0;
  for (uint64_t i = 0; i < samples; i++) {
    if (abs(array[i]) > max)
      max = array[i];
  }

  if (max == 0)
    return;

  float factor = max > 0 ? high_limit / float(max) : low_limit / float(max);
  for (uint64_t i = 0; i < samples; i++) {
    array[i] *= factor;
  }
}

static void normalize(SoundRAW *s) {
  uint64_t samples = s->num_samples * s->channels;
  switch (s->bits_per_sample) {
  case 8:
    norm_aux(s->i8, samples, s->low_limit, s->high_limit);
    break;
  case 16:
    norm_aux(s->i16, samples, s->low_limit, s->high_limit);
    break;
  default:
    norm_aux(s->i32, samples, s->low_limit, s->high_limit);
    break;
  }
}

static int sortcmp(const void *a, const void *b) {
  return abs(*(int *)a) - abs(*(int *)b);
}

static void my_sort(SoundRAW *s) {
  uint64_t samples = s->num_samples * s->channels;
  switch (s->bits_per_sample) {
  case 8:
    qsort(s->i8, samples, sizeof(int8_t), sortcmp);
    break;
  case 16:
    qsort(s->i16, samples, sizeof(int16_t), sortcmp);
    break;
  default:
    qsort(s->i32, samples, sizeof(int32_t), sortcmp);
    break;
  }
}

template <typename T>
static void invert_aux(T *array, uint64_t samples, int64_t low_limit,
                       int64_t high_limit) {
  for (uint64_t i = 0; i < samples; i++) {
    array[i] = -array[i] + (array[i] < 0 ? low_limit : high_limit);
  }
}

static void invert(SoundRAW *s) {
  uint64_t samples = s->num_samples * s->channels;
  switch (s->bits_per_sample) {
  case 8:
    invert_aux(s->i8, samples, s->low_limit, s->high_limit);
    break;
  case 16:
    invert_aux(s->i16, samples, s->low_limit, s->high_limit);
    break;
  default:
    invert_aux(s->i32, samples, s->low_limit, s->high_limit);
    break;
  }
}

template <typename T>
static void mod_aux(T *array, uint64_t samples, int64_t low, int64_t high) {
  for (uint64_t i = 0; i < samples; i++) {
    array[i] = array[i] < 0 ? -(-array[i] % low) : array[i] % high;
  }
}

static void modulo(SoundRAW *s, float percent) {
  uint64_t samples = s->num_samples * s->channels;
  uint64_t low = -s->low_limit * percent;
  uint64_t high = s->high_limit * percent;
  switch (s->bits_per_sample) {
  case 8:
    mod_aux(s->i8, samples, low, high);
    break;
  case 16:
    mod_aux(s->i16, samples, low, high);
    break;
  default:
    mod_aux(s->i32, samples, low, high);
    break;
  }
}

static bool below_comp(int32_t sample, int32_t low, int32_t high) {
  return sample > low && sample < high;
}
static bool above_comp(int32_t sample, int32_t low, int32_t high) {
  return sample < low || sample > high;
}

template <typename T>
static void cut_aux(T *array, uint64_t samples, int64_t low, int64_t high,
                    bool (*f)(int32_t, int32_t, int32_t)) {
  for (uint64_t i = 0; i < samples; i++) {
    if (f(array[i], low, high))
      array[i] = 0;
  }
}

static void cut(SoundRAW *s, float percent,
                bool (*f)(int32_t, int32_t, int32_t)) {
  uint64_t samples = s->num_samples * s->channels;
  uint64_t low = s->low_limit * percent;
  uint64_t high = s->high_limit * percent;
  switch (s->bits_per_sample) {
  case 8:
    cut_aux(s->i8, samples, low, high, f);
    break;
  case 16:
    cut_aux(s->i16, samples, low, high, f);
    break;
  default:
    cut_aux(s->i32, samples, low, high, f);
    break;
  }
}

enum Opt : int {
  Opt_Normalize,
  Opt_Sort,
  Opt_Invert,
  Opt_Modulo,
  Opt_CutBelow,
  Opt_CutAbove,

  Opt_Count,
};

int main(int argc, char *argv[]) {
  int opt[Opt_Count];
  ArgcHelpOption options[] = {
      {"-n", "--normalize", "Normalize the sound"},
      {"-s", "--sort", "Sort by the absolute value of samples"},
      {"-i", "--invert", "Invert the value of each sample"},
      {"-m", "--modulo",
       "Modulo the samples above a certain value (0 to 1 param)"},
      {"-cb", "--cut-below",
       "Cut samples below a certain value (0 to 1 param)"},
      {"-ca", "--cut-above",
       "Cut samples above a certain value (0 to 1 param)"},
      {}};

  ArgcHandler(argc > 2, argc, argv, "<options> [files]", options);

  int acc = 0;
  int max = 0;
  for (int i = 0; i < Opt_Count; i++) {
    opt[i] =
        HasArg(argc, argv, options[i].opt, options[i].altopt, i >= Opt_Modulo);
    acc += opt[i];
    max = Max(max, opt[i]);
  }

  if (acc == 0) {
    printf("If this program doesn't have options, it does nothing other than "
           "duplicating sounds. Maybe that's what you want, but it's stupid "
           "then\n");
    return 1;
  }

  for (int i = max + 1; i < argc; i++) {
    const char *path = argv[i];

    printf("Loading file '%s'\n", path);
    SoundRAW sound = openRAWAudioFile(path);

    if (sound.num_samples == 0) {
      printf("Couldn't load sound '%s'\n", path);
      continue;
    }

    String path2 = path;
    path2 += ".wav";

    if (opt[Opt_Normalize])
      normalize(&sound);
    if (opt[Opt_Sort])
      my_sort(&sound);
    if (opt[Opt_Invert])
      invert(&sound);
    if (opt[Opt_Modulo])
      modulo(&sound, atof(argv[opt[Opt_Modulo]]));
    if (opt[Opt_CutBelow])
      cut(&sound, atof(argv[opt[Opt_CutBelow]]), below_comp);
    if (opt[Opt_CutAbove])
      cut(&sound, atof(argv[opt[Opt_CutAbove]]), above_comp);

    sound.save(path2.str());
    printf("Saved sound %s\n", path2.str());
  }

  return 0;
}