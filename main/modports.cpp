#include "../audio/jackclient.h"
#include "../midi/midi.h"

static uint8_t controller_idx = 0;

static JackClient client("modports");
static Slice<JackPort> in(client.initJackPorts(4, JackPortType_Audio,
                                               JackPortIsInput));
static Slice<JackPort> out(client.initJackPorts(2, JackPortType_Audio,
                                                JackPortIsOutput));
static MidiInput midi(client.initJackPorts(1, JackPortType_Midi,
                                           JackPortIsInput)[0]);

static int modports(jack_nframes_t nframes, void *arg) {
  JackClient *c = (JackClient *)arg;
  c->update(nframes, true);

  if (controller_idx == 0) {
    controller_idx = midi.learnIndex(MidiSignal_Controller);
    if (controller_idx != 0) {
      printf("Controller Index is %d\n", controller_idx);
    }
    return 0;
  }

  midi.update();

  for (jack_nframes_t i = 0; i < nframes; i++) {
    out[0][i] =
        in[0][i] * (midi.controllers[controller_idx] -
                    (in[2][i] - in[2][i] * midi.controllers[controller_idx]));
    out[1][i] =
        in[1][i] * (midi.controllers[controller_idx] -
                    (in[3][i] - in[3][i] * midi.controllers[controller_idx]));
  }

  return 0;
}

int main(int argc, char *argv[]) {
  client.start(modports);
  sleepProcess(NULL);

  return 0;
}