#include "../audio/jackclient.h"
#include "../input/input.h"
#include "../input/linkedinput.h"
#include "../util/math.h"
#include "../util/random.h"
#include "../util/vector.h"

static uint8_t CHANNELS = 1;
static JackClient client("sample_randomizer");
static Slice<JackPort> outputs(client.initJackPorts(CHANNELS,
                                                    JackPortType_Audio,
                                                    JackPortIsOutput));
static Slice<JackPort> inputs(client.initJackPorts(CHANNELS, JackPortType_Audio,
                                                   JackPortIsInput));
static float sample_rate = client.getSampleRate();

static jack_nframes_t sample_size = 2;
static bool randomize_channels = true;
static vector<bool> changed;

static const int SUB1 = -1;
static const int PLUS1 = +1;

static void size_change(const int &v) {
  sample_size += v;

  if (sample_size < 2)
    sample_size = 2;

  printf("Sample size : %d\n", sample_size);
}

static LinkedValue<const int, size_change> decrease(KEY_PAGEDOWN, SUB1);
static LinkedValue<const int, size_change> increase(KEY_PAGEUP, PLUS1);
static LinkedValue<bool, LinkedFuncs::boolSwitch>
    randChannels(KEY_INSERT, randomize_channels);

static int process(jack_nframes_t nframes, void *arg) {
  JackClient *c = (JackClient *)arg;
  c->update(nframes, true);

  changed.set_size(CHANNELS * nframes);
  for (size_t i = 0; i < changed.size(); i++)
    changed[i] = false;

  if (sample_size > nframes)
    sample_size = nframes;

  readKeyboardInputData();
  increase.checkPressedClean(keyboardEvents);
  decrease.checkPressedClean(keyboardEvents);
  randChannels.checkPressedClean(keyboardEvents);

  jack_nframes_t i = 0;
  // if( randomize_channels ) {
  for (size_t chan = 0; chan < outputs.size(); chan++) {
    i = 0;
    while (i < nframes) {
      jack_nframes_t r = (i + randomized.Int(sample_size));
      outputs[chan][i] =
          r >= nframes ? inputs[chan][nframes - 1] : inputs[chan][r];

      for (jack_nframes_t j = 0; j < sample_size && (i + j) < nframes; j++) {
        jack_nframes_t r = (i + randomized.Int(sample_size));
        if (r >= nframes) {
          r = i;
        }

        while (changed[r * CHANNELS + chan]) {
          r = r == (nframes - 1) ? i : r + 1;
          if (!changed[r * CHANNELS + chan]) {
            changed[r * CHANNELS + chan] = true;
            outputs[chan][r] = inputs[chan][i + j];
            break;
          }
        }
      }

      i += sample_size;
    }
  }
  /*} else {
          while( i < nframes ) {
                  (*outputs[ chan ])[ i ]
                  for( jack_nframes_t j = 0; j < sample_size && i < nframes;
  j++, i++ ) {

                  }
          }
  }*/

  return 0;
}

int main(int argc, char *argv[]) {
  initKeyboardInput();

  client.start(process);
  sleepProcess(NULL);

  shutdownInputs();
  return 0;
}