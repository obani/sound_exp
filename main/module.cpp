#include "../util/args.h"
#include "../util/string.h"
#include "../util/vector.h"

#include "../audio/jackclient.h"
#include "../input/input.h"

#include "../modules/list.h"

#include <signal.h>

using namespace Modules;

using updateFunction = bool (*)();

static vector<Module *> modules;
static JackClient client("SoundExp");
static MidiInput midi(client.initJackPorts(1, JackPortType_Midi,
                                           JackPortIsInput, "midiInput")[0]);
static vector<updateFunction> inputUpdates;
static std::array<int, NUM_MODULES> lastID = {};

static size_t selected = 0;

static void add_limited(size_t &s) {
  if (s != modules.size() - 1)
    ++s;
}

static LinkedValue<size_t, LinkedFuncs::sub_limited<size_t, 0>> prev{KEY_UP,
                                                                     selected};
static LinkedValue<size_t, add_limited> next{KEY_DOWN, selected};

static int process(jack_nframes_t nframes, void *arg) {
  client.update(nframes, true);

  if constexpr (false) {
    debugPrintln("Buffer size: %4u", nframes);
  }

  prev.checkPressedClean(keyboardEvents);
  next.checkPressedClean(keyboardEvents);

  midi.update();
  readKeyboardInputData();
  for (updateFunction f : inputUpdates)
    f();

  for (size_t i = 0; i < modules.size(); ++i) {
    bool s = selected == i;
    if (s)
      debugPrint(ANSI_BOLD_INTENSE_GREEN);
    modules[i]->process(nframes, s);
    if (s)
      debugPrint(ANSI_RESET);
  }

  const int refresh_rate = client.getSampleRate() / (nframes * 8);
  debugMovePrint(refresh_rate);
  return 0;
}

void SIGINT_handler(int pass) {
  for (Module *module : modules) {
    module->writeConfig();
    delete module;
  }
  debugMovePrintDown();
  shutdownInputs();
  exit(0);
}

static void ErrorExit() {
  printf("List of available modules:\n");
  for (const ModuleDescription &desc : modulesDescription)
    printf(" - %s: %s\n", desc.name.str(), desc.description.str());
  exit(1);
}

int main(int argc, char *argv[]) {
  ArgcHelpOption options[] = {
      {"-s", "--split",
       "Split modules in their own channels (except for midi input)"},
      {}};
  ArgcHandler(true, argc, argv, "<options>", options);

  bool split = HasArg(argc, argv, options[0].opt, options[0].altopt);

  signal(SIGINT, SIGINT_handler);
  uint8_t inputTypes = 0;

  vector<int> moduleIndexes;

  for (int i : range(1, argc)) {
    for (int m : range(modulesDescription.size())) {
      if (modulesDescription[m].name == argv[i]) {
        moduleIndexes.push_back(m);
        break;
      }
    }
  }

  if (moduleIndexes.size() == 0) {
    printf("No module specified\n");
    ErrorExit();
  }

  if (!split) {
    printf("Unify channels\n");
    Module::ModuleBuffers b;

    for (int m : moduleIndexes) {
      const ModuleDescription &desc = modulesDescription[m];
      b.audioInputs = std::max(b.audioInputs, desc.properties.audioInputs);
      b.audioOutputs = std::max(b.audioOutputs, desc.properties.audioOutputs);
      b.midiOutput = b.midiOutput || desc.properties.midiOutput;
    }

    const char *name = "modules";
    client.initJackPorts(b.audioInputs, JackPortType_Audio, JackPortIsInput,
                         name);
    client.initJackPorts(b.audioOutputs, JackPortType_Audio, JackPortIsOutput,
                         name);

    if (b.midiOutput)
      client.initJackPorts(1, JackPortType_Midi, JackPortIsOutput, name);
  }

  for (int m : moduleIndexes) {
    const ModuleDescription &desc = modulesDescription[m];
    modules.push_back(desc.instantiate(client, ++lastID[m], midi, split));
    inputTypes |= desc.input;
  }

  initKeyboardInput();

  if (inputTypes & HasMouse) {
    initMouseInput();
    inputUpdates.push_back(readMouseInputData);
  }

  if (inputTypes & HasJoystick) {
    initJoystickInput();
    inputUpdates.push_back(readJoystickInputData);
  }

  initMidi(client.getSampleRate());

  client.start(process);
  sleepProcess(NULL);

  return 0;
}