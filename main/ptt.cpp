#include "../util/args.h"
#include "../util/string.h"

#include "../audio/jackclient.h"
#include "../input/input.h"

#include <signal.h>

static const char *NAME = "Push-To-Talk";

static JackClient client(NAME);
static JackPort *in;
static JackPort *out;
static uint8_t key;
static bool pressed = false;

template <bool SWITCH_MODE>
static int process(jack_nframes_t nframes, void *arg) {
  client.update(nframes, true);
  readKeyboardInputData();

  bool cond;
  if constexpr (SWITCH_MODE) {
    if (keyboardEvents.pressedClean(key)) {
      pressed = !pressed;
      printf("%s%s\n", ANSI_MOVE_UP_DELETE, pressed ? "On" : "Off");
    }
    cond = pressed;
  } else {
    cond = keyboardEvents.pressed(key);
    if (pressed != cond)
      printf("%s%s\n", ANSI_MOVE_UP_DELETE, cond ? "On" : "Off");
    pressed = cond;
  }

  if (cond) {
    for (jack_nframes_t i = 0; i < nframes; ++i)
      (*out)[i] = (*in)[i];
  }

  return 0;
}

static void Exit(bool cond, const char *msg = NULL) {
  if (!cond)
    return;

  if (msg != NULL) {
    printf("%s\n", msg);
  }

  client.writeConfig();
  shutdownInputs();
  exit(0);
}

void SIGINT_handler(int pass) { Exit(true); }

int main(int argc, char *argv[]) {
  ArgcHelpOption options[] = {
      {"-k", "--keys", "Show key names for parameters"},
      {"-s", "--switch", "Show key names for parameters"},
      {}};
  ArgcHandler(true, argc, argv, "[options] <key>", options);

  int KEYS = HasArg(argc, argv, options[0].opt, options[0].altopt, false);
  int SWITCH = HasArg(argc, argv, options[1].opt, options[1].altopt, false);

  if (KEYS) {
    for (uint8_t k = 0; k < 255; ++k) {
      for (uint8_t i = 0; k < 255 && i < 4; ++k, ++i)
        printf("%s\t", keyName(k));
      printf("\n");
    }

    Exit(true);
  }

  int i;
  for (i = 1; i < argc; ++i) {
    if (i != KEYS && i != SWITCH) {
      uint8_t k;
      for (k = 0; k < 255; ++k) {
        if (strEqual(argv[i], keyName(k))) {
          printf("Started program with key %s (value: %d)\n", argv[i], k);
          key = k;
          break;
        }
      }

      Exit(k == 255, "Key wasn't found, use option --keys to show what keys "
                     "are available");
      break;
    }
  }

  Exit(i == argc,
       "Key wasn't set, use option --keys to show what keys are available");

  if (SWITCH)
    printf("Using SWITCH mode (--switch)\n");

  signal(SIGINT, SIGINT_handler);
  initKeyboardInput();
  client.initJackPorts(1, JackPortType_Audio, JackPortIsInput, "in");
  client.initJackPorts(1, JackPortType_Audio, JackPortIsOutput, "out", false);

  in = &client.getJackPorts(JackPortType_Audio, JackPortIsInput)[0];
  out = &client.getJackPorts(JackPortType_Audio, JackPortIsOutput)[0];

  client.readConfig("", NAME);

  printf("Off\n");
  client.start(SWITCH ? process<true> : process<false>);
  sleepProcess(NULL);

  return 0;
}