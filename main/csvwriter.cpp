#include "../audio/jackclient.h"

#include "../input/input.h"

#include "../util/args.h"
#include "../util/string.h"

#include "../files/csv.h"

#include "../modules/debug.h"

#include <signal.h>

static JackClient client("csvWriter");
static JackPort &midiInput(client.initJackPorts(1, JackPortType_Midi,
                                                JackPortIsInput,
                                                "midiInput")[0]);

static ssize_t fileIdx = -1;
static vector<String> pathes;
static const char *filePath;

static CSV csv;
static CSVWriter writer;

static int line = 0, column = 0;
static unsigned int cell = 0;

static void SIGINT_handler(int pass) {
  client.writeConfig();
  Modules::debugClearPrint(1);
  shutdownInputs();
  exit(0);
}

static void Exit(const char *str) {
  printf(str);
  SIGINT_handler(0);
}

static void nextCSV() {
  if (!writer.empty()) {
    writer.store(filePath, true);
    writer.clear();
  }

  while (true) {
    if (++fileIdx == ssize_t(pathes.size())) {
      Exit("Finished parsing CSVs");
      break;
    }

    filePath = pathes[fileIdx].str();
    printf("Trying to read file '%s'\n", filePath);
    if (!fileExists(filePath)) {
      printf("File '%s' doesn't exist\n", filePath);
      continue;
    }

    csv = CSV(filePath);
    if (!csv.isLoaded()) {
      printf("Couldn't read '%s' file\n", filePath);
      continue;
    }

    printf("Loaded file '%s' with %lu lines\n", filePath, csv.lines());

    line = 0;
    column = 0;
    break;
  }
}

static void nextLine() {
  while (csv.getCells(line) == 0) {
    printf("Finished parsing file '%s'\n", filePath);
    nextCSV();
  }

  const vector<String> &comments = csv.getComments(line);
  if (comments.size() != 0)
    for (const String &s : comments)
      writer.comment(s.str());

  ++line;
}

static void nextColumn() {
  if (column > 0) {
    writer.add(cell);
  }

  while (!csv.getInt(line - 1, column).exists) {
    column = 0;
    writer.next();
    nextLine();
  }

  ++column;
}

static const vector<String> &findLastComments(const CSV &csv, int currentLine) {
  const vector<String> &fallback = csv.getComments(0);

  while (currentLine >= 0) {
    const vector<String> &comments = csv.getComments(currentLine);
    currentLine -= 1;

    if (comments.size() != 0)
      return comments;
  }

  return fallback;
}

static int process(jack_nframes_t nframes, void *arg) {
  constexpr uint32_t MASK = 0b11111111;
  client.update(nframes, true);

  keyboardEvents.clear();
  readKeyboardInputData();

  jack_midi_event_t in_event;

  void *port_buf = midiInput.midi_data();
  jack_nframes_t event_count = jack_midi_get_event_count(port_buf);

  if (event_count != 0) {
    jack_midi_event_get(&in_event, port_buf, event_count - 1);
    cell = (*in_event.buffer << 8) + *(in_event.buffer + 1);
  }

  const int currentLine = line - 1;
  Modules::debugPrint(ANSI_BOLD_INTENSE_GREEN);
  Modules::debugPrintln("Editing file '%s'", filePath);
  Modules::debugPrintln("Line: %d\t Column: %d", currentLine, column - 1);

  const vector<String> &comments = findLastComments(csv, currentLine);
  for (const String &s : comments)
    Modules::debugPrintln(s.str());
  Modules::debugPrintln("%3d %3d", (cell >> 8) & MASK, cell & MASK);
  Modules::debugPrint(ANSI_RESET);

  if (keyboardEvents.pressed(KEY_ESC)) {
    nextCSV();
    nextLine();
    nextColumn();
  }

  if (keyboardEvents.pressed(KEY_ENTER)) {
    nextColumn();
  }

  const int refresh_rate = 1 + client.getSampleRate() / (nframes * 12);
  Modules::debugClearPrint(refresh_rate);

  return 0;
}

int main(int argc, char *argv[]) {
  ArgcHandler(argc > 1, argc, argv, "<files>", NULL);
  initKeyboardInput();

  for (int i = 1; i < argc; ++i)
    pathes.push_back(argv[i]);

  nextCSV();
  nextLine();
  nextColumn();

  signal(SIGINT, SIGINT_handler);

  client.readConfig();
  client.start(process);
  sleepProcess(NULL);

  return 0;
}