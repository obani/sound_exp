#include "../files/audio.h"
#include "../util/args.h"
#include "../util/string.h"

int main(int argc, char *argv[]) {
  ArgcHandler(argc > 1, argc, argv, "[files]", NULL);

  for (int i = 1; i < argc; i++) {
    const char *path = argv[i];

    printf("Loading file '%s'\n", path);
    SoundRAW sound(44100, 8, 2);
    openRAWFile(&sound, argv[i]);

    if (sound.num_samples == 0) {
      printf("Couldn't load sound '%s'\n", path);
      continue;
    }

    String path2(path);
    path2 += ".wav";
    printAudioSpecs(sound);
    sound.save(path2.str());
  }

  return 0;
}