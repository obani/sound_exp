# Contact

https://soundcloud.com/obani
https://twitter.com/obanigarage

# Sound Exp

A project about treating physical inputs to make cool audio renders and apps.

Support mouse/keyboard/controller and jack inputs (audio/midi).

It supports opening files and manipulating raw data and raw sample data.
Input data is read directly from the linux files (`/dev/input`), so it doesn't need any graphical interface nor focus. 

Some jack connections may be saved in `~/.config/sound_exp` upon closing the program.
This makes it possible to not reconnect everything on each launch.

Programs list : 
- fileselector : Permits to select files with a specific file extension in your file system to list them in a file
- gensounds : Generates the wav sounds from the data of the specified input files
- modports : Multiplies a jack port by another jack port (not very interesting)
- modsounds : Modifies input sounds with a specified effect (included: invert signal/sort)
- sampler : Sampler where you load samples and play them with MIDI keyboard input
- sample_randomizer : Randomizes samples in an input (not very interesting, buggy)
- sequencer : Sequencer that uses MIDI keyboard input
- sound_exp : Main program, permits to load several modules
    - dubatone: Continuous oscillator controlled with midi controllers
    - loopstation: Loopstation controlled with midi controllers
    - miditest: Test midi input and display it
    - synthz: Synths
    - tunor: Generate a synth sound on the main frequency of the input
    - tunetest: Find the main frequency of the current signal
    - vmk: Virtual midi keyboard based on computer keyboard
    - vmw: Virtual midi pitch wheel based on mouse
    - vmkw: Virtual midi keyboard and pitch wheel based on computer keyboard and mouse

- terminatory : A bad terminatorx copy
- test : Test program to test audio/input/midi

## Troubleshooting

#### Input

The input data is read from the /dev/input system files, that may require root access.

Unfortunately, jack doesn't seem to run well with root, and running a program with root can be pretty annoying anyways, so you might want to setup an input group if it doesn't already exist, add yourself to this group and set /dev/input to be owned by the group input

**If you already have the input reading working, you don't need to do what is in this section.**

Most of these commands require root access.
Create the input group :
```
groupadd -f input
```

Add yourself to the group :
```
usermod -a -G input my_user_name
```

Set the rights using ACL. You may want to create a file in `/etc/udev/rules.d` :
```
touch /etc/udev/rules.d/99-userdev-input.rules
```

Then edit it to add this line :
```
KERNEL=="*", SUBSYSTEM=="input", RUN+="/usr/bin/setfacl -m g:input:rx $env{DEVNAME}"
```

If you want to test right away, you can use the ACL command directly :
```
setfacl -m g:input:rx /dev/input/*
```

*You may need to restart your PC after that*

#### Audio

The audio engine used is now jack, so you won't be able to run the audio programs without it started
We recommend using a tool like [Cadence](https://kx.studio/Applications:Cadence) or [qjackctl](https://qjackctl.sourceforge.io/) if you're not familiar with the use of jack.

If you have problems starting jack because of memory locks, you may want to set your user's limit to 'unlimited'



## Build the project

Init the submodules
```
git submodule update --init
```

### Depencies

**Build depencies:** `go`, `ninja`, `make`
**Program depencies:** `jack`, `fftw3`

#### TL;DR

Run :
```
make clean && make ninja && ninja
```

#### More precise instructions

To generate the ninja build file, run :
```
make ninja
```

To build all the executables, run :
```
ninja
```

To build a specific binary, check the `main/` directory and type `ninja <name of the file>`.
Example :
```
ninja module
```

To clean the build directory, type :
```
make clean
```