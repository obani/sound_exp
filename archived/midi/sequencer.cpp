#include "sequencer.h"

// Sequence

Sequence::Sequence()
    : called(false), was_called(false), current_step(0), active(true) {}
Sequence::Sequence(size_t num_steps)
    : called(false), was_called(false), current_step(0), steps(num_steps, 0),
      active(true) {}
Sequence::Sequence(size_t num_steps, const char *n)
    : called(false), was_called(false), name(n), current_step(0),
      steps(num_steps, 0), active(true) {}
Sequence::Sequence(const Sequence &cpy)
    : called(cpy.called), was_called(cpy.was_called), name(cpy.name),
      current_step(cpy.current_step), steps(cpy.steps), active(cpy.active) {}

Sequence &Sequence::operator=(const Sequence &rhs) {
  called = rhs.called;
  was_called = rhs.was_called;
  name = rhs.name;
  current_step = rhs.current_step;
  steps = rhs.steps;
  active = rhs.active;
  return *this;
}

void Sequence::setStep(size_t step) {
  assert(step < steps.len());
  current_step = step;
  called = false;
  was_called = false;
}

void Sequence::setStep(size_t step, uint16_t val) {
  assert(step < steps.len());
  current_step = step;
  steps[current_step] = val;
  called = false;
  was_called = false;
}

uint16_t Sequence::getStep() {
  if (called || !active)
    return 0;
  called = true;
  was_called = true;
  return steps[current_step];
}

uint16_t Sequence::lastStep() {
  if (!was_called || !active)
    return 0;
  was_called = false;
  return steps[current_step];
}

size_t Sequence::getIdx(jack_nframes_t frame, jack_nframes_t length) {
  return (frame * steps.len()) / length;
}

// Sequencer
// protected

bool Sequencer::seq_doesnt_exist(size_t s) {
  if (s >= sequences.len())
    printf("Sequence %ld doesn't exist\n", s);
  return s >= sequences.len();
}

bool Sequencer::seq_step_doesnt_exist(size_t s, size_t i) {
  if (sequences[s].steps.len() <= i)
    printf("Sequence only has %ld steps (tried to access step %ld)\n",
           sequences[s].steps.len(), i + 1);
  return sequences[s].steps.len() <= i;
}

// public

Sequencer::Sequencer() : length{0}, selected{0}, frame{0}, active{true} {}
Sequencer::Sequencer(jack_nframes_t l)
    : length{l}, selected{0}, frame{0}, active{true} {}
Sequencer::Sequencer(Sequencer &s)
    : length{s.length}, selected{s.selected}, sequences{s.sequences},
      frame{s.frame}, active{s.active} {}

Sequencer &Sequencer::operator=(const Sequencer &rhs) {
  frame = rhs.frame;
  length = rhs.length;
  sequences = rhs.sequences;
  return *this;
}

jack_nframes_t Sequencer::getLength() const { return length; }

void Sequencer::setLength(jack_nframes_t new_length) {
  frame *= (new_length / ((jack_default_audio_sample_t)length));
  length = new_length;
}

void Sequencer::select(size_t i) {
  if (seq_doesnt_exist(i))
    return;

  selected = i;
}

void Sequencer::selectPrev() { select(selected == 0 ? 0 : selected - 1); }
void Sequencer::selectNext() { select(Min(selected + 1, sequences.len() - 1)); }

size_t Sequencer::getSelectedIndex() { return selected; }

Sequence *Sequencer::getSelected() { return &sequences[selected]; }
Sequence *Sequencer::getSequence(size_t seq) {
  if (seq_doesnt_exist(seq))
    return NULL;

  return &sequences[seq];
}

void Sequencer::setStep(uint16_t val) {
  sequences[selected].setStep(sequences[selected].current_step, val);
}

void Sequencer::setStep(size_t step, uint16_t val) {
  if (seq_step_doesnt_exist(selected, step))
    return;

  sequences[selected].setStep(step, val);
}

void Sequencer::setStep(size_t seq, size_t step, uint16_t val) {
  if (seq_doesnt_exist(seq) || seq_step_doesnt_exist(seq, step))
    return;

  sequences[seq].setStep(step, val);
}

int16_t Sequencer::getStep(size_t seq) {
  if (seq_doesnt_exist(seq))
    return -1;

  return sequences[seq].getStep();
}

void Sequencer::step(jack_nframes_t step_size) {
  if (length == 0)
    return;

  frame += step_size;

  bool startover = false;
  if (frame >= length) {
    frame = frame % length;
    startover = true;
  }

  if (!active)
    return;

  for (size_t i = 0; i < sequences.len(); i++) {
    Sequence *s = &sequences[i];
    if (s->steps.len() == 0)
      continue;

    size_t idx = s->getIdx(frame, length);
    if (idx != s->current_step || startover) {
      s->setStep(idx);
    }
  }
}
