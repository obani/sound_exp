#ifndef __MY_SEQUENCER_HANDLER
#define __MY_SEQUENCER_HANDLER

#include <assert.h>

#include "../util/vector.h"

#include "../audio/sampler.h"
#include "../midi/midi.h"
#include "../midi/sequencer.h"

#include "../synth/bank/additive.h"
#include "../synth/blendsynth.h"
#include "../synth/synth.h"

class SequencerHandler {
private:
  template <typename NODE> struct SeqNode {
    NODE *node;
    size_t seq;
  };

  template <typename NODE>
  void addNode(NODE *n, size_t seq, vector<SeqNode<NODE>> *vec) {
    assert(seq < sequencer.sequences.len());

    n->outputs = outputs;
    for (size_t i = 0; i < vec->len(); i++) {
      if ((*vec)[i].node == NULL) {
        (*vec)[i].node = n;
        (*vec)[i].seq = seq;
        return;
      }
    }

    vec->push_back({n, seq});
  }

  template <typename NODE>
  void removeIfExists(NODE *n, vector<SeqNode<NODE>> *vec) {
    for (size_t i = 0; i < vec->len(); i++) {
      if ((*vec)[i].node == n) {
        (*vec)[i].node = NULL;
        break;
      }
    }
  }

  template <typename NODE>
  void stepSynthNodes(vector<NODE> &v, jack_nframes_t nframes) {
    for (size_t i = 0; i < v.len(); i++) {
      if (v[i].node != NULL) {
        int16_t step = sequencer.getStep(v[i].seq);
        if (step > 0) {
          v[i].node->play(getMidiKey(step, 1.0f));
        }

        v[i].node->step(nframes);
      }
    }
  }

  vector<SeqNode<Synth<Additive>>> synth_Nodes;
  vector<SeqNode<BlendSynth>> blendsynth_Nodes;
  vector<SeqNode<Sampler>> sampler_Nodes;

public:
  Sequencer sequencer;
  Slice<JackPort> outputs;

  SequencerHandler() {}
  SequencerHandler(Sequencer &s) : sequencer{s} {}
  SequencerHandler(SequencerHandler &cpy)
      : synth_Nodes{cpy.synth_Nodes}, blendsynth_Nodes{cpy.blendsynth_Nodes},
        sampler_Nodes{cpy.sampler_Nodes}, sequencer{cpy.sequencer},
        outputs{cpy.outputs} {}

  SequencerHandler &operator=(const SequencerHandler &rhs) {
    synth_Nodes = rhs.synth_Nodes;
    blendsynth_Nodes = rhs.blendsynth_Nodes;
    sampler_Nodes = rhs.sampler_Nodes;

    sequencer = rhs.sequencer;
    outputs = rhs.outputs;
    return *this;
  }

  jack_nframes_t getLength() const { return sequencer.getLength(); }
  void setLength(jack_nframes_t new_length) {
    return sequencer.setLength(new_length);
  }
  void select(size_t i) { return sequencer.select(i); }
  void selectPrev() { return sequencer.selectPrev(); }
  void selectNext() { return sequencer.selectNext(); }
  Sequence *getSelected() { return sequencer.getSelected(); }
  size_t getSelectedIndex() { return sequencer.getSelectedIndex(); }
  Sequence *getSequence(size_t seq) { return sequencer.getSequence(seq); }
  void setStep(uint16_t val) { return sequencer.setStep(val); }
  void setStep(size_t step, uint16_t val) {
    return sequencer.setStep(step, val);
  }
  void setStep(size_t seq, size_t step, uint16_t val) {
    return sequencer.setStep(seq, step, val);
  }
  int16_t getStep(size_t seq) { return sequencer.getStep(seq); }

  void link(Synth<Additive> *s, size_t i) { addNode(s, i, &synth_Nodes); }
  void link(BlendSynth *s, size_t i) { addNode(s, i, &blendsynth_Nodes); }
  void link(Sampler *s, size_t i) { addNode(s, i, &sampler_Nodes); }
  void unlink(Synth<Additive> *s) { removeIfExists(s, &synth_Nodes); }
  void unlink(BlendSynth *s) { removeIfExists(s, &blendsynth_Nodes); }
  void unlink(Sampler *s) { removeIfExists(s, &sampler_Nodes); }

  void step(jack_nframes_t nframes) {
    sequencer.step(nframes);

    stepSynthNodes(synth_Nodes, nframes);
    stepSynthNodes(blendsynth_Nodes, nframes);

    for (size_t i = 0; i < sampler_Nodes.len(); i++) {
      if (sampler_Nodes[i].node != NULL) {
        sampler_Nodes[i].node->play(sequencer.getStep(sampler_Nodes[i].seq),
                                    1.0);
        sampler_Nodes[i].node->step(nframes);
      }
    }
  }
};

#endif //__MY_SEQUENCER_HANDLER