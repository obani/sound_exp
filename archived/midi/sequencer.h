#ifndef _SEQUENCER_HEADER
#define _SEQUENCER_HEADER

#include <assert.h>
#include <jack/jack.h>
#include <string.h>
#include <unistd.h>

#include "../util/math.h"
#include "../util/string.h"
#include "../util/vector.h"

class Sequence {
private:
  bool called, was_called;

public:
  String name;
  size_t current_step;
  vector<uint16_t> steps;
  bool active;

  Sequence();
  Sequence(size_t num_steps);
  Sequence(size_t num_steps, const char *n);
  Sequence(const Sequence &cpy);
  Sequence &operator=(const Sequence &rhs);

  void setStep(size_t step);
  void setStep(size_t step, uint16_t val);
  uint16_t getStep();
  uint16_t lastStep();
  size_t getIdx(jack_nframes_t frame, jack_nframes_t length);
};

class Sequencer {
protected:
  jack_nframes_t length;
  size_t selected;

  bool seq_doesnt_exist(size_t s);
  bool seq_step_doesnt_exist(size_t s, size_t i);

public:
  vector<Sequence> sequences;
  jack_nframes_t frame;
  bool active;

  Sequencer();
  Sequencer(jack_nframes_t l);
  Sequencer(Sequencer &s);
  Sequencer &operator=(const Sequencer &rhs);
  jack_nframes_t getLength() const;
  void setLength(jack_nframes_t new_length);
  void select(size_t i);
  void selectPrev();
  void selectNext();
  size_t getSelectedIndex();
  Sequence *getSelected();
  Sequence *getSequence(size_t seq);
  void setStep(uint16_t val);
  void setStep(size_t step, uint16_t val);
  void setStep(size_t seq, size_t step, uint16_t val);
  int16_t getStep(size_t seq);
  virtual void step(jack_nframes_t step_size);
};

#endif