#include "../input/linkedinput.h"

class Selector {
private:
  LinkedInput<void> addInput;
  LinkedInput<void> popInput;
  LinkedInput<void> prevInput;
  LinkedInput<void> nextInput;

  void (*addCallback)(void);
  void (*popCallback)(void);

  size_t selected;
  size_t length;

  void addClass() {
    length++;

    if (addCallback) {
      addCallback();
    }
  }

  void popClass() {
    length--;

    if (popCallback != NULL) {
      popCallback();
    }

    if (selected == length) {
      selected--;
    }
  }

  void selectPrev() {
    if (selected != 0) {
      selected--;
    }
  }

  void selectNext() {
    if (selected != length - 1) {
      selected++;
    }
  }

public:
  ClassAdder() : addCallback{NULL}, popCallback{NULL}, selected{0} {
    addInput.setFunc(addClass);
    popInput.setFunc(popClass);
    prevInput.setFunc(selectPrev);
    nextInput.setFunc(selectNext);
  }

  ClassAdder(ClassAdder &cpy)
      : addInput{cpy.addInput}, popInput{cpy.popInput},
        prevInput{cpy.prevInput}, nextInput{cpy.nextInput},
        addCallback{cpy.addCallback}, popCallback{cpy.popCallback},
        selected{cpy.selected}, length{cpy.length} {}

  void setAddKey(uint8_t k) { addInput.input = k; }
  void setPopKey(uint8_t k) { popInput.input = k; }
  void setPrevKey(uint8_t k) { prevInput.input = k; }
  void setNextKey(uint8_t k) { nextInput.input = k; }

  void setAddCallback(void (*f)(void)) { addCallback = f; }
  void setPopCallback(void (*f)(void)) { popCallback = f; }

  void check(array<uint8_t> *keys) {
    for (size_t i = 0; i < keys->len(); i++) {
      if (addInput.check((*keys)[i]) || popInput.check((*keys)[i]) ||
          prevInput.check((*keys)[i]) || nextInput.check((*keys)[i])) {
        return;
      }
    }
  }

  size_t getIndex() {
    assert(length != 0);
    return selected;
  }
};

template <typename T> class ClassSelector : public Selector {
private:
  vector<T> vec;

  void addCallback() { vector.set_size(length); }

  void popCallback() { vector.remove(selected); }

public:
  ClassSelector
}
