#ifndef __LOOPSTATION_HEADER
#define __LOOPSTATION_HEADER

#include <assert.h>
#include <jack/jack.h>
#include <stdlib.h>
#include <unistd.h>

#include "../util/array.h"
#include "../util/classes.h"
#include "../util/math.h"

#include "../audio/adsr.h"

#include "../midi/midi.h"

#include "pan.h"

struct LoopTrack {
  MidiControllerLink<> volume_control;
  MidiControllerLink<> pan_control;

private:
  bool initialized, clearing, activated, recording;

  size_t length, frame, clearing_frame;

  array<array<jack_default_audio_sample_t>> samples;
  ADSR envelope, record_envelope; // fade-in, fade-out
  Pan pan;

  void do_step(Slice<JackPort> &outputs, Slice<JackPort> &inputs, size_t from,
               size_t to) {
    assert(from <= to);

    if (from == to)
      return;

    size_t nframes = to - from;

    envelope.checkHeld(activated);

    bool was_clearing = clearing;
    clearing = clearing && !(from <= clearing_frame && to > clearing_frame);

    const float volume = volume_control.get();

    if ((!envelope.finished() || activated) && initialized &&
        length >= nframes) { // playback, clear if we need to
      size_t chan = 0;
      for (JackPort &p : outputs) {
        size_t track_chan = (chan++) % samples.len();

        for (size_t i = 0; i < nframes; i++) {
          size_t real_i = from + i;
          if (was_clearing && (clearing || real_i <= clearing_frame))
            samples[track_chan][real_i] = 0.f;

          p[i] += samples[track_chan][real_i] * pan.output[chan] * volume *
                  envelope.step();
        }
      }
    } else if (was_clearing) // Clear even if we're not doing the playback
      for (array<jack_default_audio_sample_t> &chan : samples)
        for (size_t i = from; i < to; i++)
          if (clearing || i <= clearing_frame)
            chan[i] = 0.f;

    record_envelope.checkHeld(recording);
    if (recording || !envelope.finished()) // record inputs
      for (size_t chan = 0; chan < samples.len(); chan++)
        for (size_t i = 0; i < nframes; i++)
          samples[chan][from + i] += inputs[chan][i] * record_envelope.step();
  }

public:
  LoopTrack()
      : initialized{false}, clearing{false}, activated{false}, recording{false},
        length{0}, frame{0}, clearing_frame{0} {
    active();
  }

  LoopTrack(jack_default_audio_sample_t sample_rate, size_t l,
            uint16_t channels)
      : initialized{false}, clearing{false}, activated{false}, recording{false},
        length{0}, frame{0}, clearing_frame{0},
        samples{channels, array<jack_default_audio_sample_t>(l, 0.0)},
        envelope{sample_rate, 0.05, 0.0, 1.0, 0.05},
        record_envelope{sample_rate, 0.05, 0.0, 1.0, 0.05} {
    active();
  }

  LoopTrack &operator=(const LoopTrack &rhs) {
    initialized = rhs.initialized;
    clearing = rhs.clearing;
    activated = rhs.activated;
    recording = rhs.recording;

    length = rhs.length;
    frame = rhs.frame;
    clearing_frame = rhs.clearing_frame;

    samples = rhs.samples;
    envelope = rhs.envelope;
    record_envelope = rhs.record_envelope;
    pan = rhs.pan;

    volume_control = rhs.volume_control;
    pan_control = rhs.pan_control;
    return *this;
  }

  bool isInitialized() { return initialized; }
  bool isActive() { return activated; }
  bool isRecording() { return recording; }

  bool active() {
    activated = !activated;
    if (activated)
      envelope.start();
    return activated;
  }

  size_t record() {
    if (!initialized && recording && frame != 0) {
      initialized = true;

      if (length == 0) {
        length = frame;
        clearing = false;

        return length;
      }

      size_t i = 1; // Adapt to previously set length;
      if (frame <= length) {
        while (frame <= (length / (i * 2)))
          i *= 2;
        length /= i;
      } else {
        while (frame > (length * i))
          i *= 2;
        length *= i;
      }

      if (clearing && clearing_frame > frame && frame < length)
        clearing_frame = length - 1;

      frame = frame % length;
    } else {
      recording = !recording;
      if (recording)
        record_envelope.start();
    }

    return 0;
  }

  // divider should always be a power of 2, or there are more risks of
  // desyncrhonizing the loops. it doesn't matter for only one loop tho.
  void backwardBy(int divider) { // backward by length/divider (for example 1/4
                                 // of length), to stay in sync
    if (!initialized)
      return;

    size_t div = length / divider;
    frame = (div > frame) ? length - div + frame : frame - div;
  }

  void forwardBy(int divider) { // same with forward
    if (!initialized)
      return;

    frame = (frame + length / divider) % length;
  }

  void sync(size_t l) {
    assert(l < samples[0].len());

    if (!initialized)
      length = l;
  }

  void reset(bool reset_length) {
    if (length == 0)
      return;

    initialized = false;
    recording = false;
    clearing = true;

    if (reset_length) {
      length = 0;
      clearing_frame = samples[0].len() - 1;
    } else {
      clearing_frame = length - 1;
    }

    frame = 0;
  }

  void clear() {
    if (length == 0)
      return;

    clearing = true;
    clearing_frame = (frame == 0) ? (length - 1) : (frame - 1);
  }

  void step(Slice<JackPort> &outputs, Slice<JackPort> &inputs, size_t nframes) {
    if (!(initialized || recording))
      return;

    size_t from = frame;
    frame += nframes;

    pan.orientation = pan_control.get() - 0.5f;

    pan.updateOutput(samples.len());

    if (!initialized || (frame < length)) {
      do_step(outputs, inputs, from, frame);
    } else if (frame >= length) {
      do_step(outputs, inputs, from, length);
      frame = frame % length;
      do_step(outputs, inputs, 0, frame);
    }
  }
};

class LoopStation : NotCopyable {
  MidiInput *midi;
  jack_default_audio_sample_t sample_rate;

  size_t max_samples;
  size_t selected;

  bool track_doesnt_exist(size_t t) {
    if (t >= tracks.len()) {
      printf("Track %ld doesn't exist\n", t);
      return true;
    }
    return false;
  }

public:
  vector<LoopTrack> tracks;
  Slice<JackPort> inputs, outputs;

  bool input_playback;

  LoopStation(jack_default_audio_sample_t sr, const Slice<JackPort> &i,
              const Slice<JackPort> &o)
      : midi{NULL}, sample_rate{sr}, selected{0}, inputs{i}, outputs{o},
        input_playback{false} {}

  void setMaxSeconds(float seconds) { max_samples = sample_rate * seconds; }

  size_t addTrack() {
    if (inputs.size() == 0) {
      printf("No input channel added, please add input channels before "
             "creating tracks\n");
      return 0;
    }

    tracks.push_back(LoopTrack(sample_rate, max_samples, inputs.size()));
    tracks[tracks.len() - 1].volume_control.midi = midi;
    tracks[tracks.len() - 1].pan_control.midi = midi;
    return tracks.len();
  }

  void setController(MidiInput *m) {
    midi = m;

    for (size_t i = 0; i < tracks.len(); i++) {
      tracks[i].volume_control.midi = midi;
      tracks[i].pan_control.midi = midi;
    }
  }

  void activate() {
    for (size_t i = 0; i < tracks.len(); i++)
      if (!tracks[i].isActive())
        tracks[i].active();
  }

  void deactivate() {
    for (size_t i = 0; i < tracks.len(); i++)
      if (tracks[i].isActive())
        tracks[i].active();
  }

  void reset() {
    for (size_t i = 0; i < tracks.len(); i++)
      tracks[i].reset(true);
  }

  size_t select(size_t t) {
    if (track_doesnt_exist(t))
      t = clamp(0UL, t, tracks.len() - 1);

    if (tracks[selected].isRecording() && t != selected) {
      tracks[selected].record();
      if (tracks[selected].isRecording())
        tracks[selected].record();
    }

    return (selected = t);
  }

  size_t getSelected() { return selected; }
  size_t selectPrev() {
    return selected == 0 ? selected : select(selected - 1);
  }
  size_t selectNext() {
    return selected == (tracks.len() - 1) ? selected : select(selected + 1);
  }

  size_t trackClear() {
    tracks[selected].clear();
    return selected;
  }

  size_t trackReset() {
    bool reset_length = true;
    for (size_t i = 0; i < tracks.len(); i++) {
      if (i != selected && tracks[i].isInitialized()) {
        reset_length = false;
        break;
      }
    }

    if (reset_length)
      for (size_t i = 0; i < tracks.len(); i++)
        tracks[selected].reset(true);
    else
      tracks[selected].reset(false);

    return selected;
  }

  size_t trackVolumeLink() {
    tracks[selected].volume_control.link();
    return selected;
  }

  size_t trackPanLink() {
    tracks[selected].pan_control.link();
    return selected;
  }

  bool trackActive() { return tracks[selected].active(); }

  bool trackActive(size_t i) {
    if (track_doesnt_exist(i))
      i = clamp(0UL, i, tracks.len() - 1);
    return tracks[i].active();
  }

  bool trackRecord() {
    size_t l = tracks[selected].record();
    if (l != 0)
      for (size_t j = 0; j < tracks.len(); j++)
        tracks[j].sync(l);

    return tracks[selected].isRecording();
  }

  void trackBackwardBy(int power_of_2) {
    tracks[selected].backwardBy(pow(2, power_of_2));
  }
  void trackForwardBy(int power_of_2) {
    tracks[selected].forwardBy(pow(2, power_of_2));
  }

  void step(size_t nframes) {
    if (input_playback)
      for (size_t chan = 0; chan < outputs.size(); chan++)
        for (size_t i = 0; i < nframes; i++)
          outputs[chan][i] += inputs[chan][i];

    for (size_t i = 0; i < tracks.len(); i++) {
      uint8_t v = tracks[i].volume_control.update();
      uint8_t p = tracks[i].pan_control.update();

      if (v != 0)
        printf("Track %ld volume linked to controller %d\n", i + 1, v);
      if (p != 0)
        printf("Track %ld pan linked to controller %d\n", i + 1, p);

      tracks[i].step(outputs, inputs, nframes);
    }
  }
};

#endif