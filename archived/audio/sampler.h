#pragma once

#include <assert.h>
#include <jack/jack.h>
#include <unistd.h>

#include "../audio/pan.h"
#include "../audio/sound.h"

#include "../files/audio.h"

#include "../util/classes.h"
#include "../util/math.h"
#include "../util/string.h"
#include "../util/vector.h"

template <size_t CHANNELS> class Sample {
  bool playing;
  float velocity;

public:
  Sound sound;
  uint16_t id;
  String name;
  float volume, speed;
  Pan<CHANNELS> pan;

  Sample() : playing{false}, id{0}, volume{0.0}, speed{1.0} {}

  Sample(const Sound &s, uint16_t ID, const char *n)
      : playing{false}, sound{s}, id{ID}, name{n}, volume{1.0}, speed{1.0} {}

  Sample(const Sound &s, uint16_t ID, const char *n, float v)
      : playing{false}, sound{s}, id{ID}, name{n}, volume{v}, speed{1.0} {}

  Sample &operator=(const Sample &rhs) {
    playing = rhs.playing;
    sound = rhs.sound;
    id = rhs.id;
    name = rhs.name;
    volume = rhs.volume;
    speed = rhs.speed;
    pan = rhs.pan;
    return *this;
  }

  void setSound(const Sound &s) { sound = s; }

  bool isPlaying() const { return playing && sound.exists(); }

  void start(float vel) {
    if (sound.exists() &&
        (!playing || sound.offset == 0 || sound.offset > 2000)) {
      sound.offset = 0;
      velocity = vel;
      playing = true;
    }
  }

  void step(Slice<JackPort> &outputs, jack_nframes_t nframes) {
    size_t channels = outputs.size();

    jack_nframes_t frames_written = 0;
    for (size_t chan = 0; chan < channels; chan++) {
      frames_written = sound.toPort(outputs[chan], 0, nframes, speed, chan,
                                    velocity * volume * pan.getPan(chan));
    }

    if (frames_written != nframes)
      playing = false;
    else
      sound.offset += nframes;
  }
};

template <size_t CHANNELS> struct Sampler : NotCopyable {
  bool recording;
  vector<Sample<CHANNELS>> samples;

  Slice<JackPort> outputs;

  Sampler(const Slice<JackPort> &o) : outputs{o} {}

  bool addSample(const Sample<CHANNELS> &s) {
    samples.push_back(s);
    return true;
  }

  bool addSample(const char *path, uint16_t ID) {
    Sound s = openAudioFile(path);
    if (s.num_samples == 0)
      return false;

    String path_str(path);
    vector<String> sub_pathes(path_str.split("/"));
    const char *sample_name = sub_pathes[sub_pathes.size() - 1].str();

    samples.push_back({s, ID, sample_name});
    return true;
  }

  bool addSample(const char *path, uint16_t ID, float volume) {
    if (!addSample(path, ID))
      return false;
    samples[samples.size() - 1].volume = volume;
    return true;
  }

  void removeSample(size_t i, bool free) {
    assert(i < samples.size());
    if (free)
      samples[i].sound.free();
    samples.remove(i);
  }

  bool play(uint16_t id, float volume) {
    for (size_t i = 0; i < samples.size(); i++) {
      if (samples[i].id == id) {
        samples[i].start(volume);
        return true;
      }
    }

    return false;
  }

  void step(jack_nframes_t nframes) {
    for (size_t s = 0; s < samples.size(); s++)
      if (samples[s].isPlaying())
        samples[s].step(outputs, nframes);
  }
};