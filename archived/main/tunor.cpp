#include "../audio/jackclient.h"

#include "../synth/bank/harmonics.h"
#include "../synth/synth.h"

#include "../util/math.h"
#include <fftw3.h>
#include <signal.h>

JackClient client("tunor");
JackPorts inputs(client.getJackPorts(JackPortType_Audio, JackPortIsInput));
JackPorts outputs(client.getJackPorts(JackPortType_Audio, JackPortIsOutput));

static int tune(jack_nframes_t nframes, void *arg) {
  JackClient *c = (JackClient *)arg;
  c->update(nframes, true);

  double max = -10.0f;
  for (jack_nframes_t i = 0; i < nframes; step++, i++) {
    fftw_input[step][0] = inputs[0][i];
    fftw_input[step][1] = 0.0f;

    if (fftw_input[step][0] > max) {
      max = fftw_input[step][0];
    }
  }

  if (abs(max) < 0.05) {
    step = 0;
  }

  if (step >= MAX_SIZE) { // buffer filled
    step = 0;

    fftw_execute(plan);

    max = -10.0f;
    int16_t max_idx = 0;
    for (int16_t i = 0; i < L; i++) {
      freqs[i] = sqrt(fftw_output[i][0] * fftw_output[i][0] +
                      fftw_output[i][1] * fftw_output[i][1]);
      if (freqs[i] > max) {
        max = freqs[i];
        max_idx = i;
      }
    }

    if (max_idx != 0) {
      synth.play({max_idx, (jack_default_audio_sample_t)(max_idx) / L, 0.5f});
    }
  }

  synth.step(nframes);

  return 0;
}

void SIGINT_handler(int pass) {
  client.writeConfig();
  fftw_destroy_plan(plan);
  exit(0);
}

int main(int argc, char *argv[]) {
  signal(SIGINT, SIGINT_handler);

  plan = fftw_plan_dft_1d(MAX_SIZE, fftw_input, fftw_output, FFTW_FORWARD,
                          FFTW_ESTIMATE);

  initJackPorts(client, 1, JackPortType_Audio, JackPortIsInput);
  initJackPorts(client, 2, JackPortType_Audio, JackPortIsOutput);
  client.readConfig();

  {
    Harmonics s(ADSR(client.getSampleRate(), randomized.Float(0.5f, 1.f),
                     randomized.Float(1.f, 2.f), 0.5,
                     randomized.Float(1.f, 3.f)));

    s.setWave(Wave_Sine);

    for (size_t i = 0; i < 3; i++) {
      s.addHarmonic(randomized.Float(0.7f, 0.8f),
                    Pan(randomized.Float11(), 0.5f),
                    randomized.Float(0.98f, 1.02f));
    }

    synth = Synth(s, 10, outputs);
  }

  startJackClient(client, tune);
  sleepProcess(NULL);

  return 0;
}