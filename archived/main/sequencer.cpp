#include "../util/args.h"
#include "../util/math.h"
#include "../util/random.h"
#include "../util/vector.h"

#include "../input/input.h"
#include "../input/linkedinput.h"

#include "../midi/midi.h"
#include "../midi/sequencer.h"

static JackClient client("sequencer");
static Slice<JackPort> output(client.initJackPorts(1, JackPortType_Midi,
                                                   JackPortIsOutput));

static Sequencer sequencer;
static MidiInput midi(client.initJackPorts(1, JackPortType_Midi,
                                           JackPortIsInput)[0]);

class InternalDisplay {
private:
  int step;
  vector<String> lines;

public:
  InternalDisplay(int s) : step{s + 1} {
    lines.push_back(ANSI_MOVE_UP_DELETE);
    addLine();
    printf("\n");
  }

  void set(int x, int y, char c) { lines[y + 1][x * step] = c; }

  void addColumn(int line) {
    for (int i = 0; i < step; i++) {
      lines[line + 1] += ' ';
    }
  }

  void addLine() {
    lines[0] += ANSI_MOVE_UP_DELETE;
    lines.push_back("");
    printf("\n");
  }

  void print() {
    for (size_t i = 0; i < lines.len(); i++) {
      printf("%s\n", lines[i].str());
    }
  }
};

static InternalDisplay display(3);

static void randomizeSeq() {
  Sequence *selected = sequencer.getSelected();
  for (size_t i = 0; i < selected->steps.len(); i++) {
    selected->steps[i] =
        randomized.Bool() ? 0 : minorScale(A3, randomized.Int(12));
  }
}

static void printSel() {
  printf("%s%sSelected %s\n\n", ANSI_MOVE_UP_DELETE, ANSI_MOVE_UP_DELETE,
         sequencer.getSelected()->name.str());
}

static void prevSeq() {
  sequencer.selectPrev();
  printSel();
}

static void nextSeq() {
  sequencer.selectNext();
  printSel();
}

static void muteSeq() {
  Sequence *selected = sequencer.getSelected();
  selected->active = !selected->active;
  printf("%s%s%sMuted channel %s\n", ANSI_MOVE_UP_DELETE, ANSI_MOVE_UP_DELETE,
         selected->active ? "Un" : "", selected->name.str());
}

static void clearSeqFunc() { sequencer.getSelected()->steps.set(0); }

static void speedUpSeq() {
  size_t l = sequencer.getLength() * 1.015;
  sequencer.setLength(l);
}

static void speedDownSeq() {
  size_t l = sequencer.getLength() * 0.985;
  sequencer.setLength(l);
}

static void addSequence() {
  String name = "seq1";
  name[3] += sequencer.sequences.len();
  sequencer.sequences.push_back(Sequence(32, name.str()));

  display.addColumn(0);

  printf("%s%sAdded track %s\n\n", ANSI_MOVE_UP_DELETE, ANSI_MOVE_UP_DELETE,
         name.str());
}

static LinkedInput<prevSeq> prev(KEY_LEFT);
static LinkedInput<nextSeq> next(KEY_RIGHT);
static LinkedInput<muteSeq> mute(KEY_END);
static LinkedInput<randomizeSeq> randSeq(KEY_F1);
static LinkedValue<jack_nframes_t, LinkedFuncs::zero> reset(KEY_BACKSPACE,
                                                            sequencer.frame);
static LinkedValue<bool, LinkedFuncs::boolSwitch> pauseSeq(KEY_SPACE,
                                                           sequencer.active);
static LinkedInput<clearSeqFunc> clearSeq(KEY_DELETE);
static LinkedInput<speedUpSeq> speedUp(KEY_PAGEUP);
static LinkedInput<speedDownSeq> speedDown(KEY_PAGEDOWN);
static LinkedInput<addSequence> addSeq(KEY_KPPLUS);

int process_sequencer(jack_nframes_t nframes, void *arg) {
  JackClient *c = (JackClient *)arg;
  c->update(nframes, true);

  readKeyboardInputData();

  prev.checkPressedClean(keyboardEvents);
  next.checkPressedClean(keyboardEvents);
  mute.checkPressedClean(keyboardEvents);

  randSeq.checkPressedClean(keyboardEvents);

  reset.checkPressedClean(keyboardEvents);
  pauseSeq.checkPressedClean(keyboardEvents);
  clearSeq.checkPressedClean(keyboardEvents);

  speedUp.checkPressedClean(keyboardEvents);
  speedDown.checkPressedClean(keyboardEvents);
  addSeq.checkPressedClean(keyboardEvents);

  midi.update();

  for (uint8_t btn = midi.keyboard.iter(0); btn != 0;
       btn = midi.keyboard.iter(btn)) {
    sequencer.setStep(clamp(uint8_t(0), btn, uint8_t(127)));
    midi.keyboard.clean(btn);
  }

  sequencer.step(nframes);

  static jack_midi_data_t midi_signal[] = {MidiSignal_SystemSignal, 0, 127};
  for (size_t i = 0; i < sequencer.sequences.len(); i++) {
    uint16_t last = sequencer.sequences[i].lastStep();
    if (last) {
      midi_signal[0] = MidiSignal_Released;
      midi_signal[1] = last;
      output[0].writeMidi(i, midi_signal, 3);
    }

    uint16_t step = sequencer.sequences[i].getStep();
    if (step) {
      midi_signal[0] = MidiSignal_Pressed;
      midi_signal[1] = step;
      output[0].writeMidi(i, midi_signal, 3);
      display.set(i, 0, '|');
    } else {
      display.set(i, 0, ' ');
    }
  }

  display.print();
  // c->ports[ 0 ].clear();

  return 0;
}

void SIGINT_handler(int pass) {
  client.writeConfig();
  shutdownInputs();
  exit(0);
}

int main(int argc, char *argv[]) {
  initKeyboardInput();

  initMidi(client.getSampleRate());

  sequencer = Sequencer(client.BPMToSamples(120) * 8);
  addSequence();

  client.start(process_sequencer);
  sleepProcess(NULL);

  shutdownInputs();
  return 0;
}