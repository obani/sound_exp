#include "../input/input.h"
#include "../input/linkedinput.h"

#include "../audio/jackclient.h"
#include "../audio/loopstation.h"

#include "../files/csv.h"

#include "../midi/midi.h"

#include "../util/math.h"
#include "../util/misc.h"
#include <signal.h>

static uint8_t NUM_TRACKS, FIRST_KEY_BTN, LAST_KEY_BTN;

static JackClient client("loopstation");
static LoopStation
    loopstation(client.getSampleRate(),
                client.initJackPorts(2, JackPortType_Audio, JackPortIsInput),
                client.initJackPorts(2, JackPortType_Audio, JackPortIsOutput));
static MidiInput midi(client.initJackPorts(1, JackPortType_Midi,
                                           JackPortIsInput)[0]);
// static int divider_step = 1;

// static void sub_min1( int * a ) { if( *a > 1 ) (*a)--; }

static void loopstation_activate() {
  printf("%sLoopstation Activate\n", ANSI_MOVE_UP_DELETE);
  loopstation.activate();
}
static void loopstation_deactivate() {
  printf("%sLoopstation Deactivate\n", ANSI_MOVE_UP_DELETE);
  loopstation.deactivate();
}
static void loopstation_reset() {
  printf("%sLoopstation reset\n", ANSI_MOVE_UP_DELETE);
  loopstation.reset();
}
static void track_record() {
  printf("%s%s\n", ANSI_MOVE_UP_DELETE,
         loopstation.trackRecord() ? "Record" : "Stop record");
}
static void track_clear() {
  printf("%sClear track [%s%ld%s]\n", ANSI_MOVE_UP_DELETE,
         ANSI_BOLD_INTENSE_BLUE, loopstation.trackClear() + 1, ANSI_RESET);
}
static void track_reset() {
  printf("%sReset track [%s%ld%s]\n", ANSI_MOVE_UP_DELETE,
         ANSI_BOLD_INTENSE_BLUE, loopstation.trackReset() + 1, ANSI_RESET);
}
static void track_prev() {
  printf("%sSelected track [%s%ld%s]\n", ANSI_MOVE_UP_DELETE,
         ANSI_BOLD_INTENSE_BLUE, loopstation.selectPrev() + 1, ANSI_RESET);
}
static void track_next() {
  printf("%sSelected track [%s%ld%s]\n", ANSI_MOVE_UP_DELETE,
         ANSI_BOLD_INTENSE_BLUE, loopstation.selectNext() + 1, ANSI_RESET);
}
// static void track_back( const void * pass ) { printf("%sBackward by %d\n",
// ANSI_MOVE_UP_DELETE, divider_step); loopstation.trackBackwardBy( divider_step
// ); } static void track_forw( const void * pass ) { printf("%sForward by
// %d\n", ANSI_MOVE_UP_DELETE, divider_step); loopstation.trackForwardBy(
// divider_step ); }
static void track_volume_link() {
  printf("%sLinking volume for track [%s%ld%s]\n", ANSI_MOVE_UP_DELETE,
         ANSI_BOLD_INTENSE_BLUE, loopstation.trackVolumeLink() + 1, ANSI_RESET);
}
static void track_pan_link() {
  printf("%sLinking pan for track [%s%ld%s]\n", ANSI_MOVE_UP_DELETE,
         ANSI_BOLD_INTENSE_BLUE, loopstation.trackPanLink() + 1, ANSI_RESET);
}

static bool check_active(uint8_t btn) {
  if (btn >= FIRST_KEY_BTN && btn < LAST_KEY_BTN) {
    size_t i = btn - FIRST_KEY_BTN;
    bool active = loopstation.trackActive(i);
    printf("%sTrack %ld %s%s%s\n", ANSI_MOVE_UP_DELETE, i + 1,
           active ? ANSI_BOLD_INTENSE_GREEN : ANSI_BOLD_INTENSE_RED,
           active ? "Activate" : "Deactivate", ANSI_RESET);
    return true;
  }
  return false;
}

static LinkedInput<loopstation_activate> loopActivate(KEY_F5);
static LinkedInput<loopstation_deactivate> loopDeactivate(KEY_F4);
static LinkedInput<loopstation_reset> loopReset(KEY_F1);

static LinkedInput<track_record> trackRecord(KEY_RIGHTSHIFT);
static LinkedInput<track_clear> trackClear(KEY_DELETE);
static LinkedInput<track_reset> trackReset(KEY_END);
static LinkedInput<track_prev> trackPrev(KEY_LEFT);
static LinkedInput<track_next> trackNext(KEY_RIGHT);
// static LinkedInput<const void> trackBack( KEY_DOWN, LinkedFuncs::null,
// track_back ); static LinkedInput<const void> trackForw( KEY_UP,
// LinkedFuncs::null, track_forw );
static LinkedInput<track_volume_link> trackVolumeLink(KEY_TAB);
static LinkedInput<track_pan_link> trackPanLink(KEY_LEFTSHIFT);
// static LinkedInput<int> trackDecDiv( KEY_PAGEDOWN, &divider_step, sub_min1 );
// static LinkedInput<int> trackIncDiv( KEY_PAGEUP, &divider_step,
// LinkedFuncs::add );

static int process(jack_nframes_t nframes, void *arg) {
  JackClient *c = (JackClient *)arg;
  c->update(nframes, true);

  readKeyboardInputData();
  midi.update();

  loopActivate.checkPressedClean(keyboardEvents);
  loopDeactivate.checkPressedClean(keyboardEvents);
  loopReset.checkPressedClean(keyboardEvents);
  trackRecord.checkPressedClean(keyboardEvents);
  trackClear.checkPressedClean(keyboardEvents);
  trackReset.checkPressedClean(keyboardEvents);
  trackPrev.checkPressedClean(keyboardEvents);
  trackNext.checkPressedClean(keyboardEvents);
  //	trackBack.checkPressedClean( keyboardEvents );
  // trackForw.checkPressedClean( keyboardEvents );
  //	trackDecDiv.checkPressedClean( keyboardEvents );
  // trackIncDiv.checkPressedClean( keyboardEvents );
  trackVolumeLink.checkPressedClean(keyboardEvents);
  trackPanLink.checkPressedClean(keyboardEvents);

  for (uint8_t btn = keyboardEvents.iter(0); btn != 0;
       btn = keyboardEvents.iter(btn)) {
    keyboardEvents.clean(btn, check_active(btn));
  }

  loopstation.step(nframes);

  return 0;
}

void SIGINT_handler(int pass) {
  client.writeConfig();
  shutdownInputs();
  exit(0);
}

int main(int argc, char *argv[]) {
  ArgcHelpOption options[] = {
      {"-c", "--config", "Use a config file to configure controllers"},
      {"-ip", "--input-playback",
       "Enable the playback of your audio input by the loopstation"},
      {}};

  ArgcHandler(argc >= 3, argc, argv,
              "<num tracks> <track max seconds> [options]", options);

  NUM_TRACKS = atoi(argv[1]);
  double max_time = atof(argv[2]);
  FIRST_KEY_BTN = KEY_1;
  LAST_KEY_BTN = FIRST_KEY_BTN + NUM_TRACKS;

  initKeyboardInput();

  signal(SIGINT, SIGINT_handler);

  initMidi(client.getSampleRate());
  client.readConfig();

  loopstation.setMaxSeconds(max_time);

  size_t num_samples = client.getSampleRate() * max_time;
  size_t total_samples = num_samples * NUM_TRACKS;

  printf("%u tracks with a max time of %.2f seconds (%ld samples)\n%ld total "
         "samples (roughly %.2fMB of data)\n\n",
         NUM_TRACKS, max_time, num_samples, total_samples,
         (total_samples * 2 * sizeof(jack_default_audio_sample_t)) /
             (1024.0 * 1024.0));

  loopstation.setController(&midi);

  for (size_t i = 0; i < NUM_TRACKS; i++) {
    printf("Adding track %ld\n", loopstation.addTrack());
  }
  printf("\n");

  loopstation.input_playback =
      HasArg(argc, argv, "-ip", "--input-playback", false);

  int cfg = HasArg(argc, argv, "-c", "--config", true);
  if (cfg) {
    const CSV csv(argv[cfg]);
    if (csv.isLoaded()) {
      for (size_t i = 0; i < csv.lines(); i++) {
        printf("Track %lu: ", i + 1);
        Option<ssize_t> volume = csv.getInt(i, 0);
        Option<ssize_t> pan = csv.getInt(i, 1);

        printf("linking volume to controller %ld", volume.val);
        loopstation.tracks[i].volume_control.setLink(volume.val);
        if (pan.exists) {
          printf(", linking pan to controller %ld", pan.val);
          loopstation.tracks[i].pan_control.setLink(pan.val);
        }

        printf("\n");
      }
    } else {
      printf("File %s couldn't be loaded", argv[cfg]);
    }
  }

  client.start(process);
  sleepProcess(NULL);

  shutdownInputs();

  return 0;
}