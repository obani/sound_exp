#include "../audio/jackclient.h"

#include "../input/input.h"
#include "../input/linkedinput.h"

#include "../files/audio.h"

#include "../util/math.h"
#include "../util/misc.h"
#include "../util/random.h"

#include "../synth/bank/additive.h"
#include "../synth/synth.h"

#include "../midi/midi.h"

JackClient client("asmrkeyboard");
JackPorts outputs(client.getJackPorts(JackPortType_Audio, JackPortIsOutput));
jack_default_audio_sample_t sample_rate = client.getSampleRate();

static MidiInput midi;
static Synth<Additive> synth(outputs);

static void randomize_synth(const void *pass) {
  Additive sb;
  for (size_t i = 0; i < 4; i++) {
    sb.addWave(
        PeriodicWave(
            Pan(randomized.Float11(), 0.5f), Wave_Sine + randomized.Int(2),
            randomized.Float(0.1f, 0.3f), randomized.Float(0.99f, 1.01f)),
        ADSR(sample_rate, randomized.Float(0.5f, 1.0f),
             randomized.Float(1.0f, 2.0f), 0.0, randomized.Float(4.0f, 8.0f)));
  }

  synth = Synth(sb, 8, outputs);
  synth.force_attack = true;
}

static uint8_t key = A2;
static bool minor = true;
static const int SUB1 = -1;
static const int PLUS1 = +1;

static void key_change(const int *v) {
  key += *v;

  printf("Changed to key %s\n", getKeyName(key, true));
}

static void switch_scale(const void *b) {
  minor = !minor;

  printf("Swicthed to %s scale\n", minor ? "minor" : "major");
}

static LinkedInput<const int> keyUp(KEY_PAGEUP, &PLUS1, key_change);
static LinkedInput<const int> keyDown(KEY_PAGEDOWN, &SUB1, key_change);
static LinkedInput<const void> minorSwitch(KEY_INSERT, LinkedFuncs::null,
                                           switch_scale);
static LinkedInput<const void> randSynth(KEY_DELETE, LinkedFuncs::null,
                                         randomize_synth);

static int process(jack_nframes_t nframes, void *arg) {
  JackClient *c = (JackClient *)arg;
  c->update(nframes, true);

  readKeyboardInputData();

  for (uint8_t btn = keyboardEvents.iter(0); btn != 0;
       btn = keyboardEvents.iter(btn)) {
    midi.addKey(minor ? minorScale(key, randomized.Int(24))
                      : majorScale(key, randomized.Int(24)),
                1.0f);
  }

  keyUp.checkPressedClean(&keyboardEvents);
  keyDown.checkPressedClean(&keyboardEvents);
  minorSwitch.checkPressedClean(&keyboardEvents);
  randSynth.checkPressedClean(&keyboardEvents);

  synth.step(nframes, &midi);
  midi.clear();

  return 0;
}

int main(int argc, char *argv[]) {
  initKeyboardInput();

  {
    initJackPorts(client, 2, JackPortType_Audio, JackPortIsOutput);

    sample_rate = client.getSampleRate();
    initMidi(sample_rate);
    randomize_synth(NULL);

    startJackClient(client, process);
    sleepProcess(NULL);
  }

  shutdownInputs();
  return 0;
}