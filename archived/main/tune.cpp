#include "../audio/jackclient.h"
#include <fftw3.h>
#include <signal.h>

JackClient client("tune");
JackPorts inputs(client.getJackPorts(JackPortType_Audio, JackPortIsInput));

constexpr size_t MAX_SIZE = 512 << 4;
constexpr size_t L = MAX_SIZE >> 1;

static fftw_complex input[MAX_SIZE];
static fftw_complex output[MAX_SIZE];
static double freqs[MAX_SIZE];
static fftw_plan plan;

static size_t step = 0;

static int tune(jack_nframes_t nframes, void *arg) {
  JackClient *c = (JackClient *)arg;
  c->update(nframes, true);

  size_t end = step + nframes;
  for (jack_nframes_t i = 0; i < nframes; step++, i++) {
    input[step][0] = inputs[0][i];
    input[step][1] = 0.0f;
  }

  if (end == MAX_SIZE) { // buffer filled
    step = 0;

    fftw_execute(plan);

    double max = -10.0f;
    size_t max_idx = 0;
    for (size_t i = 0; i < L; i++) {
      freqs[i] =
          sqrt(output[i][0] * output[i][0] + output[i][1] * output[i][1]);
      if (freqs[i] > max) {
        max = freqs[i];
        max_idx = i;
      }
    }

    if (max_idx != 0) {
      printf("%sMax around %ld\n", ANSI_MOVE_UP_DELETE,
             (max_idx * c->getSampleRate()) / MAX_SIZE);
    }
  }

  return 0;
}

void SIGINT_handler(int pass) {
  client.writeConfig();
  fftw_destroy_plan(plan);
  exit(0);
}

int main(int argc, char *argv[]) {
  signal(SIGINT, SIGINT_handler);

  plan = fftw_plan_dft_1d(MAX_SIZE, input, output, FFTW_FORWARD, FFTW_ESTIMATE);

  initJackPorts(client, 1, JackPortType_Audio, JackPortIsInput);
  client.readConfig();

  startJackClient(client, tune);
  sleepProcess(NULL);

  return 0;
}