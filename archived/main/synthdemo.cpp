#include "../util/math.h"
#include "../util/misc.h"
#include "../util/vector.h"

#include "../input/input.h"
#include "../input/linkedinput.h"

#include "../synth/bank/additive.h"
#include "../synth/bank/harmonics.h"
#include "../synth/bank/mod.h"

#include "../synth/blendsynth.h"
#include "../synth/synth.h"

#include "../midi/midi.h"
#include <signal.h>

constexpr size_t CHANNELS = 2;
using Wave = PeriodicWave<CHANNELS>;

static JackClient client("synthdemo");
static Slice<JackPort> outputs(client.initJackPorts(CHANNELS,
                                                    JackPortType_Audio,
                                                    JackPortIsOutput));
static float sample_rate = client.getSampleRate();

static MidiInput midi(client.initJackPorts(1, JackPortType_Midi,
                                           JackPortIsInput)[0]);

static float freq_modifier = 1.f;

static Synth<Additive<CHANNELS>> synth1(outputs);
static BlendSynth<CHANNELS> synth2(outputs);
static Synth<Harmonics<CHANNELS>> synth3(outputs);
static Synth<Mod<CHANNELS>> synth4(outputs);

static void mouseadd(jack_default_audio_sample_t &v, int16_t m) {
  v += m * 0.000001;
}

static size_t bank_size = 1;
static WaveType wave = Wave_Sine;
static int synthID = 0;

static ADSR randADSR() {
  return ADSR(sample_rate, randomized.Float(0.f, 0.1f), 0.f, 1.f,
              randomized.Float(0.1f, 3.f));
}

static void randomizeSynth1() {
  synth1 = Synth(Additive<CHANNELS>(), 10, outputs);

  for (size_t s = 0; s < synth1.synths.len(); s++) {
    Additive<CHANNELS> sb;

    for (size_t i = 0; i < bank_size; i++) {
      sb.addWave(
          Wave::rand(WaveType(randomized.Int(4)), 0.1f, 0.3f, 0.01f, 20.0f),
          randADSR());
    }

    synth1.setSynth(sb, s);
  }
}

static void randomizeSynth2() {
  synth2 = BlendSynth<CHANNELS>(BlendSynthBank<CHANNELS>(), 10, outputs);

  for (size_t s = 0; s < synth2.synths.len(); s++) {
    BlendSynthBank<CHANNELS> sb(randADSR());

    for (size_t i = 0; i < bank_size; i++) {
      sb.addWave(
          Wave::rand(WaveType(randomized.Int(4)), 0.2f, 0.3f, 0.01f, 20.f));
    }

    synth2.setSynth(sb, s);
  }
}

static void randomizeSynth3() {
  synth3 = Synth(Harmonics<CHANNELS>(), 10, outputs);

  for (size_t s = 0; s < synth2.synths.len(); s++) {
    Harmonics<CHANNELS> sb(randADSR());
    sb.setWave(wave);

    for (size_t i = 0; i < bank_size; i++) {
      sb.addHarmonic(randomized.Float(0.03f, 0.5f) / (i + 1),
                     Pan<CHANNELS>(randomized.Float11(), 0.5f),
                     randomized.Float(0.99f, 1.01f));
    }

    synth3.setSynth(sb, s);
  }
}

static void randomizeSynth4() {
  synth4 = Synth(Mod<CHANNELS>(), 10, outputs);

  for (size_t s = 0; s < synth2.synths.len(); s++) {
    Mod<CHANNELS> sb(randADSR());

    for (size_t i = 0; i < bank_size; i++) {
      if (i == (bank_size - 1)) {
        sb.addWave(Wave::rand(WaveType(randomized.Int(5)), 0.03f, 0.04f, 0.01f,
                              20.0f));
      } else {
        sb.addWave(
            Wave::rand(WaveType(randomized.Int(5)), 1.0f, 1.0f, 0.01f, 20.0f));
      }
    }

    synth4.setSynth(sb, s);
  }
}

static void switch_wave(WaveType &w) {
  w = (w + 1) % 4;

  if (w == Wave_Sine)
    printf("Switch to Sine wave\n");
  else if (w == Wave_Triangle)
    printf("Switch to Triangle wave\n");
  else if (w == Wave_Saw)
    printf("Switch to Triangle wave\n");
  else if (w == Wave_Square)
    printf("Switch to Square wave\n");

  randomizeSynth1();
}

static void add_bank(size_t &b) {
  if (b < 16) {
    b += 1;
    printf("Bank size %ld\n", b);
  }

  randomizeSynth1();
  randomizeSynth2();
  randomizeSynth3();
  randomizeSynth4();
}

static void sub_bank(size_t &b) {
  if (b > 1) {
    b -= 1;
    printf("Bank size %ld\n", b);
  }

  randomizeSynth1();
  randomizeSynth2();
  randomizeSynth3();
  randomizeSynth4();
}

static LinkedSignal<jack_default_audio_sample_t, mouseadd> pitch(freq_modifier);

static void switch_synth(int &id) {
  id = (id + 1) % 4;

  if (id == 0)
    printf("Additive\n");
  else if (id == 1)
    printf("BlendSynth\n");
  else if (id == 2)
    printf("Harmonics\n");
  else if (id == 3)
    printf("Mod\n");
}

static LinkedInput<randomizeSynth1> randSynth1(KEY_F1);
static LinkedInput<randomizeSynth2> randSynth2(KEY_F2);
static LinkedInput<randomizeSynth3> randSynth3(KEY_F3);
static LinkedInput<randomizeSynth4> randSynth4(KEY_F4);
static LinkedValue<int, switch_synth> switchSynth(KEY_INSERT, synthID);
static LinkedValue<WaveType, switch_wave> waveSwitch(KEY_BACKSPACE, wave);
static LinkedValue<size_t, add_bank> addBank(KEY_UP, bank_size);
static LinkedValue<size_t, sub_bank> subBank(KEY_DOWN, bank_size);

int synthdemo(jack_nframes_t nframes, void *arg) {
  JackClient *c = (JackClient *)arg;

  c->update(nframes, true);

  readMouseInputData();
  readJoystickInputData();
  readKeyboardInputData();

  if (input[INPUT_MOUSE].changed)
    pitch.update(mouseEvents.x);

  /*if( input[ INPUT_JOYSTICK ].changed ) {
          if( joystickEvents.analog ) {
                  pitch.check( j.btn, j.analog_signal );
          }
  }*/

  randSynth1.checkPressedClean(keyboardEvents);
  randSynth2.checkPressedClean(keyboardEvents);
  randSynth3.checkPressedClean(keyboardEvents);
  randSynth4.checkPressedClean(keyboardEvents);

  switchSynth.checkPressedClean(keyboardEvents);
  waveSwitch.checkPressedClean(keyboardEvents);

  addBank.checkPressedClean(keyboardEvents);
  subBank.checkPressedClean(keyboardEvents);

  midi.update();

  synth1.freq_modifier = freq_modifier;
  synth2.freq_modifier = freq_modifier;
  synth3.freq_modifier = freq_modifier;
  synth4.freq_modifier = freq_modifier;

  if (synthID == 0)
    synth1.step(nframes, &midi);
  else if (synthID == 1)
    synth2.step(nframes, &midi);
  else if (synthID == 2)
    synth3.step(nframes, &midi);
  else if (synthID == 3)
    synth4.step(nframes, &midi);

  return 0;
}

void SIGINT_handler(int pass) {
  client.writeConfig();
  shutdownInputs();
  exit(0);
}

int main(int argc, char *argv[]) {
  initKeyboardInput();
  initMouseInput();
  // initJoystickInput();

  initMidi(client.getSampleRate());

  signal(SIGINT, SIGINT_handler);
  client.readConfig();

  randomizeSynth1();
  randomizeSynth2();
  randomizeSynth3();
  randomizeSynth4();

  synthID = -1;
  switch_synth(synthID);

  client.start(synthdemo);
  sleepProcess(NULL);

  shutdownInputs();
  return 0;
}