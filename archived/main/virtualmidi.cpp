#include "../util/math.h"
#include "../util/misc.h"
#include "../util/vector.h"

#include "../input/input.h"
#include "../input/linkedinput.h"

#include "../midi/midi.h"

#include <stdarg.h>

static JackClient client("virtualmidi");
static Slice<JackPort> outputs(client.initJackPorts(1, JackPortType_Midi,
                                                    JackPortIsOutput));

static KeyVector midi_keyboard;
static int octave = 0;
static bool display = false;
static float wheel = 0.0f;
static float previous_wheel = 0.0f;
static float wheel_inertia = 0.8f;
static float wheel_factor = 0.01f;

static LinkedValue<int, LinkedFuncs::add> octaveUp(KEY_PAGEUP, octave);
static LinkedValue<int, LinkedFuncs::sub> octaveDown(KEY_PAGEDOWN, octave);

static void localPrint(const char *str, ...) {
  if (display) {
    va_list args;
    va_start(args, str);
    vprintf(str, args);
    va_end(args);
  }
}

int virtualmidi(jack_nframes_t nframes, void *arg) {
  JackClient *c = (JackClient *)arg;
  c->update(nframes, true);

  readKeyboardInputData();
  readMouseInputData();

  octaveUp.checkPressedClean(keyboardEvents);
  octaveDown.checkPressedClean(keyboardEvents);

  uint8_t num_events = 0;
  static jack_midi_data_t midi_signal[] = {MidiSignal_SystemSignal, 0, 127};
  for (uint8_t key = keyboardEvents.iter(0); key != 0;
       key = keyboardEvents.iter(key)) {
    uint8_t m = keyToMidi(key) + octave * 12;
    if (!m || midi_keyboard.pressed(m))
      continue;

    localPrint("Pressed [%d] (%s)\n", m, keyName(key));
    midi_signal[0] = MidiSignal_Pressed;
    midi_signal[1] = m;
    midi_signal[2] = 127;
    outputs[0].writeMidi(num_events++, midi_signal, 3);

    midi_keyboard.set(m);
  }

  if (mouseEvents.btn & MOUSE_LEFT) {
    wheel = clamp(-1.0f, wheel + mouseEvents.x * wheel_factor, 1.0f);
  } else {
    wheel *= wheel_inertia;
  }

  if (wheel != previous_wheel && abs(wheel) > 0.0001f) {
    uint8_t midi_wheel = (0.5f + wheel * 0.5f) * 127.0f;
    localPrint("Wheel %f% (%d)\n", wheel * 100, midi_wheel);
    midi_signal[0] = MidiSignal_Wheel;
    midi_signal[1] = 0;
    midi_signal[2] = midi_wheel;
    outputs[0].writeMidi(num_events++, midi_signal, 3);
    previous_wheel = wheel;
  }

  for (uint8_t m = midi_keyboard.iter(0); m != 0; m = midi_keyboard.iter(m)) {
    uint8_t key = MidiToKey(m - octave * 12);
    if (!key || keyboardEvents.pressed(key))
      continue;

    localPrint("Released [%d] (%s)\n", m, keyName(key));
    midi_signal[0] = MidiSignal_Released;
    midi_signal[1] = m;
    midi_signal[2] = 127;
    outputs[0].writeMidi(num_events++, midi_signal, 3);

    midi_keyboard.clean(m);
  }

  return 0;
}

void SIGINT_handler(int pass) {
  client.writeConfig();
  shutdownInputs();
  exit(0);
}

int main(int argc, char *argv[]) {
  ArgcHelpOption options[] = {{"-d", "--display", "Display inputs in terminal"},
                              {}};

  ArgcHandler(true, argc, argv, "[options]", options);
  display = HasArg(argc, argv, "-d", "--display", false);

  initKeyboardInput();
  initMouseInput();

  initMidi(client.getSampleRate());

  client.start(virtualmidi);
  sleepProcess(NULL);

  shutdownInputs();
  return 0;
}