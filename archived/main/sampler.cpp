#include "../input/input.h"
#include "../input/linkedinput.h"

#include "../audio/jackclient.h"
#include "../audio/sampler.h"

#include "../midi/midi.h"

#include "../files/audio.h"
#include "../files/csv.h"
#include "../files/walker.h"

#include "../util/args.h"
#include "../util/math.h"
#include "../util/string.h"
#include <signal.h>

enum {
  Mode_None,
  Mode_Pitch,
  Mode_Volume,

  Mode_Count
};

constexpr float mouse_editting_speed = 0.001f;
constexpr size_t CHANNELS = 2;

static JackClient client("sampler");
static Sampler<CHANNELS> sampler(client.initJackPorts(CHANNELS,
                                                      JackPortType_Audio,
                                                      JackPortIsOutput));
static MidiInput midi(client.initJackPorts(1, JackPortType_Midi,
                                           JackPortIsInput)[0]);
static FileWalker walker;

static String sample_path;

static uint8_t selecting = 0;
static size_t selected = 0;
static bool disabled = false;
static int mode = Mode_None;

static const char *current_sample_name() {
  return sampler.samples[selected].name.str();
}

const int PLUS1 = 1;
const int SUB1 = -1;
static void select(const int &add) {
  if (sampler.samples.len() == 0)
    return;

  if (add < 0 && selected == 0 && sampler.samples.len() != 0)
    selected = sampler.samples.len() - 1;
  else
    selected = (add + selected) % sampler.samples.len();

  printf("%sSelected : [%s%s%s]\n", ANSI_MOVE_UP_DELETE,
         ANSI_BOLD_INTENSE_GREEN, current_sample_name(), ANSI_RESET);
}

static void remove_sample() {
  if (sampler.samples.len() == 0)
    return;

  printf("%sRemoving sample [%s%s%s]\n", ANSI_MOVE_UP_DELETE,
         ANSI_BOLD_INTENSE_GREEN, current_sample_name(), ANSI_RESET);
  sampler.removeSample(selected, true);

  if (selected == sampler.samples.len() && sampler.samples.len() != 0)
    --selected;
}

static void switch_mode() {
  mode = (mode + 1) % Mode_Count;
  if (mode == Mode_None) {
    printf("%sStopped editing\n", ANSI_MOVE_UP_DELETE);
  } else {
    printf("%sSwitch to [%s%s%s] edit mode\n", ANSI_MOVE_UP_DELETE,
           ANSI_BOLD_INTENSE_GREEN, mode == Mode_Pitch ? "pitch" : "volume",
           ANSI_RESET);
  }
}

static LinkedValue<const int, select> selectPrev(KEY_PAGEUP, SUB1);
static LinkedValue<const int, select> selectNext(KEY_PAGEDOWN, PLUS1);
static LinkedInput<remove_sample> removeSample(KEY_BACKSPACE);
static LinkedInput<switch_mode> switchMode(KEY_F9);
static LinkedValue<bool, LinkedFuncs::boolSwitch> enableSampler(KEY_F10,
                                                                disabled);

static void addNewSample(const char *path, uint16_t ID) {
  if (sampler.addSample(path, ID)) {
    selected = sampler.samples.len() - 1;
    printf("%sAdded sample [%s%s%s] to id %d\n", ANSI_MOVE_UP_DELETE,
           ANSI_BOLD_INTENSE_GREEN, current_sample_name(), ANSI_RESET, ID);
  } else {
    printf("Couldn't open sample [%s%s%s]\n", ANSI_BOLD_INTENSE_GREEN, path,
           ANSI_RESET);
  }
}

static int process(jack_nframes_t nframes, void *arg) {
  JackClient *c = (JackClient *)arg;
  c->update(nframes, true);

  readKeyboardInputData();

  if (sampler.samples.len() != 0 && mode != Mode_None) {
    readMouseInputData();

    float *value = (mode == Mode_Pitch ? &sampler.samples[selected].speed
                                       : &sampler.samples[selected].volume);

    if (mouseEvents.btn & MOUSE_LEFT) {
      *value += mouseEvents.x * mouse_editting_speed;
    }

    if (mouseEvents.btn & MOUSE_RIGHT) { // reset
      *value = 1.0f;
    }
  }

  if (selecting != 0) {
    if (keyboardEvents.pressed(KEY_ESC)) {
      printf("Cancelled\n");
      selecting = 0;
      return 0;
    }

    walker.select(
        keyboardEvents.pressed(KEY_LEFT), keyboardEvents.pressed(KEY_RIGHT),
        keyboardEvents.pressed(KEY_UP), keyboardEvents.pressed(KEY_DOWN),
        keyboardEvents.pressed(KEY_ENTER), &sample_path);

    keyboardEvents.clear();

    if (sample_path.len() != 0) {
      addNewSample(sample_path.str(), selecting);
      selecting = 0;
    }
  }

  selectPrev.checkPressedClean(keyboardEvents);
  selectNext.checkPressedClean(keyboardEvents);
  removeSample.checkPressedClean(keyboardEvents);
  switchMode.checkPressedClean(keyboardEvents);
  enableSampler.checkPressedClean(keyboardEvents);

  if (disabled)
    return 0;

  midi.update();

  for (uint8_t btn = midi.keyboard.iter(0); btn != 0;
       btn = midi.keyboard.iter(btn)) {
    bool has_sample = sampler.play(btn, midi.keys[btn]);
    midi.keyboard.clean(btn);

    if (!has_sample) {
      printf("Sample %d not found\n", btn);
      if (selecting != 0) {
        printf("Already selecting a sample, can't select another\n");
      } else {
        printf("Do you want to choose one ?\n");
        keyboardEvents.clear();

        selecting = btn;
        sample_path = "";
        walker.getFilesList();
        break;
      }
    }
  }

  sampler.step(nframes);

  return 0;
}

void SIGINT_handler(int pass) {
  client.writeConfig();
  shutdownInputs();
  exit(0);
}

int main(int argc, char *argv[]) {
  ArgcHelpOption options[] = {
      {"-c", "--config", "Use a config file to add samples"}, {}};

  int cfg = HasArg(argc, argv, "-c", "--config", true);
  ArgcHandler(cfg == 0 || cfg != argc, argc, argv, "[options]", options);

  if (cfg) {
    CSV csv(argv[cfg]);

    for (size_t i = 0; i < csv.lines(); i++) {
      if (csv.getCells(i) < 2)
        continue;

      Option<const char *> path = csv.getString(i, 0);
      Option<ssize_t> id = csv.getInt(i, 1);
      Option<double> volume = csv.getFloat(i, 2);

      if (!volume.exists) {
        volume = 1.0;
      }

      if (sampler.addSample(path.val, id.val, volume.val)) {
        printf("Adding sample [%s%s%s] to id %ld with volume %f\n",
               ANSI_BOLD_INTENSE_GREEN, path.val, ANSI_RESET, id.val,
               volume.val);
      } else {
        printf("Couldn't open sample [%s%s%s]\n", ANSI_BOLD_INTENSE_GREEN,
               path.val, ANSI_RESET);
      }
    }
  }

  initKeyboardInput();
  initMouseInput();

  signal(SIGINT, SIGINT_handler);

  client.readConfig();

  String p = getCurrentDirectory();
  walker.setPath(p.str());
  walker.suffix = ".wav";

  client.start(process);
  sleepProcess(NULL);

  return 0;
}