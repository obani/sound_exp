#include "../input/input.h"
#include "../input/linkedinput.h"

#include "../files/audio.h"
#include "../files/walker.h"

#include "../util/math.h"
#include "../util/vector.h"

enum {
  Mode_Hold,
  Mode_Stop,

  Mode_Count
};

struct Track {
  SoundRAW sound;
  float speed, volume;
  bool paused;
};

static uint8_t CHANNELS = 1;
static JackClient client("scratch");
static Slice<JackPort> output(client.initJackPorts(CHANNELS, JackPortType_Audio,
                                                   JackPortIsOutput));

static int mode = Mode_Hold;

static vector<Track> tracks;

static FileWalker walker;
static size_t selected = 0;
static bool selecting = false;
static String sample_path;

static bool scratching = false;
static bool global_scratching = false;
static float base_speed = 1.0f;
static float speed = base_speed;
static float inertia = 0.1f;
static float mouse_speed = 0.002f;

const int PLUS1 = 1;
const int SUB1 = -1;
static void select(const int &add) {
  if (tracks.size() != 0)
    selected = clamp(0UL, selected + add, tracks.size() - 1);
  printf("%sSelected : %ld\n", ANSI_MOVE_UP_DELETE, selected);
}

static void switch_mode() {
  mode = (mode + 1) % Mode_Count;
  printf("Switched to scract mode %s\n", mode == Mode_Hold ? "Hold" : "Stop");
}

static void del_sound() {
  if (tracks.size() == 0)
    return;

  tracks[selected].sound.free();
  tracks.remove(selected);
  printf("Removed sound %ld\n", selected);

  if (selected == tracks.size() && tracks.size() != 0)
    selected--;
}

static void add_sound() {
  selecting = true;
  sample_path = "";
  walker.getFilesList();
}

static void restart_sound() {
  if (tracks.size() != 0)
    tracks[selected].sound.offset = 0;
}

static void pause_sound() {
  if (tracks.size() != 0)
    tracks[selected].paused = !tracks[selected].paused;
}

static LinkedValue<bool, LinkedFuncs::boolTrue> scratch(KEY_SPACE, scratching);
static LinkedValue<bool, LinkedFuncs::boolTrue>
    globalScratch(KEY_LEFTCTRL, global_scratching);

static LinkedValue<const int, select> selectPrev(KEY_UP, SUB1);
static LinkedValue<const int, select> selectNext(KEY_DOWN, SUB1);
static LinkedInput<switch_mode> switchMode(KEY_TAB);
static LinkedInput<del_sound> delSound(KEY_DELETE);
static LinkedInput<add_sound> addSound(KEY_INSERT);
static LinkedInput<restart_sound> restartSound(KEY_BACKSPACE);
static LinkedInput<pause_sound> pauseSound(KEY_ENTER);

static void apply_scratch(bool cond, float *speed, const MouseInput &m) {
  if (cond) {
    if (input[INPUT_MOUSE].changed)
      *speed += m.x * mouse_speed;
    else if (mode == Mode_Stop)
      *speed -= (*speed) * inertia;
  } else {
    *speed += (base_speed - (*speed)) * inertia;
  }
}

// should work only for mono and stereo files
int process_scratch(jack_nframes_t nframes, void *arg) {
  JackClient *c = (JackClient *)arg;
  c->update(nframes, true);

  readMouseInputData();
  readKeyboardInputData();

  if (selecting) {
    if (keyboardEvents.pressed(KEY_ESC)) {
      printf("Cancelled\n");
      selecting = 0;
      return 0;
    }

    walker.select(
        keyboardEvents.pressed(KEY_LEFT), keyboardEvents.pressed(KEY_RIGHT),
        keyboardEvents.pressed(KEY_UP), keyboardEvents.pressed(KEY_DOWN),
        keyboardEvents.pressed(KEY_ENTER), &sample_path);

    keyboardEvents.clear();

    if (sample_path.size() != 0) {
      tracks.push_back(
          {openRAWAudioFile(sample_path.str()), base_speed, 1.0f, true});
      if (selected != 0 && selected == tracks.size()) {
        selected--;
      }
      selecting = false;
    }
  }

  scratching = (mouseEvents.btn & MOUSE_LEFT);
  global_scratching = false;

  scratch.checkPressed(keyboardEvents);
  globalScratch.checkPressed(keyboardEvents);

  selectPrev.checkPressedClean(keyboardEvents);
  selectNext.checkPressedClean(keyboardEvents);
  switchMode.checkPressedClean(keyboardEvents);
  delSound.checkPressedClean(keyboardEvents);
  addSound.checkPressedClean(keyboardEvents);
  restartSound.checkPressedClean(keyboardEvents);
  pauseSound.checkPressedClean(keyboardEvents);

  if (tracks.size() == 0)
    return 0;

  apply_scratch(global_scratching, &speed, mouseEvents);

  for (size_t i = 0; i < tracks.size(); i++) {
    if (tracks[i].paused)
      continue;

    apply_scratch(selected == i && scratching && !global_scratching,
                  &tracks[i].speed, mouseEvents);

    jack_nframes_t frames_written = 0;
    jack_nframes_t start_idx = 0;
    jack_nframes_t expected_frames = nframes;
    float s = speed * tracks[i].speed;

    while (frames_written != expected_frames) {
      expected_frames = nframes - frames_written;

      start_idx += frames_written;
      for (int chan = 0; chan < CHANNELS; chan++) {
        frames_written = tracks[i].sound.toPort(output[chan], start_idx,
                                                nframes, s, chan, 1.0f);
      }

      tracks[i].sound.offset =
          (frames_written != expected_frames)
              ? ((s > 0.0f) ? 0 : tracks[i].sound.num_samples - 1)
              : tracks[i].sound.offset + frames_written * s;
    }
  }

  return 0;
}

int main(int argc, char *argv[]) {
  initKeyboardInput();
  initMouseInput();

  String p = getCurrentDirectory();
  walker.setPath(p.str());
  walker.suffix = ".wav";

  client.start(process_scratch);
  sleepProcess(NULL);

  shutdownInputs();
  return 0;
}
