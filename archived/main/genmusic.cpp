#include "../audio/jackclient.h"

#include "../util/math.h"
#include "../util/vector.h"

#include "../files/audio.h"

#include "../synth/bank/additive.h"
#include "../synth/synth.h"

#include "../audio/sampler.h"
#include "../midi/midi.h"
#include "../midi/sequencer.h"

enum {
  MUSIC_INTRO,
  MUSIC_A,
  MUSIC_B,
  MUSIC_C,
  MUSIC_D,
  MUSIC_END,
};

static MidiInput midi;
static Sequencer music;
static vector<Sequencer> loops;
static vector<Synth<Additive>> long_synths;
static vector<Synth<Additive>> short_synths;
static Sampler sampler;

static uint8_t BASIS = 0;
static jack_nframes_t LOOP_STEPS = 0;
static jack_nframes_t LOOP_SIZE = 0;

static void GenMusic(JackClient *client) {
  music.sequences.clear();
  loops.clear();
  long_synths.clear();
  short_synths.clear();

  BASIS = randomized.Int(A4, A5);
  int BPM = randomized.Int(70, 140);
  LOOP_STEPS = 16;
  LOOP_SIZE = client->BPMToSamples(BPM) * LOOP_STEPS;

  size_t num_loops = 17;
  music.setLength(LOOP_SIZE * num_loops);

  Sequence music_sequence(num_loops, "music_sequence");
  {
    size_t last_idx = music_sequence.steps.len() - 2;

    music_sequence.steps.set_size(num_loops);
    music_sequence.steps[0] = MUSIC_INTRO;
    for (size_t i = 1; last_idx; i++) {
      music_sequence.steps[randomized.Int(MUSIC_INTRO + 1, MUSIC_END)];
    }
    music_sequence.steps[last_idx] = MUSIC_INTRO;
    music_sequence.steps[music_sequence.steps.len() - 1] = MUSIC_END;
  }

  music.sequences.push_back(music_sequence);
}

int main(int argc, char *argv[]) {
  JackClient client("gen_music");
  initMidi(client.getSampleRate());
  initJackPorts(&client, 2, JackPortType_Audio, JackPortIsOutput);
  initJackPorts(&client, 2, JackPortType_Audio, JackPortIsInput);

  GenMusic(&client);

  //		startJackClient( &client, process_gen_music );

  sleepProcess(NULL);

  return 0;
}