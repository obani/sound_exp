#ifndef _MY_BLENDSYNTH
#define _MY_BLENDSYNTH

#include "synth.h"
#include "synthbank.h"

template <size_t CHANNELS, size_t SIZE>
struct BlendSynthBank : __SynthBank<CHANNELS, SIZE> {
private:
  using Bank = __SynthBank<CHANNELS, SIZE>;
  using Wave = PeriodicWave<CHANNELS>;

public:
  ADSR envelope;
  std::array<WaveType, SIZE> types;
  float blending = 0.f;

  bool isFinished() override { return envelope.finished(); }

  jack_nframes_t getFrame() override { return envelope.frame; }

  void custom_start() override { envelope.start(); }

  void step(Slice<JackPort> &outputs, jack_nframes_t nframes,
            float freq_modifier) override {
    if (envelope.finished())
      return;

    pfloat f = freq_modifier * Bank::freq;

    constexpr float synth_lenf = SIZE;
    size_t from = blending * synth_lenf;
    size_t to = from + 2;
    float blending_unit = (blending * synth_lenf - float(from));

    float signal, env_signal, blend_volume;
    envelope.checkHeld(Bank::pressed);

    for (jack_nframes_t i = 0; i < nframes; ++i) {
      env_signal = envelope.step() * Bank::volume;

      if (envelope.finished())
        break;

      for (size_t j = from; j < to; ++j) {
        size_t s = j % SIZE;
        Wave &wave = Bank::waves[s];

        blend_volume = j == from ? 1.f - blending_unit : blending_unit;

        signal =
            wave.volume * env_signal * blend_volume * wave.step(f, types[s]);

        for (uint16_t chan = 0; chan < CHANNELS; ++chan)
          outputs[chan][i] += signal * wave.pan[chan];
      }
    }
  }
};

template <size_t CHANNELS, size_t POLYPHONY, size_t BANK_SIZE>
struct BlendSynth : Synth<BlendSynthBank<CHANNELS, BANK_SIZE>, POLYPHONY> {
protected:
  using Bank = BlendSynthBank<CHANNELS, BANK_SIZE>;
  using __Synth = Synth<Bank, POLYPHONY>;

  void stepCallback(size_t i) override {
    __Synth::synths[i].blending = blending;
  }

public:
  float blending;

  BlendSynth(const Slice<JackPort> &o) : __Synth(o), blending(0.f){};
};

#endif //_MY_BLENDSYNTH