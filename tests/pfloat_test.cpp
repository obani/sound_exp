#include "../util/pfloat.h"
#include <catch2/catch_all.hpp>

TEST_CASE("Benchmark pfloat creation", "") {
  float f = 0.00001f;
  pfloat pf(f);

  BENCHMARK("float += float") {
    for (int i = 0; i < 100; i++)
      f += f;
    return &f;
  };

  BENCHMARK("pfloat += pfloat") {
    for (int i = 0; i < 100; i++)
      pf += pf;
    return &pf;
  };

  BENCHMARK("float *= float") {
    for (int i = 0; i < 100; i++)
      f *= f;
    return &f;
  };

  BENCHMARK("pfloat *= float") {
    for (int i = 0; i < 100; i++)
      pf *= f;
    return &pf;
  };
}