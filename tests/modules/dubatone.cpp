#include "../../modules/dubatone/continuouswave.h"
#include <catch2/catch_all.hpp>

TEST_CASE("Oscillators", "osc") {
  float f = 0.4f;
  float m = 0.125f;

  Dubatone::Oscillator<Dubatone::sineWave> sine;
  Dubatone::Oscillator<Dubatone::triangleWave> triangle;
  Dubatone::Oscillator<Dubatone::sawWave> saw;
  Dubatone::NoiseOscillator noise;

  BENCHMARK("sine") { return sine.step(f, m) + sine.step(f, m); };

  BENCHMARK("triangle") { return triangle.step(f, m) + triangle.step(f, m); };

  BENCHMARK("saw") { return saw.step(f, m) + saw.step(f, m); };

  BENCHMARK("noise") { return noise.step(f, m) + noise.step(f, m); };
}

TEST_CASE("Synth functions", "waves") {
  float f = 0.25f;
  float m = 0.125f;

  BENCHMARK("sineWave") { return Dubatone::sineWave(f, m); };

  BENCHMARK("sawWave") { return Dubatone::sawWave(f, m); };

  BENCHMARK("triangleWave") { return Dubatone::triangleWave(f, m); };
}