CMD=go run tools/simple-go-ninja-builder/make_ninja.go "build"
LIBS="-ljack -lm -lpthread -lfftw3 -ffast-math"
CPPFLAGS="--std=c++23 -Wall -O3 -mbmi -mbmi2 -mlzcnt"
EXT=".cpp" ".o"

ninja :
	$(CMD) $(LIBS) $(CPPFLAGS) \
	"g++ -O3 -fdiagnostics-color=always -fconcepts-diagnostics-depth=5" \
	$(EXT)

asan:
	$(CMD) $(LIBS) $(CPPFLAGS) \
	"g++ -O0 -g -fdiagnostics-color=always -fsanitize=address -fsanitize-address-use-after-scope" \
	$(EXT)

debug :
	$(CMD) $(LIBS) $(CPPFLAGS) \
	"g++ -O0 -g -fdiagnostics-color=always" \
	$(EXT)

clean:
	ninja -t clean

clear : clean
	rm -rf build.ninja build/.ninja_deps build/.ninja_log *.connections build/*