#include "common.h"

#include <dirent.h>
#include <errno.h>
#include <fcntl.h>
#include <string.h>
#include <sys/stat.h>

constexpr const char *CONFIG_FOLDER = ".config";
constexpr const char *PROGRAM_CONFIG_FOLDER = "sound_exp";

static const String CONFIG_PATH([]() {
  String path;

  path += getenv("HOME");
  path += "/";
  path += CONFIG_FOLDER;
  path += "/";

  if (createDir(path.str()) != 0) { // ~/.config/
    path = "";
    return path;
  }

  path += PROGRAM_CONFIG_FOLDER;
  path += "/";

  if (createDir(path.str()) != 0) // ~/.config/sound_exp/
    path = "";

  return path;
}());

bool fileExists(const char *path) { return access(path, F_OK) != -1; }

int createDir(const char *path) {
  DIR *dir = opendir(path);
  if (dir == NULL && errno == ENOENT)
    return mkdir(path, S_IRWXU);

  return 0;
}

const char *getConfigPath() { return CONFIG_PATH.str(); }

String getConfigSubPath(const char *subfolder) {
  if (strLen(subfolder) == 0)
    return String(CONFIG_PATH);

  String path = CONFIG_PATH;
  path += subfolder;
  path += "/";

  if (createDir(path.str()) != 0) // ~/.config/sound_exp/
    path = "";

  return path;
}

bool readFileToString(const char *path, String &str) {
  constexpr size_t BUFFER_SIZE = 4096;
  char tmp[BUFFER_SIZE + 1] = {};

  str = "";

  int fd = open(path, O_RDONLY);
  if (fd == -1)
    return false;

  bool keep_going = true;
  while (keep_going) {
    ssize_t r = read(fd, tmp, BUFFER_SIZE);
    if (r != BUFFER_SIZE) {
      tmp[r + 1] = '\0';
      keep_going = false;
    }

    str += tmp;
  }

  close(fd);
  return true;
}

ssize_t writeStringToFile(const char *path, const String &str, bool overwrite) {
  int fd = creat(path, S_IRUSR | S_IWUSR | S_IRGRP | S_IWGRP);
  if (fd < 0)
    return fd;

  ssize_t w = write(fd, str.str(), str.size());
  close(fd);
  return w;
}

String getCurrentDirectory() {
  char cwd[PATH_MAX];

  String base_path;
  if (getcwd(cwd, sizeof(cwd)) != NULL) {
    base_path = cwd;
    base_path += '/';
  }

  return base_path;
}

vector<vector<String>> readCSV(const char *path) {
  constexpr size_t BUF_SIZE = 4096;

  int fd = open(path, O_RDONLY, 0644);
  if (fd == -1)
    return vector<vector<String>>();

  char buffer[BUF_SIZE] = {};
  String file_out;
  while (read(fd, buffer, BUF_SIZE) > 0) {
    file_out += buffer;
    memset(buffer, 0, BUF_SIZE);
  }

  vector<String> lines = file_out.split("\n");
  vector<vector<String>> output;

  for (size_t i = 0; i < lines.size(); i++)
    output.push_back(lines[i].split(","));

  return output;
}