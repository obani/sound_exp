#pragma once

#include "../util/math.h"
#include "../util/string.h"
#include "../util/vector.h"

#include "../files/common.h"

class FileWalker {
private:
  struct DirElement {
    String name;
    uint8_t type;
  };

  String base_path;
  vector<DirElement> filesList;
  vector<String> lastOnRight;

  bool show_hidden, selected_changed;
  size_t selected;

  static void printFile(DirElement &e, size_t idx, bool delete_line) {
    printf(
        "%s%s%ld : %s%s\n", (delete_line ? ANSI_MOVE_UP_DELETE : ""),
        (e.type == DT_DIR ? ANSI_BOLD_INTENSE_BLUE : ANSI_BOLD_INTENSE_GREEN),
        idx, e.name.str(), ANSI_RESET);
  }

  void _select(String *out) {
    bool dir = filesList[selected].type == DT_DIR;
    if (dir) {
      system("clear");
    }

    printf("Selected %s '%s%s%s'\n", (dir ? "directory" : "file"),
           (dir ? ANSI_BOLD_INTENSE_BLUE : ANSI_BOLD_INTENSE_GREEN),
           filesList[selected].name.str(), ANSI_RESET);

    if (dir) {
      lastOnRight.clear();
      base_path += filesList[selected].name;
      base_path += '/';
      getFilesList();
    } else {
      (*out) = base_path;
      (*out) += filesList[selected].name;
    }
  }

public:
  String suffix;

  FileWalker() : show_hidden{false}, selected_changed{false}, selected{0} {}
  FileWalker(const char *p)
      : base_path{p}, show_hidden{false}, selected_changed{false}, selected{0} {
  }
  FileWalker(FileWalker &cpy)
      : base_path{cpy.base_path}, filesList{cpy.filesList},
        lastOnRight{cpy.lastOnRight}, show_hidden{cpy.show_hidden},
        selected{cpy.selected}, suffix{cpy.suffix} {}

  FileWalker &operator=(FileWalker &rhs) {
    base_path = rhs.base_path;
    filesList = rhs.filesList;
    lastOnRight = rhs.lastOnRight;
    show_hidden = rhs.show_hidden;
    selected = rhs.selected;
    suffix = rhs.suffix;
    return *this;
  }

  void setPath(const char *p) { base_path = p; }

  void showHidden(bool show) {
    show_hidden = show;
    getFilesList();
  }

  void getFilesList() {
    filesList.clear();

    struct dirent *de;

    DIR *dr = opendir(base_path.str());

    if (dr == NULL)
      return;

    while ((de = readdir(dr)) != NULL) {
      if ((de->d_type == DT_DIR &&
           (show_hidden || !hasPrefix(de->d_name, "."))) ||
          (hasSuffix(de->d_name, suffix.str()) && de->d_type == DT_REG)) {
        filesList.push_back({de->d_name, de->d_type});
        printFile(filesList[filesList.size() - 1], filesList.size() - 1, false);
      }
    }

    if (filesList.size() == 0) {
      printf("Directory was empty, going back\n");
      goLeft();
      return;
    }

    printf("--------------------------------\n\n");
  }

  void goLeft() {
    vector<String> split = base_path.split("/");

    if (split.size() <= 1)
      return;

    size_t to = split.size() - 1;
    base_path = "/";
    for (size_t i = 0; i < to; i++) {
      base_path += split[i];
      base_path += '/';
    }

    lastOnRight.push_back(split[split.size() - 1]);
    getFilesList();
  }

  void goRight(String *out) {
    if (selected_changed ||
        (lastOnRight.size() == 0 && filesList[selected].type == DT_DIR)) {
      _select(out);
    }
    if (lastOnRight.size() == 0) {
      return;
    }

    String last_visited = lastOnRight.pop_back();
    base_path += last_visited;
    base_path += '/';

    getFilesList();
  }

  void select(bool left, bool right, bool up, bool down, bool select,
              String *out) {
    if (left) {
      goLeft();
      selected_changed = false;
    } else if (right) {
      goRight(out);
      selected_changed = false;
    } else if (up || down) {
      selected = (selected == 0 && up) ? 0
                                       : clamp(0UL, selected + (up ? -1 : +1),
                                               filesList.size() - 1);
      selected_changed = true;
      printFile(filesList[selected], selected, true);
    } else if (select) {
      _select(out);
      selected_changed = false;
    }
  }
};