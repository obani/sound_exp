#pragma once

#include "../util/string.h"
#include "../util/vector.h"

bool fileExists(const char *path);
int createDir(const char *path);
const char *getConfigPath(const char *subfolder);
String getConfigSubPath(const char *subfolder);
bool readFileToString(const char *path, String &str);
ssize_t writeStringToFile(const char *path, const String &str, bool overwrite);
String getCurrentDirectory();