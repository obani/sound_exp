#pragma once

#include "../audio/sound.h"
#include <unistd.h>

void openRAWFile(SoundRAW *s, const char *path);
Sound openAudioFile(const char *path);
SoundRAW openRAWAudioFile(const char *path);
void printAudioSpecs(const SoundRAW &s);