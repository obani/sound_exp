#include "audio.h"

#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/mman.h>
#include <sys/stat.h>

#include <tuple>

template <typename T>
static void readRAWData(FILE *audio_file, const SoundRAW &sound, T *data) {
  uint8_t bytes_in_channel = (sound.bits_per_sample / 8);
  uint64_t size_of_each_sample = sound.channels * bytes_in_channel;
  int8_t data_buffer[size_of_each_sample];
  int read = 0;

  for (uint64_t i = 0; i < sound.num_samples; i++) {
    read = fread(data_buffer, size_of_each_sample, 1, audio_file);
    if (read != 1) {
      printf("Error reading file. %d bytes\n", read);
      break;
    }

    // dump the data read
    int offset = 0; // move the offset for every iteration in the loop below
    uint64_t pos = i * sound.channels;
    for (uint8_t chan = 0; chan < sound.channels; chan++) {
      switch (sound.bits_per_sample) {
      case 8:
        data[pos + chan] = data_buffer[offset] - 128;
        break;
      case 16:
        data[pos + chan] =
            (data_buffer[offset] & 0xff) | (data_buffer[offset + 1] << 8);
        break;
      case 24:
        data[pos + chan] = (data_buffer[offset] & 0xff) |
                           ((data_buffer[offset + 1] & 0xff) << 8) |
                           (data_buffer[offset + 2] << 16);
        break;
      case 32:
        data[pos + chan] = (data_buffer[offset] & 0xff) |
                           ((data_buffer[offset + 1] & 0xff) << 8) |
                           ((data_buffer[offset + 2] & 0xff) << 16) |
                           (data_buffer[offset + 3] << 24);
        break;
      }

      offset += bytes_in_channel;

      if (data[pos + chan] < sound.low_limit ||
          data[pos + chan] > sound.high_limit)
        printf("**value out of range (%d [%d,%d])\n", data[pos + chan],
               sound.low_limit, sound.high_limit);
    }
  }
}

static void readData(FILE *audio_file, const Sound &sound,
                     uint8_t bits_per_sample) {
  uint8_t bytes_in_channel = (bits_per_sample / 8);
  uint64_t size_of_each_sample = sound.channels * bytes_in_channel;
  int8_t data_buffer[size_of_each_sample];
  int read = 0;

  float factor = 1.f / float(setLimits<AudioLimits::High>(bits_per_sample));

  for (uint64_t i = 0; i < sound.num_samples; i++) {
    read = fread(data_buffer, size_of_each_sample, 1, audio_file);
    if (read != 1) {
      printf("Error reading file. %d bytes\n", read);
      break;
    }

    // dump the data read
    int offset = 0; // move the offset for every iteration in the loop below
    for (uint8_t chan = 0; chan < sound.channels; chan++) {
      int64_t sample = 0;
      switch (bits_per_sample) {
      case 8:
        sample = data_buffer[offset] - 128;
        break;
      case 16:
        sample = (data_buffer[offset] & 0xff) | (data_buffer[offset + 1] << 8);
        break;
      case 24:
        sample = (data_buffer[offset] & 0xff) |
                 ((data_buffer[offset + 1] & 0xff) << 8) |
                 (data_buffer[offset + 2] << 16);
        break;
      case 32:
        sample = (data_buffer[offset] & 0xff) |
                 ((data_buffer[offset + 1] & 0xff) << 8) |
                 ((data_buffer[offset + 2] & 0xff) << 16) |
                 (data_buffer[offset + 3] << 24);
        break;
      }

      float &v = sound.samples[chan][i + 1];
      v = float(sample) * factor;

      offset += bytes_in_channel;

      if (v < -1.0f || v > 1.0f)
        printf("**value out of range (%f)\n", v);
    }
  }
}

constexpr uint32_t readToU32(const char *buffer) {
  return (uint8_t)(buffer[0]) | ((uint8_t)(buffer[1]) << 8) |
         ((uint8_t)(buffer[2]) << 16) | ((uint8_t)(buffer[3]) << 24);
}

static uint16_t readFileHeaderToU16(FILE *audio_file) {
  char buffer[2];
  fread(buffer, 2, 1, audio_file);

  return (uint8_t)(buffer[0]) | ((uint8_t)(buffer[1]) << 8);
}

static uint32_t readFileHeaderToU32(FILE *audio_file) {
  char buffer[HEADER_TEXT];
  fread(buffer, HEADER_TEXT, 1, audio_file);

  return readToU32(buffer);
}

constexpr uint32_t RIFF = readToU32("RIFF");
constexpr uint32_t WAVE = readToU32("WAVE");
constexpr uint32_t fmt = readToU32("fmt "); // Trailing space is important
constexpr uint32_t data = readToU32("data");

static std::tuple<uint64_t, uint32_t, uint16_t, uint16_t>
openAudio(FILE *audio_file, const char *path) {
  constexpr std::tuple<uint64_t, uint32_t, uint16_t, uint16_t> null = {
      uint64_t(0), uint32_t(0), uint16_t(0), uint16_t(0)};
  FMT format = {};

  if (audio_file == NULL) {
    printf("Error opening file '%s'\n", path);
    return null;
  }

  if (readFileHeaderToU32(audio_file) != RIFF) {
    printf("No RIFF declaration found for '%s'\n", path);
    return null;
  }

  /*uint32_t overall_size = */ readFileHeaderToU32(audio_file); // UNUSED

  if (readFileHeaderToU32(audio_file) != WAVE) {
    printf("No WAVE declaration found for '%s'\n", path);
    return null;
  }

  uint32_t chunk = 0;
  while ((chunk = readFileHeaderToU32(audio_file)) != data) {
    uint32_t chunk_length = readFileHeaderToU32(audio_file);
    switch (chunk) {
    case fmt: {
      format.format_type = readFileHeaderToU16(audio_file);
      format.channels = readFileHeaderToU16(audio_file);
      format.sample_rate = readFileHeaderToU32(audio_file);
      format.byterate = readFileHeaderToU32(audio_file);
      format.block_align = readFileHeaderToU16(audio_file);
      format.bits_per_sample = readFileHeaderToU16(audio_file);
    } break;
    default: { // skip chunk
      fseek(audio_file, chunk_length, SEEK_CUR);
    } break;
    }
  }

  if (format.format_type != FORMAT_PCM) {
    printf("Didn't find PCM format\n");
    return null;
  }

  uint32_t data_size = readFileHeaderToU32(audio_file);
  uint16_t bytes_in_channel = format.bits_per_sample / 8;

  return {data_size / (bytes_in_channel * format.channels), format.sample_rate,
          format.bits_per_sample, format.channels};
}

// reads WAV, not BWAV
SoundRAW openRAWAudioFile(const char *path) {
  FILE *file = fopen(path, "rb");
  auto [ns, sr, bps, c] = openAudio(file, path);
  SoundRAW sound(ns, sr, bps, c);

  switch (bps) {
  case 8:
    readRAWData(file, sound, sound.i8);
    break;
  case 16:
    readRAWData(file, sound, sound.i16);
    break;
  default:
    readRAWData(file, sound, sound.i32);
    break;
  }

  fclose(file);
  sound.offset = 0;
  return sound;
}

Sound openAudioFile(const char *path) {
  FILE *file = fopen(path, "rb");
  auto [ns, sr, bps, c] = openAudio(file, path);
  Sound sound(ns, sr, c);

  readData(file, sound, bps);

  fclose(file);
  sound.offset = 0;
  return sound;
}

void openRAWFile(SoundRAW *sound, const char *path) {
  struct stat stats;
  int fd = open(path, O_RDONLY);
  fstat(fd, &stats);

  if (fd == -1 || S_ISREG(stats.st_mode) == 0) {
    printf("%s is a directory\n", path);
    close(fd);
    return;
  }

  int8_t *data =
      (int8_t *)mmap(NULL, stats.st_size, PROT_READ, MAP_PRIVATE, fd, 0);

  sound->free();
  sound->i8 = new int8_t[stats.st_size];
  memcpy(sound->i8, data, stats.st_size);
  sound->num_samples =
      stats.st_size / (sound->channels * (sound->bits_per_sample / 8));

  munmap(data, stats.st_size);
  close(fd);
}

void printAudioSpecs(const SoundRAW &s) {
  printf("Sample rate : %d\n", s.sample_rate);
  printf("Number of samples : %ld\n", s.num_samples);
  printf("Bits per sample : %d\n", s.bits_per_sample);
  printf("Channels : %d\n", s.channels);
  printf("Value limits : [ %ld, %ld ]\n", s.low_limit, s.high_limit);
}