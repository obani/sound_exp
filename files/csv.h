#pragma once

#include <initializer_list>

#include "../util/option.h"
#include "../util/string.h"
#include "../util/vector.h"

#include "../files/common.h"

class CSV {
  vector<vector<String>> comments;
  vector<vector<String>> file;
  bool loaded = false;

  void parse(const char *path) {
    String file_out;
    if (!readFileToString(path, file_out))
      return;

    vector<String> lines = file_out.split("\n");
    comments.push_back(vector<String>());

    int commentLine = 0;
    for (String &line : lines) {
      const char *s = line.str();
      if (hasPrefix(s, "//")) {
        comments[commentLine].push_back(s + 2); // Skip '//'
      } else if (hasPrefix(s, "#")) {
        comments[commentLine].push_back(s + 1); // Skip '#'
      } else if (!isEmpty(s)) {
        file.push_back(line.split(","));
        comments.push_back(vector<String>());
        ++commentLine;
      }
    }

    loaded = true;
  }

public:
  CSV() {}
  CSV(const char *path) { parse(path); }
  CSV(const CSV &csv)
      : comments(csv.comments), file(csv.file), loaded(csv.loaded) {}
  CSV(const CSV &&csv)
      : comments(csv.comments), file(csv.file), loaded(csv.loaded) {}
  CSV &operator=(const CSV &csv) {
    comments = csv.comments;
    file = csv.file;
    loaded = csv.loaded;
    return *this;
  }

  bool isLoaded() const { return loaded; }

  const vector<String> &getComments(size_t line) const {
    assert(line < lines());
    return comments[line];
  }

  Option<const char *> getString(size_t line, size_t cell) const {
    return line < lines() && cell < file[line].size()
               ? Option<const char *>(file[line][cell].str())
               : Option<const char *>();
  }

  Option<ssize_t> getInt(size_t line, size_t cell) const {
    return line < lines() && cell < file[line].size()
               ? Option<ssize_t>(file[line][cell].toInt())
               : Option<ssize_t>();
  }

  Option<double> getFloat(size_t line, size_t cell) const {
    return line < lines() && cell < file[line].size()
               ? Option<double>(file[line][cell].toFloat())
               : Option<double>();
  }

  size_t getCells(size_t line) const {
    return line < lines() ? file[line].size() : 0;
  }

  size_t lines() const { return file.size(); }
};

template <typename T>
concept CSVWriterArg =
    std::is_same_v<T, int> || std::is_same_v<T, const char *>;

class CSVWriter {
  String output;

  static void addSep(String &output) {
    if (!hasSuffix(output.str(), "\n"))
      output += ',';
  }

public:
  void store(const char *path, bool overwrite) {
    writeStringToFile(path, output, overwrite);
  }

  void comment(const char *s) {
    output += "//";
    output += s;
    output += "\n";
  }

  void add(const char *s) {
    addSep(output);
    output += s;
  }

  void add(int value) {
    addSep(output);

    if (value == 0) {
      output += "0";
      return;
    }

    constexpr size_t BUF_SIZE = 32;
    char BUFFER[BUF_SIZE] = {};

    if (value < 0) {
      value = -value;
      output += "-";
    }

    int i = BUF_SIZE - 1;
    while (value != 0 && i > 0) {
      --i;
      BUFFER[i] = '0' + value % 10;
      value /= 10;
    }

    output += BUFFER + i;
  }

  template <CSVWriterArg T, CSVWriterArg... Args> void add(T t, Args... args) {
    add(t);
    add(args...);
  }

  void next(int n = 1) {
    for (int i = 0; i < n; ++i)
      output += "\n";
  }

  bool empty() { return isEmpty(output.str()); }
  void clear() { output = ""; }
};