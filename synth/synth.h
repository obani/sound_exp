#pragma once

#include "../util/ifloat.h"
#include "../util/keyvector.h"

template <typename BANK, size_t POLYPHONY> struct Synth {
  float volume = 1.f;
  float wheel_pitch[2] = {0.5f, 1.f};
  std::array<BANK, POLYPHONY> synths;
  bool force_attack = false;

  Slice<JackPort> outputs;

  Synth(const Slice<JackPort> &o) : outputs(o) {
    static_assert(POLYPHONY != 0);
  }

  ssize_t play(const MidiKey &k, bool instantRelease) {
    if (k.id <= 0 || k.freq.raw() == 0)
      return -1;

    for (size_t i = 0; i < POLYPHONY; i++) {
      if (synths[i].pressed && synths[i].getId() == k.id) {
        if (instantRelease)
          synths[i].start(k);
        return i;
      }
    }

    for (size_t i = 0; i < POLYPHONY; i++) {
      if (synths[i].isFinished()) {
        synths[i].start(k);
        return i;
      }
    }

    size_t max_idx = 0;
    int max_synth = synths[0].getFrame();
    for (size_t i = 1; i < POLYPHONY; i++) {
      if (synths[i].getFrame() > max_synth) {
        max_idx = i;
        max_synth = synths[i].getFrame();
      }
    }

    synths[max_idx].start(k);
    return max_idx;
  }

  void step(jack_nframes_t nframes) {
    for (size_t i = 0; i < POLYPHONY; i++) {
      synths[i].pressed = force_attack;
      synths[i].step(outputs, nframes, 1.f);
    }
  }

  void step(jack_nframes_t nframes, const MidiInput &midi) {
    KeyVector pressed;

    for (uint8_t btn = midi.keyboard.iter(0); btn != 0;
         btn = midi.keyboard.iter(btn)) {
      ssize_t s = play(getMidiKey(btn, midi.keys[btn]),
                       midi.instantRelease.pressed(btn));
      if (s != -1)
        pressed.set(s);
    }

    float f = 1.f + midi.wheels[0] * wheel_pitch[midi.wheels[0] > 0.f];
    for (size_t i = 0; i < POLYPHONY; ++i) {
      synths[i].pressed = pressed.pressed(i);
      synths[i].step(outputs, nframes, f);
    }
  }
};