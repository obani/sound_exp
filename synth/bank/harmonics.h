#pragma once

#include "../../synth/synthbank.h"
#include "../../util/math.h"

template <size_t CHANNELS, size_t SIZE>
class Harmonics : public __SynthBank<CHANNELS, SIZE> {
private:
  using Bank = __SynthBank<CHANNELS, SIZE>;
  using Wave = PeriodicWave<CHANNELS>;

public:
  WaveType type = Wave_Sine;
  ADSR envelope;

  bool isFinished() override { return envelope.finished(); }

  int getFrame() override { return envelope.frame; }

  void custom_start() override { envelope.start(); }

private:
  template <WaveType w, bool PROCESS_ENVELOPE>
  static inline void
  _step(Slice<JackPort> &outputs, ADSR &env, std::array<Wave, SIZE> &waves,
        jack_nframes_t nframes, pfloat f, float synth_volume) {
    float env_signal, signal;
    for (jack_nframes_t i = 0; i < nframes; ++i) {
      if constexpr (PROCESS_ENVELOPE) {
        env_signal = env.step() * synth_volume;

        if (env.finished())
          break;
      } else {
        env_signal = synth_volume;
      }

      for (Wave &wave : waves) {
        signal = env_signal * wave.template step<w>(f);

        for (size_t chan = 0; chan < CHANNELS; ++chan)
          outputs[chan][i] += signal * wave.pan[chan];
      }
    }
  }

  template <WaveType w>
  static inline void
  _step_aux(Slice<JackPort> &outputs, ADSR &env, std::array<Wave, SIZE> &waves,
            jack_nframes_t nframes, pfloat f, float v, bool p) {
    if (env.checkHeld(p))
      _step<w, false>(outputs, env, waves, nframes, f, v * env.get());
    else
      _step<w, true>(outputs, env, waves, nframes, f, v);
  }

public:
  void step(Slice<JackPort> &outputs, jack_nframes_t nframes,
            float custom_freq) override {
    if (envelope.finished())
      return;

    pfloat f = custom_freq * Bank::freq;
    float v = Bank::volume;
    bool p = Bank::pressed;

    if (type == Wave_Sine)
      _step_aux<Wave_Sine>(outputs, envelope, Bank::waves, nframes, f, v, p);
    else if (type == Wave_Triangle)
      _step_aux<Wave_Triangle>(outputs, envelope, Bank::waves, nframes, f, v,
                               p);
    else if (type == Wave_Saw)
      _step_aux<Wave_Saw>(outputs, envelope, Bank::waves, nframes, f, v, p);
    else if (type == Wave_Square)
      _step_aux<Wave_Square>(outputs, envelope, Bank::waves, nframes, f, v, p);
    else
      _step_aux<Wave_Stair>(outputs, envelope, Bank::waves, nframes, f, v, p);
  }
};