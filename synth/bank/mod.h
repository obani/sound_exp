#pragma once

#include "../../synth/synthbank.h"
#include "../../util/math.h"

template <size_t CHANNELS, size_t SIZE>
struct Mod : __SynthBank<CHANNELS, SIZE> {
private:
  using Bank = __SynthBank<CHANNELS, SIZE>;
  using Wave = PeriodicWave<CHANNELS>;

public:
  ADSR envelope;
  std::array<WaveType, SIZE> types;

  bool isFinished() override { return envelope.finished(); }

  int getFrame() override { return envelope.frame; }

  void custom_start() override { envelope.start(); }

private:
  template <bool PROCESS_ENVELOPE>
  static inline void
  _step(Slice<JackPort> &outputs, ADSR &env, std::array<Wave, SIZE> &waves,
        const std::array<WaveType, SIZE> &types, jack_nframes_t nframes,
        pfloat f, float synth_volume) {
    float env_signal;
    for (jack_nframes_t i = 0; i < nframes; ++i) {
      if constexpr (PROCESS_ENVELOPE) {
        env_signal = env.step() * synth_volume;

        if (env.finished())
          break;
      } else {
        env_signal = synth_volume;
      }

      std::array<float, CHANNELS> signal = {1.f, 1.f};

      for (size_t s = 0; s < SIZE; ++s) {
        const WaveType type = types[s];
        float sig;

        if (type == Wave_Sine)
          sig = waves[s].template step<Wave_Sine>(f);
        else if (type == Wave_Triangle)
          sig = waves[s].template step<Wave_Triangle>(f);
        else if (type == Wave_Saw)
          sig = waves[s].template step<Wave_Saw>(f);
        else if (type == Wave_Square)
          sig = waves[s].template step<Wave_Square>(f);
        else
          sig = waves[s].template step<Wave_Stair>(f);

        for (size_t chan = 0; chan < CHANNELS; ++chan)
          signal[chan] *= sig * (waves[s].pan[chan] * 0.5f + 0.5f);
      }

      for (size_t chan = 0; chan < CHANNELS; ++chan)
        outputs[chan][i] += signal[chan] * env_signal;
    }
  }

public:
  void step(Slice<JackPort> &outputs, jack_nframes_t nframes,
            float freq_mod) override {
    if (envelope.finished())
      return;

    if (envelope.checkHeld(Bank::pressed))
      _step<false>(outputs, envelope, Bank::waves, types, nframes,
                   Bank::freq * freq_mod, Bank::volume * envelope.get());
    else
      _step<true>(outputs, envelope, Bank::waves, types, nframes,
                  Bank::freq * freq_mod, Bank::volume);
  }
};