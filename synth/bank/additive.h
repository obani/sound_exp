#pragma once

#include "../../synth/synthbank.h"

template <size_t CHANNELS, size_t SIZE>
struct Additive : __SynthBank<CHANNELS, SIZE> {
private:
  using Bank = __SynthBank<CHANNELS, SIZE>;
  using Wave = PeriodicWave<CHANNELS>;

public:
  struct Properties {
    ADSR env;
    WaveType type;
  };

  std::array<Properties, SIZE> prop;

  bool isFinished() override {
    for (Properties &p : prop)
      if (!p.env.finished())
        return false;
    return true;
  }

  int getFrame() override {
    int max_frame = prop[0].env.frame;
    for (size_t i = 1; i < SIZE; ++i)
      max_frame = Max(max_frame, prop[i].env.frame);

    return max_frame;
  }

  void custom_start() override {
    for (Properties &p : prop)
      p.env.start();
  }

private:
  template <WaveType w, bool PROCESS_ENVELOPE>
  static inline void _step(Slice<JackPort> &outputs, ADSR &env, Wave &wave,
                           jack_nframes_t nframes, pfloat synth_freq,
                           float synth_volume) {
    float signal;

    for (jack_nframes_t i = 0; i < nframes; ++i) {
      if constexpr (PROCESS_ENVELOPE) {
        signal = env.step() * synth_volume;

        if (env.finished())
          break;
      } else {
        signal = synth_volume;
      }

      signal *= wave.template step<w>(synth_freq);

      for (size_t chan = 0; chan < CHANNELS; ++chan)
        outputs[chan][i] += signal * wave.pan[chan];
    }
  }

  template <WaveType w>
  static inline void _step_aux(Slice<JackPort> &outputs, ADSR &env, Wave &wave,
                               jack_nframes_t nframes, pfloat f, float v,
                               bool p) {
    if (env.checkHeld(p))
      _step<w, false>(outputs, env, wave, nframes, f, v * env.get());
    else
      _step<w, true>(outputs, env, wave, nframes, f, v);
  }

public:
  void step(Slice<JackPort> &outputs, jack_nframes_t nframes,
            float freq_mod) override {
    pfloat freq = Bank::freq * freq_mod;

    for (size_t s = 0; s < SIZE; ++s) {
      ADSR &env = prop[s].env;
      Wave &wave = Bank::waves[s];
      WaveType type = prop[s].type;

      if (env.finished())
        continue;

      pfloat f = freq * wave.freq_factor;
      float v = Bank::volume * wave.volume;
      bool p = Bank::pressed;

      if (type == Wave_Sine)
        _step_aux<Wave_Sine>(outputs, env, wave, nframes, f, v, p);
      else if (type == Wave_Triangle)
        _step_aux<Wave_Triangle>(outputs, env, wave, nframes, f, v, p);
      else if (type == Wave_Saw)
        _step_aux<Wave_Saw>(outputs, env, wave, nframes, f, v, p);
      else if (type == Wave_Square)
        _step_aux<Wave_Square>(outputs, env, wave, nframes, f, v, p);
      else
        _step_aux<Wave_Stair>(outputs, env, wave, nframes, f, v, p);
    }
  }
};