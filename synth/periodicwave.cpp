#include "periodicwave.h"

#include <type_traits>

// This file is about the not interesting part that needs or is better split
// from the rest

WaveType operator+(WaveType x, int i) {
  using T = typename std::underlying_type<WaveType>::type;
  return WaveType(T(x) + i);
}

WaveType operator%(WaveType x, int i) {
  using T = typename std::underlying_type<WaveType>::type;
  return WaveType(T(x) % i);
}