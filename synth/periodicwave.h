#pragma once

#include "../audio/adsr.h"
#include "../audio/pan.h"

#include "../util/math.h"
#include "../util/pfloat.h"
#include "../util/random.h"
#include "../util/vector.h"

#include "osc.h"

enum WaveType : int {
  Wave_Sine = 0,
  Wave_Triangle,
  Wave_Saw,
  Wave_Square,
  Wave_Stair,

  Wave_Count
};

WaveType operator+(WaveType x, int i);
WaveType operator%(WaveType x, int i);

template <size_t CHANNELS> struct PeriodicWave {
  pfloat ramp;

public:
  float freq_factor = 1.f, volume = 0.f;
  Pan<CHANNELS> pan;

  template <WaveType w> float step(const pfloat &freq) {
    ramp += freq * freq_factor;

    return wave_func<w>(ramp) * volume;
  }

  template <WaveType w> static inline float wave_func(const pfloat &ramp) {
    if constexpr (w == Wave_Sine)
      return Osc::sine(ramp);
    else if constexpr (w == Wave_Triangle)
      return Osc::triangle(ramp);
    else if constexpr (w == Wave_Saw)
      return Osc::saw(ramp);
    else if constexpr (w == Wave_Stair)
      return Osc::stair(ramp);
    else if constexpr (w == Wave_Square)
      return Osc::square(ramp);
    else
      return 0.f;
  }

  void randPan() { pan = Pan<CHANNELS>(0.5f + randomized.Float11() * 0.5f); }

  void rand(float min_volume, float max_volume, float rand_freq) {
    freq_factor = randomized.Float(1.f - rand_freq, 1.f + rand_freq);
    volume = randomized.Float(min_volume, max_volume);
    randPan();
  }
};