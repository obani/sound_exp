#pragma once

#include "../util/pfloat.h"
#include "../util/random.h"

namespace Osc {

using OscillatorFunction = float (*)(pfloat);

constexpr float sine(pfloat ramp) { return Sine(ramp); }

constexpr float triangle(pfloat ramp) {
  return abs(ramp.f() - 0.5f) * 4.f - 1.f;
}

constexpr float saw(pfloat ramp) { return ramp.f() * 2.f - 1.f; }

constexpr float stair(pfloat ramp) {
  constexpr uint32_t STAIR_DIVIDER = pfloat::size >> 4;
  return 2.f * (ramp.raw() / STAIR_DIVIDER) - 1.f;
}

constexpr float square(pfloat ramp) {
  constexpr uint32_t SQUARE_SHIFTER = 32 - 1;
  return (ramp.raw() >> SQUARE_SHIFTER) - 0.5f;
}

constexpr float noise(pfloat ramp) { return randomized.Float11(); }

} // namespace Osc