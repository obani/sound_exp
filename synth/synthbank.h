#pragma once

#include <array>

#include "../midi/midi.h"
#include "periodicwave.h"

template <size_t CHANNELS, size_t SIZE> struct __SynthBank {
protected:
  uint16_t id = 0;
  using Wave = PeriodicWave<CHANNELS>;

public:
  std::array<Wave, SIZE> waves;
  pfloat freq;
  float volume = 1.f;
  bool pressed = false;

  void reset() {
    id = 0;
    volume = 1.f;
    pressed = false;
  }

  uint16_t getId() const { return id; }

  virtual bool isFinished() {
    printf("isFinished() not implemented\n");
    return true;
  }

  virtual int getFrame() {
    printf("getFrame() not implemented\n");
    return 0;
  }

  virtual void custom_start() { printf("custom_start not implemented\n"); }

  void start(const MidiKey &k) {
    custom_start();

    pressed = true;
    id = k.id;
    freq = k.freq;
    volume = k.velocity;
  }

  virtual void step(Slice<JackPort> &outputs, jack_nframes_t nframes,
                    float custom_freq) {
    printf("step( outputs, nframes, custom_volume, custom_freq ) not "
           "implemented\n");
  }
};