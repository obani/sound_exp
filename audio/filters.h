#pragma once

#include "../util/math.h"

template <int NUM> class LP {
  float state = 0.f;

public:
  LP<NUM> &operator=(const LP<NUM> &rhs) {
    state = rhs.state;
    return *this;
  }

  float getCoeff(float f) const { return 1.f - expf(-M_TAU * f); }

  float step(float sig, float e) {
    if constexpr (NUM == 1) {
      return state += (sig - state) * e;
    } else {
      for (int i = 0; i < NUM; ++i)
        state += (sig - state) * e;
      return state;
    }
  }
};

template <int NUM> class HP {
  LP<NUM> lp;

public:
  HP<NUM> &operator=(const HP<NUM> &rhs) {
    lp = rhs.lp;
    return *this;
  }

  float getCoeff(float f) const { return lp.getCoeff(f); }

  float step(float sig, float e) { return sig - lp.step(sig, e); }
};

float crossfade(float x, float y, float cf);