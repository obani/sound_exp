#include "filters.h"

float crossfade(float x, float y, float c) { return x + (y - x) * c; }