#pragma once

#include <jack/jack.h>

#include "../util/classes.h"
#include "../util/ifloat.h"

class ADSR : NotCopyable {
  enum {
    ADSR_Attack,
    ADSR_Decay,
    ADSR_Sustain,
    ADSR_Release,
    ADSR_Finished,
  };

  int attack = 0;
  int decay = 0;
  float sustain = 1.f;
  int release = 0;

  int state = ADSR_Finished;
  int end_frame = 0;
  IFloat phase;

private:
  void setState(int s, int time) {
    frame = 0;
    state = s;
    end_frame = time;
    phase.reset();
  }

  void setState(int s, int time, float goal) {
    frame = 0;
    state = s;
    end_frame = time;
    if (end_frame == 0)
      phase.jump(goal);
    else
      phase.set(goal, end_frame);
  }

  void setupNextState() {
    if (state == ADSR_Attack)
      setState(ADSR_Decay, decay, sustain);
    else if (state == ADSR_Decay)
      setState(ADSR_Sustain, 10000);
    else
      setState(ADSR_Finished, 10000);
  }

public:
  int frame = -1;

  ADSR() {}
  ADSR(float sr, float a, float d, float s, float r) { set(sr, a, d, s, r); }

  void set(float sr, float a, float d, float s, float r) {
    attack = a * sr;
    decay = d * sr;
    sustain = s;
    release = r * sr;
  }

  void start() { setState(ADSR_Attack, attack, 1.f); }

  bool checkHeld(bool held) {
    if (!held && state < ADSR_Release)
      setState(ADSR_Release, release, 0.f);

    return state == ADSR_Sustain;
  }

  float step() { // Shouldn't be called on sustain
    while (frame >= end_frame) {
      setupNextState();
      if (finished())
        return 0.f;
    }

    ++frame;
    return phase.next();
  }

  float get() const { return phase.get(); }
  bool finished() const { return state == ADSR_Finished; }
};