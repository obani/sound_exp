#pragma once

#include <assert.h>
#include <fcntl.h>
#include <stdint.h>
#include <stdio.h>
#include <unistd.h>

#include "jackclient.h"

#include "../util/math.h"

#define HEADER_TEXT 4

#define FORMAT_PCM 1
#define FORMAT_ALAW 6
#define FORMAT_MULAW 7

struct FMT {
  uint16_t format_type; // format type. 1-PCM, 3- IEEE float, 6 - 8bit A law, 7
                        // - 8bit mu law
  uint16_t channels;    // no.of channels
  uint32_t sample_rate; // sampling rate (blocks per second)
  uint32_t byterate;    // SampleRate * NumChannels * BitsPerSample/8
  uint16_t block_align; // NumChannels * BitsPerSample/8
  uint16_t bits_per_sample; // bits per sample, 8- 8bits, 16- 16 bits etc
};

enum AudioLimits {
  Low,
  High,
};

template <AudioLimits limit> static int64_t setLimits(uint16_t bps) {
  if constexpr (limit == AudioLimits::Low) {
    return bps == 8    ? -128
           : bps == 16 ? -32768
           : bps == 24 ? -8388608
           : bps == 32 ? -2147483648
                       : 0;
  } else {
    return bps == 8    ? 127
           : bps == 16 ? 32767
           : bps == 24 ? 8388607
           : bps == 32 ? 2147483647
                       : 0;
  }
}

struct SoundRAW {
  uint64_t num_samples;
  uint32_t sample_rate;
  uint16_t bits_per_sample;
  uint16_t channels;
  int64_t low_limit;
  int64_t high_limit;
  uint64_t offset;
  union {
    int8_t *i8;
    int16_t *i16;
    int32_t *i32;
  };

  SoundRAW()
      : num_samples{0}, sample_rate{0}, bits_per_sample{0}, channels{0},
        low_limit{0}, high_limit{0}, offset{0}, i8{nullptr} {}

  SoundRAW(uint32_t sr, uint16_t bps, uint16_t c)
      : num_samples{0}, sample_rate{sr}, bits_per_sample{bps}, channels{c},
        low_limit{setLimits<AudioLimits::Low>(bps)},
        high_limit{setLimits<AudioLimits::High>(bps)}, offset{0}, i8{nullptr} {}

  SoundRAW(uint64_t ns, uint32_t sr, uint16_t bps, uint16_t c)
      : num_samples{ns}, sample_rate{sr}, bits_per_sample{bps}, channels{c},
        low_limit{setLimits<AudioLimits::Low>(bps)},
        high_limit{setLimits<AudioLimits::High>(bps)}, offset{0} {
    assert(num_samples > 0);
    assert(sample_rate > 0);
    assert(bits_per_sample >= 8);
    assert(channels > 0);

    bps = bps == 24 ? 32 : bps;
    i8 = new int8_t[num_samples * channels * bps / 8];
  }

  SoundRAW(const SoundRAW &s)
      : num_samples{s.num_samples}, sample_rate{s.sample_rate},
        bits_per_sample{s.bits_per_sample}, channels{s.channels},
        low_limit{setLimits<AudioLimits::Low>(bits_per_sample)},
        high_limit{setLimits<AudioLimits::High>(bits_per_sample)},
        offset{s.offset}, i8{s.i8} {}

  SoundRAW &operator=(const SoundRAW &rhs) {
    num_samples = rhs.num_samples;
    sample_rate = rhs.sample_rate;
    bits_per_sample = rhs.bits_per_sample;
    channels = rhs.channels;
    low_limit = setLimits<AudioLimits::Low>(bits_per_sample);
    high_limit = setLimits<AudioLimits::High>(bits_per_sample);
    offset = rhs.offset;
    i8 = rhs.i8;
    return *this;
  }

  void free() {
    if (i8 == nullptr)
      return;

    if (bits_per_sample == 8)
      delete[] i8;
    else if (bits_per_sample == 16)
      delete[] i16;
    else
      delete[] i32;

    i8 = nullptr;
  }

  bool exists() const { return i8 != nullptr; }

  void setProperties(uint32_t sr, uint16_t bps, uint16_t c) {
    bps = bps == 24 ? 32 : bps;

    if (bps != bits_per_sample)
      num_samples = (num_samples * bits_per_sample) / bps;
    if (c != channels)
      num_samples = (num_samples * channels) / c;

    sample_rate = sr;
    bits_per_sample = bps;
    channels = c;
    low_limit = setLimits<AudioLimits::Low>(bps);
    high_limit = setLimits<AudioLimits::High>(bps);
  }

private:
  template <typename T> static void writeNum(int fd, T val) {
    write(fd, &val, sizeof(T));
  }

public:
  void save(const char *name) {
    int fd = creat(name, S_IWUSR | S_IRUSR);
    if (fd == -1) {
      printf("Error on creating file %s, maybe it already exists ?\n", name);
      return;
    }

    uint32_t data_size = num_samples * channels * bits_per_sample / 8;
    uint32_t total_size = data_size + sizeof(FMT) + 48;

    write(fd, "RIFF", HEADER_TEXT);
    writeNum(fd, uint32_t(total_size));
    write(fd, "WAVE", HEADER_TEXT);
    write(fd, "fmt ", HEADER_TEXT);
    writeNum(fd, uint32_t(16));
    writeNum(fd, uint16_t(FORMAT_PCM));
    writeNum(fd, uint16_t(channels));
    writeNum(fd, uint32_t(sample_rate));
    writeNum(fd, uint32_t(sample_rate * channels * bits_per_sample / 8));
    writeNum(fd, uint16_t(channels * bits_per_sample / 8));
    writeNum(fd, uint16_t(bits_per_sample));
    write(fd, "data", HEADER_TEXT);
    writeNum(fd, uint32_t(data_size));

    write(fd, i8, data_size);

    close(fd);
  }

private:
  template <typename T>
  static inline void writeToPort(JackPort &p, jack_nframes_t i,
                                 jack_nframes_t end_frame, float step,
                                 uint64_t channels, uint64_t offset, float fact,
                                 T *data) {
    for (; i != end_frame; ++i)
      p[i] += data[(uint64_t)(i * step) * channels + offset] * fact;
  }

public:
  // off is usually use to offset the sound and get the correct part of the
  // sound, and to get the correct channel of the sound
  jack_nframes_t toPort(JackPort &p, jack_nframes_t i, jack_nframes_t nframes,
                        float step, uint64_t chan, float volume) {
    assert(i < nframes);

    chan %= channels;

    const float fact = volume / high_limit;
    const uint64_t off = offset * channels + chan;

    jack_nframes_t size = nframes - i;
    const int64_t end = (offset + size * step);

    if (step < 0.f && end < 0) {
      size = Min(jack_nframes_t(offset / (-step)), size);
    } else if (step > 0.f && end > 0 && size_t(end) > num_samples) {
      size = Min(jack_nframes_t((size - offset) / step), size);
    }

    const jack_nframes_t end_frame = size + i;
    if (bits_per_sample == 8)
      writeToPort(p, i, end_frame, step, channels, off, fact, i8);
    else if (bits_per_sample == 16)
      writeToPort(p, i, end_frame, step, channels, off, fact, i16);
    else
      writeToPort(p, i, end_frame, step, channels, off, fact, i32);

    return size;
  }
};

struct Sound {
  uint64_t num_samples = 0;
  uint32_t sample_rate = 0;
  uint16_t channels = 1;
  uint64_t offset = 1;

  float **samples = nullptr;

  Sound() {}
  Sound(uint64_t ns, uint32_t sr, uint16_t c)
      : num_samples(ns), sample_rate(sr), channels(c) {
    assert(num_samples > 0);
    assert(sample_rate > 0);
    assert(channels > 0);

    samples = new float *[channels];
    for (uint16_t c = 0; c < channels; ++c) {
      samples[c] = new float[num_samples + 2];
      samples[c][0] = 0.f;
      samples[c][num_samples + 1] = 0.f;
    }
  }

  Sound(const Sound &s)
      : num_samples(s.num_samples), sample_rate(s.sample_rate),
        channels(s.channels), offset(s.offset) {
    samples = s.samples;
    for (uint16_t c = 0; c < channels; ++c)
      samples[c] = s.samples[c];
  }

  Sound &operator=(const Sound &rhs) {
    num_samples = rhs.num_samples;
    sample_rate = rhs.sample_rate;
    channels = rhs.channels;
    offset = rhs.offset;

    samples = rhs.samples;
    for (uint16_t c = 0; c < channels; ++c)
      samples[c] = rhs.samples[c];
    return *this;
  }

  void free() {
    if (samples == nullptr)
      return;

    for (uint16_t c = 0; c < channels; ++c)
      delete[] samples[c];
    delete[] samples;

    samples = nullptr;
  }

  bool exists() const { return samples != nullptr; }
  void setStart() { offset = 1; }
  void setEnd() { offset = num_samples + 1; }

  void setProperties(uint32_t sr, uint16_t c) {
    sample_rate = sr;
    channels = c;
  }

private:
  float t = 0.f;

public:
  // off is usually use to offset the sound and get the correct part of the
  // sound, and to get the correct channel of the sound
  jack_nframes_t toPort(JackPort &p, jack_nframes_t i, jack_nframes_t nframes,
                        float speed, uint16_t chan, float volume) {
    assert(i < nframes);

    chan %= channels;

    jack_nframes_t size = nframes - i;
    const int64_t end = (offset + size * speed);

    if (speed < 0.f && end < 0) {
      size = Min(jack_nframes_t(offset / (-speed)), size);
    } else if (speed > 0.f && end > 0 && size_t(end) > num_samples) {
      size = Min(jack_nframes_t((size - offset) / speed), size);
    }

    const jack_nframes_t end_frame = size + i;
    const float *array = samples[chan];

    for (; i != end_frame; ++i) {
      float x0 = array[offset - 1];
      float x1 = array[offset];
      float x2 = array[offset + 1];
      float x3 = array[offset + 2];

      float a0 = x3 - x2 - x0 + x1;
      float a1 = x0 - x1 - a0;
      float a2 = x2 - x0;
      float a3 = x1;

      p[i] += a0 * (t * t * t) + a1 * (t * t) + a2 * t + a3;

      t += speed;
      if (t >= 1.f || t <= 1.f) {
        float c = std::floor(t);
        t -= c;
        offset += c;
      }
    }

    return size;
  }

  jack_nframes_t toPorts(Slice<JackPort> &ports, jack_nframes_t i,
                         jack_nframes_t nframes, float speed, float volume) {
    jack_nframes_t size = 0;
    for (uint16_t c = 0; c < ports.size(); ++c)
      size = toPort(ports[c], i, nframes, speed, c, volume);

    return size;
  }
};