#include "jackclient.h"

#include <errno.h>

static void ExitCheck(bool b, const char *msg) {
  if (b) {
    fprintf(stderr, msg);
    exit(1);
  }
}

/*                          */
/*                          */
/*         JackPort         */
/*                          */
/*                          */

JackPort::JackPort()
    : type(JackPortType_Audio), buffer(NULL), buffer_frames(0), name(NULL),
      port(NULL) {}
JackPort::JackPort(jack_port_t *p)
    : buffer(NULL), buffer_frames(0), name(jack_port_name(p)), port(p) {
  const char *type_name = jack_port_type(port);
  flags = (JackPortFlags)jack_port_flags(port);
  type = JackPortType_None;

  if (strEqual(type_name, JACK_DEFAULT_AUDIO_TYPE))
    type = JackPortType_Audio;
  else if (strEqual(type_name, JACK_DEFAULT_MIDI_TYPE))
    type = JackPortType_Midi;
  else
    printf("Couldn't find a proper jack port type for port : %s\n", name);
}

JackPort::JackPort(const JackPort &cpy)
    : type(cpy.type), flags(cpy.flags), buffer(cpy.buffer),
      buffer_frames(cpy.buffer_frames), port(cpy.port) {}

JackPort &JackPort::operator=(const JackPort &rhs) {
  type = rhs.type;
  flags = rhs.flags;
  buffer = rhs.buffer;
  buffer_frames = rhs.buffer_frames;
  name = rhs.name;
  port = rhs.port;
  return *this;
}

void JackPort::update(jack_nframes_t nframes) {
  buffer = jack_port_get_buffer(port, nframes);
  buffer_frames = nframes;
}

float *JackPort::audio_data() { return buffer_audio; }
void *JackPort::midi_data() { return buffer; }
void JackPort::clear() {
  if (type == JackPortType_Midi)
    jack_midi_clear_buffer(buffer);
  else
    memset(buffer, 0, sizeof(float) * buffer_frames);
}

float &JackPort::operator[](size_t at) {
  assert(at < buffer_frames);
  return buffer_audio[at];
}

JackPortType JackPort::getType() { return type; }
JackPortFlags JackPort::getFlags() { return flags; }

void JackPort::writeMidi(jack_nframes_t offset, const jack_midi_data_t *data,
                         size_t data_size) {
  if (type != JackPortType_Midi && flags != JackPortIsOutput)
    return;

  jack_midi_event_write(buffer, offset, data, data_size);
}

/*                          */
/*                          */
/*        JackClient        */
/*                          */
/*                          */

void JackClient::shutdown(void *arg) {
  jack_client_t *client = (jack_client_t *)arg;
  printf("Closing client %s\n", jack_get_client_name(client));
  jack_client_close(client);
}

JackClient::JackClient(const char *client_name) {
  jack_status_t status;
  client = jack_client_open(client_name, JackServerName, &status, "default");

  if (status == JackFailure) {
    printf("jack server not running?\n");
    shutdown(client);
    return;
  }

  if (client == NULL) {
    printf("jack client is NULL\n");
    shutdown(client);
    return;
  }

  jack_on_shutdown(client, shutdown, client);

  printf("Created client %s\n", client_name);
}

JackClient::~JackClient() { shutdown(client); }

vector<JackPort> &JackClient::getCorrectBuffer(JackPortType type,
                                               JackPortFlags flags) {
  return type == JackPortType_Audio
             ? (flags & JackPortIsOutput) ? audioOutputs : audioInputs
         : (flags & JackPortIsOutput) ? midiOutputs
                                      : midiInputs;
}

int JackClient::registerPort(const char *port_name, JackPortType type,
                             JackPortFlags flags) {
  const char *type_name = type == JackPortType_Audio ? JACK_DEFAULT_AUDIO_TYPE
                                                     : JACK_DEFAULT_MIDI_TYPE;
  jack_port_t *port =
      jack_port_register(client, port_name, type_name, flags, 0);
  if (port == NULL) {
    printf("Couldn't create port %s with type %s\n", port_name, type_name);
    return -1;
  }

  vector<JackPort> &portsBuffer = getCorrectBuffer(type, flags);

  portsBuffer.push_back(port);
  return 0;
}

int JackClient::connect(const char *port, const char *to) {
  int e = jack_connect(client, port, to);
  if (e && e != EEXIST)
    printf("Couldn't connect port %s to port %s\n", port, to);
  return e;
}

/*void JackClient::connectPattern(JackPort &port, const char *pattern) {
  JackPortFlags flags = port.getFlags();
  const char **port_names = jack_get_ports(
      client, pattern,
      port.getType() == JackPortType_Audio ? JACK_DEFAULT_AUDIO_TYPE
                                           : JACK_DEFAULT_MIDI_TYPE,
      flags == JackPortIsOutput ? JackPortIsInput : JackPortIsOutput);

  if (port_names == NULL)
    return;

  if (flags == JackPortIsOutput)
    connect(port.name, port_names[0]);
  else
    connect(port_names[0], port.name);
}*/

jack_nframes_t JackClient::getSampleRate() const {
  return jack_get_sample_rate(client);
}
jack_nframes_t JackClient::getBufferSize() const {
  return jack_get_buffer_size(client);
}
int JackClient::setBufferSize(jack_nframes_t size) {
  return jack_set_buffer_size(client, size);
}

void JackClient::update(jack_nframes_t nframes, bool clear) {
  constexpr auto updatePorts = [](vector<JackPort> &ports, int nframes) {
    for (JackPort &p : ports)
      p.update(nframes);
  };

  constexpr auto updateAndClearPorts = [](vector<JackPort> &ports,
                                          int nframes) {
    for (JackPort &p : ports) {
      p.update(nframes);
      p.clear();
    }
  };

  if (clear) {
    updatePorts(audioInputs, nframes);
    updateAndClearPorts(audioOutputs, nframes);
    updatePorts(midiInputs, nframes);
    updateAndClearPorts(midiOutputs, nframes);
  } else {
    updatePorts(audioInputs, nframes);
    updatePorts(audioOutputs, nframes);
    updatePorts(midiInputs, nframes);
    updatePorts(midiOutputs, nframes);
  }
}

Slice<JackPort> JackClient::getJackPorts(JackPortType type,
                                         JackPortFlags flags) {
  vector<JackPort> &buffer = getCorrectBuffer(type, flags);

  return Slice<JackPort>(buffer.data(), 0, buffer.size());
}

Slice<JackPort> JackClient::initJackPorts(size_t num_ports, JackPortType type,
                                          JackPortFlags flags,
                                          const char *prefix,
                                          bool connect_default) {
  constexpr const char *unknown = "UNKNOWN";
  constexpr const char *audio = "Audio";
  constexpr const char *midi = "Midi";
  constexpr const char *input = "Input";
  constexpr const char *output = "Output";

  const char **port_names =
      type == JackPortType_Audio && (flags == JackPortIsOutput)
          ? jack_get_ports(client, NULL, JACK_DEFAULT_AUDIO_TYPE,
                           JackPortIsInput)
          : NULL;

  vector<JackPort> &buffer = getCorrectBuffer(type, flags);

  for (size_t i = 0; i < num_ports; ++i) {
    char str[20] = {};
    sprintf(str, "%s%s%ld",
            type == JackPortType_Audio  ? audio
            : type == JackPortType_Midi ? midi
                                        : unknown,
            flags == JackPortIsInput    ? input
            : flags == JackPortIsOutput ? output
                                        : unknown,
            i);

    String name(prefix);
    name += str;

    ExitCheck(registerPort(name.str(), type, flags), "Couldn't create port\n");

    if (port_names != NULL && connect_default)
      ExitCheck(connect(buffer.last().name, port_names[i]), "");
  }

  if (port_names != NULL)
    free(port_names);

  return Slice<JackPort>(buffer.data(), buffer.size() - num_ports,
                         buffer.size());
}

void JackClient::start(JackProcessCallback process_func) {
  ExitCheck(jack_set_process_callback(client, process_func, this) != 0,
            "Couldn't set process callback");
  ExitCheck(jack_activate(client) != 0, "Couldn't activate client");
}

String JackClient::getConfigFileName(const char *subfolder,
                                     const char *fileName) {
  const char *client_name = jack_get_client_name(client);
  String file_name(getConfigSubPath(subfolder));
  file_name += strlen(fileName) == 0 ? client_name : fileName;
  file_name += ".connections";
  return file_name;
}

void JackClient::readConfig(const char *subfolder, const char *fileName) {
  constexpr auto connectPort = [](JackClient &c, vector<JackPort> &ports,
                                  vector<String> &line) {
    ssize_t found = -1;
    for (size_t p = 0; p < ports.size(); p++) {
      if (strEqual(ports[p].name, line[0].str())) {
        found = p;
        break;
      }
    }

    if (found == -1)
      return false;

    for (size_t j = 1; j < line.size(); j++) {
      if (ports[found].getFlags() & JackPortIsOutput)
        c.connect(ports[found].name, line[j].str());
      else
        c.connect(line[j].str(), ports[found].name);
    }

    return true;
  };

  String in, file_name(getConfigFileName(subfolder, fileName));

  if (!readFileToString(file_name.str(), in)) {
    printf("Couldn't read config file %s\n", file_name.str());
    return;
  }

  vector<String> ports_lines(in.split(port_sep));

  printf("Reading connections from %s\n", file_name.str());

  for (size_t i = 0; i < ports_lines.size(); i++) {
    vector<String> line(ports_lines[i].split(conn_sep));

    if (!connectPort(*this, audioInputs, line))
      if (!connectPort(*this, audioOutputs, line))
        if (!connectPort(*this, midiInputs, line))
          if (!connectPort(*this, midiOutputs, line))
            printf("None found for '%s'\n", line[0].str());
  }
}

void JackClient::writeConfig() {
  String out, file_name(getConfigFileName());

  writePortConnections(audioInputs, out);
  writePortConnections(audioOutputs, out);
  writePortConnections(midiInputs, out);
  writePortConnections(midiOutputs, out);

  printf("Storing connections to %s\n", file_name.str());
  writeStringToFile(file_name.str(), out, true);
}

void sleepProcess(bool *s) {
  while (s == NULL || *s)
    sleep(1);
}