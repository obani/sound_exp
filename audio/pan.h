#pragma once

#include <array>

#include "../util/math.h"
#include <assert.h>
#include <jack/jack.h>

template <size_t CHANNELS> struct Pan {
private:
  std::array<float, CHANNELS> output;

public:
  float orientation; // 0 to 1

  Pan(float o) : orientation(o) { updateOutput(); }
  Pan() : Pan(0.5f) { updateOutput(); }
  Pan(const Pan &p) : Pan(p.orientation) { updateOutput(); }

  Pan &operator=(const Pan &rhs) {
    orientation = rhs.orientation;
    updateOutput();
    return *this;
  }

  float operator[](size_t at) const { return output[at]; }

  float getPan(uint16_t channel) {
    constexpr uint16_t LAST_CHANNEL = CHANNELS - 1;
    constexpr float N = LAST_CHANNEL;

    float out = N * (orientation - (channel / N));
    out = Max(0.f, channel == 0              ? 1.f - out
                   : channel == LAST_CHANNEL ? 1.f + out
                                             : 1.f - std::abs(out));

    return sqrtf(out);
  }

  void updateOutput() {
    if constexpr (CHANNELS < 2) {
      for (float &v : output)
        v = 0.f;
    } else {
      constexpr uint16_t NUM_CHANNELS = CHANNELS;
      for (uint16_t chan = 0; chan < NUM_CHANNELS; ++chan)
        output[chan] = getPan(chan);
    }
  }
};