#pragma once

#include <jack/jack.h>
#include <jack/midiport.h>
#include <stdio.h>
#include <stdlib.h>

#include "../files/common.h"

#include "../util/math.h"
#include "../util/slice.h"
#include "../util/string.h"
#include "../util/vector.h"

enum JackPortType {
  JackPortType_None,

  JackPortType_Audio,
  JackPortType_Midi,
};

class JackPort {
private:
  JackPortType type;
  JackPortFlags flags;

  union {
    void *buffer;
    float *buffer_audio;
  };

  jack_nframes_t buffer_frames;

public:
  const char *name;
  jack_port_t *port;

  JackPort();
  JackPort(jack_port_t *p);
  JackPort(const JackPort &cpy);
  JackPort &operator=(const JackPort &rhs);

  void update(jack_nframes_t nframes);

  float *audio_data();
  void *midi_data();
  void clear();

  float &operator[](size_t at);
  JackPortType getType();
  JackPortFlags getFlags();

  void writeMidi(jack_nframes_t offset, const jack_midi_data_t *data,
                 size_t data_size);
};

class JackClient {
  jack_client_t *client;

  vector<JackPort> audioInputs;
  vector<JackPort> audioOutputs;
  vector<JackPort> midiInputs;
  vector<JackPort> midiOutputs;

  static void shutdown(void *arg);

  vector<JackPort> &getCorrectBuffer(JackPortType type, JackPortFlags flags);

  static constexpr const char *conn_sep = "\t";
  static constexpr const char *port_sep = "\n";

public:
  JackClient(const char *client_name);
  virtual ~JackClient();

  int registerPort(const char *port_name, JackPortType type,
                   JackPortFlags flags);
  int connect(const char *port, const char *to);
  // void connectPattern(JackPort &port, const char *pattern);

  jack_nframes_t getSampleRate() const;
  jack_nframes_t getBufferSize() const;
  int setBufferSize(jack_nframes_t size);

  void update(jack_nframes_t nframes, bool clear);
  Slice<JackPort> getJackPorts(JackPortType type, JackPortFlags flags);
  Slice<JackPort> initJackPorts(size_t num_ports, JackPortType type,
                                JackPortFlags flags, const char *prefix = "",
                                bool connect_default = true);

  void start(JackProcessCallback process_func);

  String getConfigFileName(const char *subfolder = "",
                           const char *fileName = "");

  template <class VEC>
  static void writePortConnections(VEC &ports, String &out) {
    for (JackPort &p : ports) {
      const char **connections = jack_port_get_connections(p.port);
      if (connections == NULL)
        continue;

      out += p.name;
      out += conn_sep;

      for (size_t j = 0; connections[j] != NULL; j++) {
        out += connections[j];
        out += conn_sep;
      }

      out += port_sep;
    }
  }
  void readConfig(const char *subfolder = "", const char *fileName = "");
  void writeConfig();
};

void sleepProcess(bool *sleep);