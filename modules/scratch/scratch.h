#include "../debug.h"
#include "../module.h"

#include "../../input/linkedinput.h"

#include "../../files/audio.h"
#include "../../files/walker.h"

#include "../../util/math.h"
#include "../../util/vector.h"

namespace Modules {

struct Scratch : Module {
private:
  static constexpr float BASE_SPEED = 1.0f;
  static constexpr float INERTIA = 0.1f;
  static constexpr float MOUSE_SPEED = 0.002f;

  enum {
    Mode_Hold,
    Mode_Stop,

    Mode_Count
  };

  struct Track {
    Sound sound;
    float speed, volume;
    bool paused;
  };

  int mode = Mode_Hold;

  vector<Track> tracks;
  FileWalker walker;
  size_t file_selected = 0;
  bool file_selecting = false;
  String sample_path;

  float speed = BASE_SPEED;
  bool scratching = false;
  bool global_scratching = false;

  template <int ADD> void select() {
    if (tracks.size() != 0)
      file_selected = clamp(0UL, file_selected + ADD, tracks.size() - 1);
    debugPrintln("Selected : %ld", file_selected);
  }

  void switch_mode() {
    mode = (mode + 1) % Mode_Count;
    debugPrintln("Switched to scract mode %s",
                 mode == Mode_Hold ? "Hold" : "Stop");
  }

  void del_sound() {
    if (tracks.size() == 0)
      return;

    tracks[file_selected].sound.free();
    tracks.remove(file_selected);
    debugPrintln("Removed sound %ld", file_selected);

    if (file_selected == tracks.size() && tracks.size() != 0)
      file_selected--;
  }

  void add_sound() {
    file_selecting = true;
    sample_path = "";
    walker.getFilesList();
  }

  void apply_scratch(bool hold, float *speed, const MouseInput &m) {
    if (hold) {
      if (input[INPUT_MOUSE].changed)
        *speed += m.x * MOUSE_SPEED;
      else if (mode == Mode_Stop)
        *speed -= (*speed) * INERTIA;
    } else {
      *speed += (BASE_SPEED - (*speed)) * INERTIA;
    }
  }

public:
  static constexpr const char *name = "scratch";
  static constexpr ModuleBuffers properties = {.audioOutputs = 1};

  Scratch(JackClient &c, int id, MidiInput &midi, bool initBuffers)
      : Module(c, name, id, properties, initBuffers) {
    String p = getCurrentDirectory();
    walker.setPath(p.str());
    walker.suffix = ".wav";
  }

  void process(jack_nframes_t nframes, bool moduleFocus) override {
    if (file_selecting) {
      if (keyboardEvents.pressed(KEY_ESC)) {
        debugPrintln("Cancelled file selection");
        file_selecting = false;
      } else {
        walker.select(keyboardEvents.pressedClean(KEY_LEFT),
                      keyboardEvents.pressedClean(KEY_RIGHT),
                      keyboardEvents.pressedClean(KEY_UP),
                      keyboardEvents.pressedClean(KEY_DOWN),
                      keyboardEvents.pressedClean(KEY_ENTER), &sample_path);

        if (sample_path.size() != 0) {
          tracks.push_back(
              {openAudioFile(sample_path.str()), BASE_SPEED, 1.0f, true});
          file_selecting = false;
          if (file_selected != 0 && file_selected == tracks.size())
            file_selected--;
        }
      }
    }

    scratching =
        (mouseEvents.btn & MOUSE_LEFT) || keyboardEvents.pressed(KEY_SPACE);
    global_scratching = keyboardEvents.pressed(KEY_LEFTCTRL);

    if (keyboardEvents.pressedClean(KEY_UP))
      select<-1>();
    if (keyboardEvents.pressedClean(KEY_DOWN))
      select<+1>();

    if (keyboardEvents.pressedClean(KEY_TAB))
      switch_mode();
    if (keyboardEvents.pressedClean(KEY_DELETE))
      del_sound();
    if (keyboardEvents.pressedClean(KEY_INSERT))
      add_sound();

    if (tracks.size() == 0)
      return;

    // restart
    if (keyboardEvents.pressedClean(KEY_BACKSPACE))
      tracks[file_selected].sound.offset;
    // pause/unpause
    if (keyboardEvents.pressedClean(KEY_ENTER))
      tracks[file_selected].paused = !tracks[file_selected].paused;

    apply_scratch(global_scratching, &speed, mouseEvents);

    for (size_t i = 0; i < tracks.size(); i++) {
      if (tracks[i].paused)
        continue;

      apply_scratch(file_selected == i && scratching && !global_scratching,
                    &tracks[i].speed, mouseEvents);

      jack_nframes_t frames_written = 0;
      jack_nframes_t start_idx = 0;
      jack_nframes_t expected_frames = nframes;
      float s = speed * tracks[i].speed;

      while (frames_written != expected_frames) {
        expected_frames = nframes - frames_written;

        start_idx += frames_written;
        frames_written =
            tracks[i].sound.toPorts(audioOutputs, start_idx, nframes, s, 1.0f);

        if (frames_written != expected_frames) {
          if (s > 0.f)
            tracks[i].sound.setStart();
          else
            tracks[i].sound.setEnd();
        } else {
          tracks[i].sound.offset += frames_written * s;
        }
      }
    }
  }
};

} // namespace Modules