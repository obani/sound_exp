#include "../../midi/midi.h"

namespace Modules {

class MidiFeedback : Module {
  MidiInput &midi;

public:
  static constexpr const char *name = "midifb";
  static constexpr ModuleBuffers properties = {.midiOutput = true};

  MidiFeedback(JackClient &c, int id, MidiInput &m, bool initBuffers)
      : Module(c, name, id, properties, initBuffers), midi(m) {}

  void process(jack_nframes_t nframes, bool focused) override {
    static jack_midi_data_t signal[] = {MidiSignal_Pressed, 0, 0};
    jack_nframes_t i = 0;

    for (JackPort &out : midiOutputs) {
      signal[2] = 0;
      for (uint8_t btn = midi.instantRelease.iter(0); btn != 0;
           btn = midi.instantRelease.iter(btn)) {
        signal[1] = btn;
        out.writeMidi(++i, signal, 3);
      }

      signal[2] = 127;
      for (uint8_t btn = midi.keyboard.iter(0); btn != 0;
           btn = midi.keyboard.iter(btn)) {
        signal[1] = btn;
        out.writeMidi(++i, signal, 3);
      }
    }
  }
};

} // namespace Modules