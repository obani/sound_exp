#include "../debug.h"
#include "../module.h"

#include "../../util/math.h"
#include <fftw3.h>

namespace Modules {

struct TuneTest : Module {
private:
  static constexpr uint32_t MAX_SIZE = 512 << 5;
  static constexpr uint32_t L = MAX_SIZE >> 1;

  fftw_complex fftw_input[MAX_SIZE];
  fftw_complex fftw_output[MAX_SIZE];
  float freqs[MAX_SIZE];
  fftw_plan plan = fftw_plan_dft_1d(MAX_SIZE, fftw_input, fftw_output,
                                    FFTW_FORWARD, FFTW_ESTIMATE);

  size_t step = 0;

public:
  static constexpr const char *name = "tunetest";
  static constexpr ModuleBuffers properties = {.audioInputs = 1,
                                               .audioOutputs = 1};

  TuneTest(JackClient &c, int id, MidiInput &midi, bool initBuffers)
      : Module(c, name, id, properties, initBuffers) {}
  ~TuneTest() override { fftw_destroy_plan(plan); }

  void process(jack_nframes_t nframes, bool moduleFocus) override {
    float max = -10.0f;

    for (jack_nframes_t i = 0; i < nframes; step++, i++) {
      fftw_input[step][0] = audioInputs[0][i];
      fftw_input[step][1] = 0.0f;

      if (fftw_input[step][0] > max)
        max = fftw_input[step][0];
    }

    if (step >= MAX_SIZE) { // buffer filled
      step = 0;

      fftw_execute(plan);

      max = -10.0f;
      uint64_t max_idx = 0;
      for (uint64_t i = 0; i < L; i++) {
        freqs[i] = sqrt(fftw_output[i][0] * fftw_output[i][0] +
                        fftw_output[i][1] * fftw_output[i][1]);
        if (freqs[i] > max) {
          max = freqs[i];
          max_idx = i;
        }
      }

      if (max_idx != 0)
        debugPrintln("Max around %ld",
                     (max_idx * client.getSampleRate()) / MAX_SIZE);
    }
  }
};

} // namespace Modules