#include "../debug.h"
#include "../module.h"

namespace Modules {

struct MidiTest : Module {
  static constexpr size_t NUM_NUMBERS = 15;
  int num[NUM_NUMBERS] = {};
  size_t size = 0;

public:
  static constexpr const char *name = "miditest";
  static constexpr ModuleBuffers properties = {};

  MidiTest(JackClient &c, int id, MidiInput &midi, bool initBuffers)
      : Module(c, name, id, properties, initBuffers) {}

  void process(jack_nframes_t nframes, bool moduleFocus) override {
    jack_midi_event_t in_event;

    void *port_buf = midiInputs[0].midi_data();
    jack_nframes_t event_count = jack_midi_get_event_count(port_buf);

    for (jack_nframes_t i = 0; i < event_count; ++i) {
      jack_midi_event_get(&in_event, port_buf, i);

      for (size_t i = 0; i < in_event.size; ++i)
        num[i] = *(in_event.buffer + i);
      size = in_event.size;
    }

    size_t i = 0;
    for (; i < size; ++i)
      debugPrint("%3d ", num[i]);
    for (; i < NUM_NUMBERS; ++i)
      debugPrint("    ");
    debugPrintln();
  }
};

} // namespace Modules