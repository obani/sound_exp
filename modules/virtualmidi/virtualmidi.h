#include "../module.h"

#include "../../input/linkedinput.h"
#include "../../midi/midi.h"

#include <stdarg.h>

namespace Modules {

template <bool KEYBOARD, bool WHEEL> struct VirtualMidiKeyboard : Module {
private:
  static constexpr float wheel_inertia = 0.8f;
  static constexpr float wheel_factor = 0.01f;

  KeyVector midi_keyboard;
  int octave = 0;
  float wheel = 0.0f;
  float previous_wheel = 0.0f;

  LinkedValue<int, LinkedFuncs::add> octaveUp{KEY_PAGEUP, octave};
  LinkedValue<int, LinkedFuncs::sub> octaveDown{KEY_PAGEDOWN, octave};

public:
  static constexpr const char *name = KEYBOARD && WHEEL ? "vmkw"
                                      : WHEEL           ? "vmw"
                                      : KEYBOARD        ? "vmk"
                                                        : NULL;
  static constexpr ModuleBuffers properties = {.midiOutput = true};

  VirtualMidiKeyboard(JackClient &c, int id, MidiInput &midi, bool initBuffers)
      : Module(c, name, id, properties, initBuffers) {}

  void process(jack_nframes_t nframes, bool moduleFocus) override {
    if (!moduleFocus)
      return;

    static jack_midi_data_t midi_signal[] = {MidiSignal_SystemSignal, 0, 127};
    uint8_t num_events = 0;

    if constexpr (KEYBOARD) {
      octaveUp.checkPressedClean(keyboardEvents);
      octaveDown.checkPressedClean(keyboardEvents);

      for (uint8_t key = keyboardEvents.iter(0); key != 0;
           key = keyboardEvents.iter(key)) {
        uint8_t m = keyToMidi(key) + octave * 12;
        if (!m || midi_keyboard.pressed(m))
          continue;

        midi_signal[0] = MidiSignal_Pressed;
        midi_signal[1] = m;
        midi_signal[2] = 127;
        midiOutputs[0].writeMidi(num_events++, midi_signal, 3);

        midi_keyboard.set(m);
      }
    }

    if constexpr (WHEEL) {
      if (mouseEvents.btn & MOUSE_LEFT)
        wheel = clamp(-1.0f, wheel + mouseEvents.x * wheel_factor, 1.0f);
      else
        wheel *= wheel_inertia;

      if (wheel != previous_wheel && abs(wheel) > 0.0001f) {
        uint8_t midi_wheel = (0.5f + wheel * 0.5f) * 127.0f;
        midi_signal[0] = MidiSignal_Wheel;
        midi_signal[1] = 0;
        midi_signal[2] = midi_wheel;
        midiOutputs[0].writeMidi(num_events++, midi_signal, 3);
        previous_wheel = wheel;
      }
    }

    if constexpr (KEYBOARD) {
      for (uint8_t m = midi_keyboard.iter(0); m != 0;
           m = midi_keyboard.iter(m)) {
        uint8_t key = MidiToKey(m - octave * 12);
        if (!key || keyboardEvents.pressed(key))
          continue;

        midi_signal[0] = MidiSignal_Released;
        midi_signal[1] = m;
        midi_signal[2] = 127;
        midiOutputs[0].writeMidi(num_events++, midi_signal, 3);

        midi_keyboard.clean(m);
      }
    }
  }
};

} // namespace Modules