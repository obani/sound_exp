#include "../module.h"

namespace Modules {

template <size_t CHANNELS> class Input : Module {
public:
  static constexpr const char *name =
      CHANNELS == 1 ? "inputMono" : "inputStereo";
  static constexpr ModuleBuffers properties = {.audioInputs = CHANNELS,
                                               .audioOutputs = CHANNELS};

  Input(JackClient &c, int id, MidiInput &m, bool initBuffers)
      : Module(c, name, id, properties, initBuffers) {}

  void process(jack_nframes_t nframes, bool focused) override {
    for (size_t c = 0; c < CHANNELS; ++c)
      for (jack_nframes_t i = 0; i < nframes; ++i)
        audioOutputs[c][i] += audioInputs[c][i];
  }
};

} // namespace Modules