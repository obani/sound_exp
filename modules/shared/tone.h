#pragma once

#include "../../midi/midi.h"
#include "../debug.h"

#include <tuple>

namespace Modules {

using ToneContent = std::tuple<int, const Scale &>;

template <bool applyButton> class ToneController {
  int currentBasis = 0;
  const Scale *currentScale = &scaleFrom01(0.f);

public:
  MidiController basis, scale;
  MidiButton apply;

  ToneController(MidiInput &m) : basis(m), scale(m), apply(m) {}

  ToneContent update() {
    int b = A0 + int(basis.get() * 12.f) - 1;
    const Scale &s = scaleFrom01(scale.get());

    if constexpr (applyButton) {
      if (apply.get()) {
        currentBasis = b;
        currentScale = &s;
      }

      debugPrintln("%s %s (current: %s %s)", getKeyNameDisplay(b, false),
                   s.name, getKeyNameDisplay(currentBasis, false),
                   currentScale->name);
      return {currentBasis, *currentScale};
    } else {
      debugPrintln("%s %s", getKeyNameDisplay(b, false), s.name);
      return {b, s};
    }
  }
};

/*class ToneStorer {
  bool held = false;
  int currentBasis = 0;
  const Scale* currentScale = &scaleFrom01(0.f);

public:
  MidiButton btn;

  ToneStorer(MidiInput &m) : btn(m) {}

  std::tuple<int, const Scale &> update(jack_nframes_t nframes, int basis, const
Scale& scale) { if (btn.released() && !held) { if (currentBasis == 0) {
        currentBasis = basis;
        currentScale = &scale;
      }

      return {currentBasis, *currentScale};
    }

    if (held = btn.held(nframes)) {
      currentBasis = 0;
    }

    return {basis, scale};
  }
}*/

} // namespace Modules