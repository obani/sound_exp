#pragma once

#include <array>

#include "arpeggio/arpeggio.h"
#include "delay/delay1.h"
#include "dubatone/dubatone.h"
#include "filters/filters.h"
#include "input/input.h"
#include "loopstation/loopstation.h"
#include "midifb/midifb.h"
#include "miditest/miditest.h"
#include "sampler/sampler.h"
#include "scratch/scratch.h"
#include "shaper/shaper.h"
#include "synthz/synthz.h"
#include "tunetest/tunetest.h"
#include "tunor/tunor.h"
#include "virtualmidi/virtualmidi.h"

namespace Modules {

constexpr uint8_t HasKeyboard = 1 << 0;
constexpr uint8_t HasMouse = 1 << 1;
constexpr uint8_t HasJoystick = 1 << 2;

struct ModuleDescription {
  String name;
  Module::ModuleBuffers properties;
  Module *(*instantiate)(JackClient &, int, MidiInput &, bool);
  uint8_t input;
  String description;
};

constexpr size_t NUM_MODULES = 20;

template <typename MODULE>
static Module *instantiate(JackClient &c, int id, MidiInput &midi,
                           bool initBuffers) {
  return (Module *)new MODULE(c, id, midi, initBuffers);
}

static const std::array<ModuleDescription, NUM_MODULES> modulesDescription = {
    ModuleDescription{Arpeggio<false>::name, Arpeggio<false>::properties,
                      instantiate<Arpeggio<false>>, 0,
                      "Random arpeggiator (write on internal midi channel)"},
    ModuleDescription{Arpeggio<true>::name, Arpeggio<true>::properties,
                      instantiate<Arpeggio<true>>, 0,
                      "Random arpeggiator (write on output midi channel)"},
    ModuleDescription{Delay::name, Delay::properties, instantiate<Delay>, 0,
                      "Clipping Delay"},
    ModuleDescription{Dubatone::name, Dubatone::properties,
                      instantiate<Dubatone>, 0,
                      "Continuous oscillator controlled with midi controllers"},
    ModuleDescription{Filters::name, Filters::properties, instantiate<Filters>,
                      0, "Filters lol"},
    ModuleDescription{
        Input<1>::name, Input<1>::properties, instantiate<Input<1>>, 0,
        "Mono input (prefer using this if there's no input already)"},
    ModuleDescription{
        Input<2>::name, Input<2>::properties, instantiate<Input<2>>, 0,
        "Stereo input (prefer using this if there's no input already)"},
    ModuleDescription{LoopStation::name, LoopStation::properties,
                      instantiate<LoopStation>, 0,
                      "Loopstation controlled with midi controllers"},
    ModuleDescription{MidiFeedback::name, MidiFeedback::properties,
                      instantiate<MidiFeedback>, 0,
                      "Midi feedback for AKAI MIDIMIX"},
    ModuleDescription{MidiTest::name, MidiTest::properties,
                      instantiate<MidiTest>, 0,
                      "Test midi input and display it"},
    ModuleDescription{Sampler<1>::name, Sampler<1>::properties,
                      instantiate<Sampler<1>>, 0, "Mono audio sampler"},
    ModuleDescription{Sampler<2>::name, Sampler<2>::properties,
                      instantiate<Sampler<2>>, 0, "Stereo audio sampler"},
    ModuleDescription{Scratch::name, Scratch::properties, instantiate<Scratch>,
                      HasKeyboard | HasMouse, "Virtual turntable"},
    ModuleDescription{Shaper::name, Shaper::properties, instantiate<Shaper>, 0,
                      "LFO sound shaper"},
    ModuleDescription{Synthz::name, Synthz::properties, instantiate<Synthz>,
                      HasKeyboard, "Synths"},
    ModuleDescription{
        Tunor::name, Tunor::properties, instantiate<Tunor>, 0,
        "Generate a synth sound on the main frequency of the input"},
    ModuleDescription{TuneTest::name, TuneTest::properties,
                      instantiate<TuneTest>, 0,
                      "Find the main frequency of the current signal"},
    ModuleDescription{VirtualMidiKeyboard<true, false>::name,
                      VirtualMidiKeyboard<true, false>::properties,
                      instantiate<VirtualMidiKeyboard<true, false>>,
                      HasKeyboard,
                      "Virtual midi keyboard based on computer keyboard"},
    ModuleDescription{VirtualMidiKeyboard<false, true>::name,
                      VirtualMidiKeyboard<false, true>::properties,
                      instantiate<VirtualMidiKeyboard<false, true>>, HasMouse,
                      "Virtual midi pitch wheel based on mouse"},
    ModuleDescription{VirtualMidiKeyboard<true, true>::name,
                      VirtualMidiKeyboard<true, true>::properties,
                      instantiate<VirtualMidiKeyboard<true, true>>,
                      HasKeyboard | HasMouse,
                      "Virtual midi keyboard and pitch wheel based on computer "
                      "keyboard and mouse"}};

} // namespace Modules