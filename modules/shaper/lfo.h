#pragma once

#include "../../audio/filters.h"
#include "../../midi/midi.h"
#include "../../util/ifloat.h"

namespace Modules {

namespace ShaperModule {

using OSCFunctionf = float (*)(float);

template <OSCFunctionf f> struct OSC {
protected:
  float ramp = 0.f;

public:
  float step(float freq) {
    ramp += freq;
    ramp -= (int)ramp;
    return f(ramp);
  }
};

template <typename O>
concept isOsc = requires(O &o) { o.step(0.f); };

float sinef(float r) { return sinf(M_TAU * r); }
float sawf(float r) { return r * 2.f - 1.f; }
float revsawf(float r) { return 1.f - r * 2.f; }
float squaref(float r) { return r < 0.5f ? 1.f : -1.f; }

struct LFO {
  OSC<sinef> sine;
  OSC<sawf> saw;
  OSC<revsawf> revsaw;
  OSC<squaref> square;

  LP<1> lp;

  enum {
    Wave_Sine,
    Wave_Saw,
    Wave_RevSaw,
    Wave_Square,

    Wave_Count,
  };

  enum {
    LFO_Amp,
    LFO_LP,

    LFO_Count,
  };

  int wave_type = Wave_Sine;
  int lfo_type = LFO_Amp;

  template <int LFO_TYPE, isOsc OSC, hasNext F, typename C>
  static inline void loop(C &out, OSC &osc, F &freq, F &amp, LP<1> &lp,
                          jack_nframes_t i, jack_nframes_t end) {
    for (; i < end; ++i) {
      float signal = osc.step(freq.next());
      if constexpr (LFO_TYPE == LFO_Amp) {
        out[i] = crossfade(out[i], out[i] * signal, amp.next());
      } else if constexpr (LFO_TYPE == LFO_LP) {
        float a = amp.next();
        // 0.5f * (1.f - a) + (a * signal * 0.25f + 0.25f)
        // 0.5 - 0.5a + (a * signal * 0.25 + 0.25)
        // 0.75 - 0.5a + a * signal * 0.25
        out[i] = lp.step(out[i],
                         lp.getCoeff(0.75f - 0.5f * a + (a * signal * 0.25f)));
      }
    }
  }

  template <int LFO_TYPE, typename C>
  static inline void loopPrepare(C &buffer, int wave_type, jack_nframes_t itime,
                                 jack_nframes_t nframes, IFloat &freq_,
                                 IFloat &amplitude_, const NextFloat &freq_goal,
                                 const NextFloat &amp_goal, OSC<sinef> &sine,
                                 OSC<sawf> &saw, OSC<revsawf> &revsaw,
                                 OSC<squaref> &square, LP<1> &lp) {
    const jack_nframes_t start = std::min(itime, nframes);
    if (wave_type == Wave_Sine) {
      loop<LFO_TYPE>(buffer, sine, freq_, amplitude_, lp, 0, start);
      loop<LFO_TYPE>(buffer, sine, freq_goal, amp_goal, lp, start, nframes);
    } else if (wave_type == Wave_Saw) {
      loop<LFO_TYPE>(buffer, saw, freq_, amplitude_, lp, 0, start);
      loop<LFO_TYPE>(buffer, saw, freq_goal, amp_goal, lp, start, nframes);
    } else if (wave_type == Wave_RevSaw) {
      loop<LFO_TYPE>(buffer, revsaw, freq_, amplitude_, lp, 0, start);
      loop<LFO_TYPE>(buffer, revsaw, freq_goal, amp_goal, lp, start, nframes);
    } else if (wave_type == Wave_Square) {
      loop<LFO_TYPE>(buffer, square, freq_, amplitude_, lp, 0, start);
      loop<LFO_TYPE>(buffer, square, freq_goal, amp_goal, lp, start, nframes);
    }
  }

  static constexpr const char *lfoWaveNames[4] = {"Sin ", "Saw ", "RSaw",
                                                  "Squ "};
  static constexpr const char *lfoTypeNames[2] = {"Amp", "LP "};

  IFloat freq_, amplitude_;

public:
  MidiController freq, amplitude;
  MidiButton wave, type;

  LFO(MidiInput &midi) : freq(midi), amplitude(midi), wave(midi), type(midi) {}

  template <typename C>
  void step(C &buffer, jack_nframes_t nframes, jack_nframes_t itime) {
    constexpr float MIN_GAIN_VALUE = 0.0000001f;

    const float freq_value = freq.get();
    const float amp_value = amplitude.get();

    NextFloat freq_goal(std::pow(freq_value, 5) * 0.0625f);
    NextFloat amp_goal(amp_value * amp_value * amp_value);

    if (wave.getClean())
      wave_type = (wave_type + 1) % Wave_Count;
    if (type.getClean())
      lfo_type = (lfo_type + 1) % LFO_Count;

    freq_.set(freq_goal, itime);
    amplitude_.set(amp_goal, itime);

    debugPrintln("Freq: %.3f   Gain: %.3f   (LFO: %s %s)", freq_value,
                 amp_value, lfoWaveNames[wave_type], lfoTypeNames[lfo_type]);

    if (amp_value < MIN_GAIN_VALUE && amplitude_.get() < MIN_GAIN_VALUE)
      return;

    if (lfo_type == LFO_Amp) {
      loopPrepare<LFO_Amp>(buffer, wave_type, itime, nframes, freq_, amplitude_,
                           freq_goal, amp_goal, sine, saw, revsaw, square, lp);
    } else if (lfo_type == LFO_LP) {
      loopPrepare<LFO_LP>(buffer, wave_type, itime, nframes, freq_, amplitude_,
                          freq_goal, amp_goal, sine, saw, revsaw, square, lp);
    }
  }
};

} // namespace ShaperModule

} // namespace Modules
