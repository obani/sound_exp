#pragma once

#include "../debug.h"
#include "../module.h"

#include "../../files/csv.h"
#include "lfo.h"

namespace Modules {

struct Shaper : Module {
private:
  static constexpr size_t NUM_LFOS = 8;
  static constexpr size_t CHANNELS = 1;

  MidiController interpolation, amplitude;
  ShaperModule::LFO lfos[NUM_LFOS];

  IFloat amp_;

  template <typename LFO>
  static void SetCSVLinks(const CSV &csv, LFO &lfo, size_t l) {
    lfo.freq.setLink(csv.getInt(l, 0));
    lfo.amplitude.setLink(csv.getInt(l, 1));
    lfo.wave.setLink(csv.getInt(l, 2));
    lfo.type.setLink(csv.getInt(l, 3));
  }

  using BUFFER_TYPE = std::array<float, 4096>;

public:
  static constexpr const char *name = "shaper";
  static constexpr ModuleBuffers properties = {.audioOutputs = CHANNELS};

  Shaper(JackClient &c, int id, MidiInput &midi, bool initBuffers)
      : Module(c, name, id, properties, initBuffers), interpolation(midi),
        amplitude(midi), lfos{midi, midi, midi, midi, midi, midi, midi, midi} {
    const CSV csv(CSVName.str());

    if (csv.isLoaded()) {
      interpolation.setLink(csv.getInt(0, 0));
      amplitude.setLink(csv.getInt(0, 1));

      for (size_t l = 0; l < NUM_LFOS; ++l)
        SetCSVLinks(csv, lfos[l], l + 1);
    } else {
      writeStringToFile(CSVName.str(),
                        "// Dubatone config file\n"
                        "\n"
                        "// Interpolation, Amplitude\n 0, 0\n"
                        "\n"
                        "// Oscillators\n"
                        "\n"
                        "// Frequency, Amplitude, Wave, Type\n"
                        "\n"
                        "0,0,0,0\n"
                        "0,0,0,0\n"
                        "0,0,0,0\n"
                        "0,0,0,0\n"
                        "0,0,0,0\n"
                        "0,0,0,0\n"
                        "0,0,0,0\n"
                        "0,0,0,0\n",
                        true);
    }
  }

  template <hasNext F>
  static inline void loop(JackPort &p, BUFFER_TYPE &b, F &amp,
                          jack_nframes_t start, jack_nframes_t end) {
    for (jack_nframes_t i = start; i < end; ++i)
      p[i] = crossfade(p[i], p[i] * b[i], amp.next());
  }

  void process(jack_nframes_t nframes, bool moduleFocus) override {
    static BUFFER_TYPE buffer;

    const size_t itime = interpolation.get() * 200000.f;
    const float _amp_goal = amplitude.get();
    NextFloat amp_goal = _amp_goal * _amp_goal * _amp_goal;

    amp_.set(amp_goal, itime);

    for (jack_nframes_t i = 0; i < nframes; ++i) {
      buffer[i] = 1.f;
    }

    for (ShaperModule::LFO &lfo : lfos)
      lfo.step<BUFFER_TYPE>(buffer, nframes, itime);

    const jack_nframes_t start = std::min(itime, size_t(nframes));

    IFloat a = amp_;
    for (JackPort &port : audioOutputs) {
      amp_ = a;
      loop(port, buffer, amp_, 0, start);
      loop(port, buffer, amp_goal, start, nframes);
    }
  }
};

} // namespace Modules