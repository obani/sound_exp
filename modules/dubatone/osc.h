#pragma once

#include "../../audio/filters.h"
#include "../../util/random.h"

namespace Modules {

using OscillatorFunctionf = float (*)(float, float);

template <OscillatorFunctionf f> struct Oscillator {
protected:
  float ramp = 0.f;

public:
  float step(float freq, float morph) {
    ramp += freq;
    ramp -= (int)ramp;
    return f(ramp, morph);
  }
};

class NoiseOscillator {
  LP<1> lp;

public:
  float getCoeff(float f) const { return lp.getCoeff(f); }

  float step(float e, float morph) {
    return lp.step(randomized.Float01() < morph
                       ? 0.f
                       : randomized.Float11() * (4.f - e * 3.f),
                   e);
  }
};

} // namespace Modules