#pragma once

#include "../../audio/filters.h"
#include "../../util/ifloat.h"
#include "../../util/math.h"

#include "../../midi/midi.h"
#include "../debug.h"
#include "osc.h"

namespace Modules {

float sineWave(float ramp, float morph) { // morph -> [0..16]
  return sinf(M_TAU * ramp + Sine(pfloat(ramp * morph)));
}

float sineMorph(float morph) {
  constexpr float maxIndex = 16.f;
  return morph * morph * maxIndex;
}

float triangleWave(float ramp, float morph) { // morph -> [0..2]
  return ramp < 0.5f ? ramp * 2.f * morph + 0.5f - morph * 0.75f
                     : -ramp * 2.f * morph - 0.5f +
                           1.75f * morph; // Divide square's volume by 2
}

float triangleMorph(float morph) {
  return 2.f - 2.f * morph; // (1 - morph) * 2
}

float sawWave(float ramp, float morph) { // morph -> [0..1]
  const float div = 1.f / (1.f - morph);
  return ramp < morph ? ramp / morph : 2.f * (div - ramp * div) - 1.f;
}

float sawMorph(float morph) { return morph; }

float noiseMorph(float morph) { return morph * (2.f - morph); }

float sineLfoWave(float ramp, float morph) {
  return sineWave(ramp, morph) + 1.f;
}

float triangleLfoWave(float ramp, float morph) { // morph -> [0..2]
  return ramp < 0.5f ? 2.f * (ramp * morph + 1.f) - morph
                     : 2.f * (-ramp * morph + morph);
}

float sawLfoWave(float ramp, float morph) {
  const float div = 1.f / (1.f - morph);
  return 2.f * (ramp < morph ? ramp / morph : div - ramp * div);
}

enum {
  LFO_Sine,
  LFO_Triangle,
  LFO_Saw,

  LFO_Count,
};

using OscillatorMorph = float (*)(float);

template <typename O>
concept isOsc = requires(O &o) { o.step(0.f, 0.f); };

template <isOsc OSC, OscillatorMorph morphFunc, char ID> class ContinuousWave {
  OSC osc_;
  IFloat freq_, gain_, morph_, modAmp_, modFreq_, modMorph_;

  using SineLFO = Oscillator<sineLfoWave>;
  using TriangleLFO = Oscillator<triangleLfoWave>;
  using SawLFO = Oscillator<sawLfoWave>;

  SineLFO sineLFO;
  TriangleLFO triangleLFO;
  SawLFO sawLFO;

  template <isOsc LFO, hasNext F, bool addAssign, typename C>
  static inline void mainLoop(C &out, OSC &osc, LFO &lfo, F &freq, F &morph,
                              F &gain, F &mod_freq, F &mod_morph, F &mod,
                              bool isAmpMod, jack_nframes_t i,
                              jack_nframes_t end) {
    if (isAmpMod) {
      for (; i < end; ++i) {
        float signal = osc.step(freq.next(), morph.next()) * gain.next();
        signal = crossfade(signal,
                           signal * lfo.step(mod_freq.next(), mod_morph.next()),
                           mod.next());

        if constexpr (addAssign)
          out[i] += signal;
        else
          out[i] = signal;
      }
    } else {
      for (; i < end; ++i) {
        float signal =
            osc.step(freq.next() *
                         (1.f - mod.next() +
                          lfo.step(mod_freq.next(), mod_morph.next()) *
                              mod.next()), // mod is negative in this case,
                                           // so it's like 1 - LFO * amp
                     morph.next()) *
            gain.next();

        if constexpr (addAssign)
          out[i] += signal;
        else
          out[i] = signal;
      }
    }
  }

  int lfo = LFO_Sine;
  OscillatorMorph lfoMorphFunc = sineMorph;
  bool isAmpMod = true;

  float gain_stored, morph_stored, mod_amp_stored, freq_stored;

  static constexpr const char *lfoTypeNames[2] = {"Freq", "Amp "};
  static constexpr const char *lfoWaveNames[3] = {"Sin", "Tri", "Saw"};

  const char *lfoTypeName = lfoTypeNames[isAmpMod];
  const char *lfoWaveName = lfoWaveNames[lfo];

public:
  MidiController freq, gain, morph, mod_amp;

  MidiButton LFOWave, interpolationOff, LFOType, scaleOff;

  ContinuousWave(MidiInput &midi)
      : freq(midi), gain(midi), morph(midi), mod_amp(midi), LFOWave(midi),
        interpolationOff(midi), LFOType(midi), scaleOff(midi) {}

  template <bool addAssign, typename C>
  void step(C &buffer, jack_nframes_t nframes, jack_nframes_t itime,
            float mod_freq, float mod_morph, int basis, const Scale &scale) {
    constexpr float MIN_GAIN_VALUE = 0.0000001f;

    if (LFOWave.getClean()) {
      lfo = (lfo + 1) % LFO_Count;

      lfoMorphFunc = lfo == LFO_Sine       ? sineMorph
                     : lfo == LFO_Triangle ? triangleMorph
                                           : sawMorph;

      modMorph_.jump(lfoMorphFunc(mod_morph));

      lfoWaveName = lfoWaveNames[lfo];
    }

    const float gain_value = gain_stored;
    const float morph_value = morph_stored;
    const float mod_amp_value = mod_amp_stored;
    NextFloat freq_goal(
        (basis < A0 || scaleOff.get())
            ? midiFreqFrom01(freq_stored)
            : midiFreqFrom01FloorScale(freq_stored, basis, scale));

    if (LFOType.getClean()) {
      isAmpMod = !isAmpMod;
      modAmp_.jump(isAmpMod ? sqrtf(mod_amp_value)
                            : mod_amp_value * mod_amp_value);
      lfoTypeName = lfoTypeNames[isAmpMod];
    }

    debugPrintln(
        "%c: Freq: %.3f (%s)   Gain: %.3f   Morph: %3.0f%   Mod: %3.0f% "
        "(LFO: %s %s)",
        ID, freq_goal.next(),
        getKeyNameDisplay(midiNoteFrom01Scale(freq_stored, basis, scale), true),
        gain_value, morph_value * 100.f, mod_amp_value * 100.f, lfoTypeName,
        lfoWaveName);

    if (gain_value < MIN_GAIN_VALUE && gain_.get() < MIN_GAIN_VALUE) {
      if constexpr (!addAssign) {
        for (jack_nframes_t i = 0; i < nframes; ++i)
          buffer[i] = 0.f;
      }
      return;
    }

    if constexpr (std::is_same_v<OSC, NoiseOscillator>) {
      freq_goal = NextFloat(osc_.getCoeff(freq_goal.next()));
    }

    NextFloat gain_goal(gain_value * gain_value * gain_value * 0.25f);
    NextFloat morph_goal(morphFunc(morph_value));
    NextFloat mod_amp_goal(isAmpMod ? sqrtf(mod_amp_value)
                                    : mod_amp_value * mod_amp_value);
    NextFloat mod_morph_goal(lfoMorphFunc(mod_morph));
    NextFloat mod_freq_goal(mod_freq);

    jack_nframes_t endInterp = std::min(itime, nframes);

    if (itime == 0 || interpolationOff.get()) {
      endInterp = 0;

      freq_.jump(freq_goal);
      gain_.jump(gain_goal);
      morph_.jump(morph_goal);
      modAmp_.jump(mod_amp_goal);
      modFreq_.jump(mod_freq_goal);
      modMorph_.jump(mod_morph_goal);
    } else {
      freq_.set(freq_goal, itime);
      gain_.set(gain_goal, itime);
      morph_.set(morph_goal, itime);
      modAmp_.set(mod_amp_goal, itime);
      modFreq_.set(mod_freq_goal, itime);
      modMorph_.set(mod_morph_goal, itime);

      if (lfo == LFO_Sine)
        mainLoop<SineLFO, IFloat, addAssign>(buffer, osc_, sineLFO, freq_,
                                             morph_, gain_, modFreq_, modMorph_,
                                             modAmp_, isAmpMod, 0, endInterp);
      else if (lfo == LFO_Triangle)
        mainLoop<TriangleLFO, IFloat, addAssign>(
            buffer, osc_, triangleLFO, freq_, morph_, gain_, modFreq_,
            modMorph_, modAmp_, isAmpMod, 0, endInterp);
      else
        mainLoop<SawLFO, IFloat, addAssign>(buffer, osc_, sawLFO, freq_, morph_,
                                            gain_, modFreq_, modMorph_, modAmp_,
                                            isAmpMod, 0, endInterp);
    }

    if (lfo == LFO_Sine)
      mainLoop<SineLFO, const NextFloat, addAssign>(
          buffer, osc_, sineLFO, freq_goal, morph_goal, gain_goal,
          mod_freq_goal, mod_morph_goal, mod_amp_goal, isAmpMod, endInterp,
          nframes);
    else if (lfo == LFO_Triangle)
      mainLoop<TriangleLFO, const NextFloat, addAssign>(
          buffer, osc_, triangleLFO, freq_goal, morph_goal, gain_goal,
          mod_freq_goal, mod_morph_goal, mod_amp_goal, isAmpMod, endInterp,
          nframes);
    else
      mainLoop<SawLFO, const NextFloat, addAssign>(
          buffer, osc_, sawLFO, freq_goal, morph_goal, gain_goal, mod_freq_goal,
          mod_morph_goal, mod_amp_goal, isAmpMod, endInterp, nframes);
  }
};

} // namespace Modules