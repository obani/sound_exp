#include "../debug.h"
#include "../module.h"
#include "../shared/tone.h"

#include "../../files/csv.h"
#include "../../input/linkedinput.h"

#include "continuouswave.h"
#include "osc.h"

#include <cmath>

namespace Modules {

struct Dubatone : Module {
private:
  static constexpr size_t CHANNELS = 1;

  ToneController<true> tone;
  MidiController interpolation, mod_freq, mod_morph;

  template <typename OSC>
  static int SetCSVLinks(const CSV &csv, OSC &osc, int l) {
    ++l;
    osc.freq.setLink(csv.getInt(l, 0));
    osc.gain.setLink(csv.getInt(l, 1));
    osc.morph.setLink(csv.getInt(l, 2));
    osc.mod_amp.setLink(csv.getInt(l, 3));
    ++l;
    osc.LFOWave.setLink(csv.getInt(l, 0));
    osc.interpolationOff.setLink(csv.getInt(l, 1));
    osc.LFOType.setLink(csv.getInt(l, 2));
    osc.scaleOff.setLink(csv.getInt(l, 3));
    return l;
  }

public:
  static constexpr const char *name = "dubatone";
  static constexpr ModuleBuffers properties = {.audioOutputs = CHANNELS};

  ContinuousWave<Oscillator<sineWave>, sineMorph, 'S'> sineOsc;
  ContinuousWave<Oscillator<triangleWave>, triangleMorph, 'T'> triangleOsc;
  ContinuousWave<NoiseOscillator, noiseMorph, 'N'> noiseOsc;

  Dubatone(JackClient &c, int id, MidiInput &midi, bool initBuffers)
      : Module(c, name, id, properties, initBuffers), tone(midi),
        interpolation(midi), mod_freq(midi), mod_morph(midi), sineOsc(midi),
        triangleOsc(midi), noiseOsc(midi) {
    const CSV csv(CSVName.str());

    if (csv.isLoaded()) {
      interpolation.setLink(csv.getInt(0, 0));
      mod_freq.setLink(csv.getInt(0, 1));
      mod_morph.setLink(csv.getInt(0, 2));
      tone.basis.setLink(csv.getInt(0, 3));
      tone.scale.setLink(csv.getInt(0, 4));
      tone.apply.setLink(csv.getInt(0, 5));

      int l = 0;
      l = SetCSVLinks(csv, sineOsc, l);
      l = SetCSVLinks(csv, triangleOsc, l);
      l = SetCSVLinks(csv, noiseOsc, l);
    } else {
      writeStringToFile(CSVName.str(),
                        "// Dubatone config file\n"
                        "\n"
                        "// Interpolation, Modulation Frequency, Modulation "
                        "Morph, Basis, Scale, Apply\n"
                        "0,0,0,0,0,0\n"
                        "\n"
                        "// Oscillators\n"
                        "\n"
                        "// Frequency, Amplitude, Morph, Modulation Amplitude\n"
                        "// LFO wave, Interpolation Off, LFO type, Scale Off\n"
                        "\n"
                        "// Sine\n"
                        "0,0,0,0\n"
                        "0,0,0,0\n"
                        "// Triangle\n"
                        "0,0,0,0\n"
                        "0,0,0,0\n"
                        "// Noise\n"
                        "0,0,0,0\n"
                        "0,0,0,0\n",
                        true);
    }
  }

  void process(jack_nframes_t nframes, bool moduleFocus) override {
    static std::array<float, 4096> buffer;

    const float ivalue = interpolation.get();
    const int interp = 200000.f * (ivalue * ivalue * ivalue);
    const float mf = std::pow(mod_freq.get(), 4) * 0.0625f;
    const float mm = mod_morph.get();
    auto [b, s] = tone.update();

    debugPrintln("Interpolation: %6d  Mod(freq: %9.4f morph: %3.0f%)", interp,
                 mf * client.getSampleRate(), mm * 100.f);

    sineOsc.step<false>(buffer, nframes, interp, mf, mm, b, s);
    triangleOsc.step<true>(buffer, nframes, interp, mf, mm, b, s);
    noiseOsc.step<true>(buffer, nframes, interp, mf, mm, b, s);

    // Copy content to all other channels
    for (JackPort &port : audioOutputs)
      for (jack_nframes_t i = 0; i < nframes; ++i)
        port[i] += buffer[i];
  }
};

} // namespace Modules