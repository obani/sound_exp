#include "../module.h"

#include "../../audio/filters.h"

namespace Modules {

struct Filters : Module {
private:
  MidiController lp_control, hp_control;

  vector<LP<1>> lp;
  vector<HP<1>> hp;

public:
  static constexpr const char *name = "filters";
  static constexpr ModuleBuffers properties = {.audioOutputs = 1};

  Filters(JackClient &c, int id, MidiInput &midi, bool initBuffers)
      : Module(c, name, id, properties, initBuffers), lp_control(midi),
        hp_control(midi), lp(audioOutputs.size()), hp(audioOutputs.size()) {

    const CSV csv(CSVName.str());

    if (csv.isLoaded()) {
      lp_control.setLink(csv.getInt(0, 0));
      hp_control.setLink(csv.getInt(0, 1));
    } else {
      writeStringToFile(CSVName.str(),
                        "// Filters config file\n"
                        "\n"
                        "// LP, HP\n"
                        "0,0\n",
                        true);
    }
  }

  void process(jack_nframes_t nframes, bool moduleFocus) override {
    const float cLP = lp[0].getCoeff(midiFreqFrom01(lp_control.get()));
    const float cHP = hp[0].getCoeff(midiFreqFrom01(hp_control.get()));

    debugPrintln("LP: %.4f HP: %.4f", cLP, cHP);

    for (size_t c = 0; c < audioOutputs.size(); ++c)
      for (jack_nframes_t i = 0; i < nframes; ++i)
        audioOutputs[c][i] =
            lp[c].step(hp[c].step(audioOutputs[c][i], cHP), cLP);
  }
};

} // namespace Modules