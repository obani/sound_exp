#include <array>

#include "../module.h"

#include "tracks.h"

namespace Modules {

class LoopStation : Module {
  static constexpr size_t CHANNELS = 2;
  static constexpr bool INPUT_PLAYBACK = true;

  MidiButton play, stop, record, reset, prev, next;

  Tracks<CHANNELS> tracks;

  // LinkedValue<Tracks, clear> trackClear{KEY_DELETE, tracks};
  // LinkedValue<Tracks, track_reset> trackReset{KEY_END, tracks};
  // LinkedInput<const void> trackBack( KEY_DOWN, LinkedFuncs::null,
  // track_back ); static LinkedInput<const void> trackForw( KEY_UP,
  // LinkedFuncs::null, track_forw );

public:
  static constexpr const char *name = "loopstation";
  static constexpr ModuleBuffers properties = {.audioInputs = CHANNELS,
                                               .audioOutputs = CHANNELS};

  LoopStation(JackClient &c, int id, MidiInput &midi, bool initBuffers)
      : Module(c, name, id, properties, initBuffers), play(midi), stop(midi),
        record(midi), reset(midi), prev(midi), next(midi),
        tracks(getSampleRate(), audioInputs, audioOutputs, midi) {
    const CSV csv(CSVName.str());

    if (csv.isLoaded()) {
      play.setLink(csv.getInt(0, 0));
      stop.setLink(csv.getInt(0, 1));

      record.setLink(csv.getInt(1, 0));
      reset.setLink(csv.getInt(1, 1));

      prev.setLink(csv.getInt(2, 0));
      next.setLink(csv.getInt(2, 1));

      for (size_t i = 0, l = 3; i < Tracks<CHANNELS>::NUM_TRACKS; ++i, ++l)
        tracks.setLinks(i, csv.getInt(l, 0), csv.getInt(l, 1), csv.getInt(l, 2),
                        csv.getInt(l, 3));
    } else {
      writeStringToFile(CSVName.str(),
                        "// Loopstation config file\n"
                        "\n"
                        "// Play, Stop\n"
                        "// Record, Reset\n"
                        "// Previous, Next\n"
                        "\n"
                        "0,0\n"
                        "0,0\n"
                        "0,0\n"
                        "\n"
                        "// Track parameters\n"
                        "// Volume, Pan, Mute, Clear\n"
                        "\n"
                        "0,0,0,0\n"
                        "0,0,0,0\n"
                        "0,0,0,0\n"
                        "0,0,0,0\n"
                        "0,0,0,0\n"
                        "0,0,0,0\n"
                        "0,0,0,0\n"
                        "0,0,0,0\n"
                        "0,0,0,0\n",
                        true);
    }
  }

  void process(jack_nframes_t nframes, bool focused) override {
    if (play.getClean())
      tracks.start();
    if (stop.getClean())
      tracks.stop();
    if (record.getClean())
      tracks.record();
    if (reset.getClean())
      tracks.reset();
    if (prev.getClean())
      tracks.prev();
    if (next.getClean())
      tracks.next();

    //  trackBack.checkPressedClean( keyboardEvents );
    // trackForw.checkPressedClean( keyboardEvents );
    //  trackDecDiv.checkPressedClean( keyboardEvents );
    // trackIncDiv.checkPressedClean( keyboardEvents );

    if constexpr (INPUT_PLAYBACK) {
      for (size_t chan = 0; chan < audioOutputs.size(); ++chan)
        for (jack_nframes_t i = 0; i < nframes; ++i)
          audioOutputs[chan][i] += audioInputs[chan][i];
    }

    tracks.step(nframes);
  }
};

} // namespace Modules