#pragma once

#include <assert.h>
#include <jack/jack.h>
#include <stdlib.h>
#include <unistd.h>

#include <array>

#include "../../audio/adsr.h"
#include "../../files/csv.h"
#include "../../midi/midi.h"
#include "../../util/classes.h"
#include "../../util/math.h"

#include "../../audio/pan.h"

namespace Modules {

template <size_t CHANNELS> struct LoopTrack {
  static constexpr size_t TRACK_MAX_SIZE = 1323300;

private:
  MidiController volume_control, pan_control;
  MidiButton active_control, clear_control;

  bool initialized = false;
  bool clearing = false;
  bool activated = false;
  bool recording = false;

  size_t length = 0;
  size_t frame = 0;
  size_t clearing_frame = 0;

  using Track = std::array<float, TRACK_MAX_SIZE>;

  std::array<Track, CHANNELS> samples = {};
  ADSR envelope, record_envelope; // fade-in, fade-out
  Pan<CHANNELS> pan;

  void do_step(Slice<JackPort> &outputs, Slice<JackPort> &inputs, size_t from,
               size_t to) {
    assert(from <= to);

    if (from == to)
      return;

    const size_t nframes = to - from;
    const float volume = volume_control.get();
    const bool was_clearing = clearing;
    clearing = clearing && !(from <= clearing_frame && to > clearing_frame);

    envelope.checkHeld(activated);
    if ((!envelope.finished() || activated) && initialized &&
        length >= nframes) { // playback, clear if we need to
      size_t chan = 0;
      for (JackPort &out : outputs) {
        const size_t track_chan = (chan++) % CHANNELS;
        Track &arr = samples[track_chan];

        for (size_t i = 0; i < nframes; ++i) {
          size_t real_i = from + i;
          if (was_clearing && (clearing || real_i <= clearing_frame))
            arr[real_i] = 0.f;

          out[i] += arr[real_i] * pan[track_chan] * volume * envelope.step();
        }
      }
    } else if (was_clearing) // Clear even if we're not doing the playback
      for (Track &chan : samples)
        for (size_t i = from; i < to; i++)
          if (clearing || i <= clearing_frame)
            chan[i] = 0.f;

    record_envelope.checkHeld(recording);
    if (recording || !envelope.finished()) // record inputs
      for (size_t chan = 0; chan < CHANNELS; chan++)
        for (size_t i = 0; i < nframes; i++)
          samples[chan][from + i] += inputs[chan][i] * record_envelope.step();
  }

  void enable() {
    if (!activated)
      envelope.start();

    activated = true;
  }

public:
  LoopTrack(float sample_rate, MidiInput &m)
      : volume_control(m), pan_control(m), active_control(m), clear_control(m),
        envelope(sample_rate, 0.05f, 0.f, 1.f, 0.05f),
        record_envelope(sample_rate, 0.05f, 0.f, 1.f, 0.05f) {
    enable();
  }

  void setLinks(const Option<ssize_t> &v, const Option<ssize_t> &p,
                const Option<ssize_t> &a, const Option<ssize_t> &c) {
    if (v.exists)
      volume_control.setLink(v.val);
    if (p.exists)
      pan_control.setLink(p.val);
    if (a.exists)
      active_control.setLink(a.val);
    if (c.exists)
      clear_control.setLink(c.val);
  }

  bool isInitialized() const { return initialized; }
  bool isActive() const { return activated; }
  bool isRecording() const { return recording; }

  void disable() { activated = false; }

  void start() {
    enable();
    frame = 0;
  }

  size_t stopRecord() {
    if (!initialized && frame != 0) {
      initialized = true;

      if (length == 0) {
        constexpr size_t LEN_OFFSET = 32;
        length = frame + (LEN_OFFSET - (frame % LEN_OFFSET));
        clearing = false;

        return length;
      }

      size_t i = 1; // Adapt to previously set length;
      if (frame <= length) {
        while (frame <= (length / (i * 2)))
          i *= 2;
        length /= i;
      } else {
        while (frame > (length * i))
          i *= 2;
        length *= i;
      }

      if (clearing && clearing_frame > frame && frame < length)
        clearing_frame = length - 1;

      frame = frame % length;
      return 0;
    }

    recording = false;
    return 0;
  }

  void startRecord() {
    if (!recording)
      record_envelope.start();
    recording = true;
  }

  // divider should always be a power of 2, or there are more risks of
  // desyncrhonizing the loops. it doesn't matter for only one loop tho.
  void backwardBy(int divider) { // backward by length/divider (for example 1/4
                                 // of length), to stay in sync
    if (!initialized)
      return;

    size_t div = length / divider;
    frame = (div > frame) ? length - div + frame : frame - div;
  }

  void forwardBy(int divider) { // same with forward
    if (!initialized)
      return;

    frame = (frame + length / divider) % length;
  }

  void sync(size_t l) {
    if (!initialized)
      length = l;
  }

  void reset(bool reset_length) {
    if (length == 0)
      return;

    initialized = false;
    recording = false;
    clearing = true;

    if (reset_length) {
      length = 0;
      clearing_frame = TRACK_MAX_SIZE - 1;
    } else {
      clearing_frame = length - 1;
    }

    frame = 0;
  }

  /*int clear() {
    if (length == 0)
      return 0;

    clearing = true;
    clearing_frame = (frame == 0) ? (length - 1) : (frame - 1);
    return 1;
  }*/

  bool clear() { return clear_control.getClean(); }

  void step(Slice<JackPort> &outputs, Slice<JackPort> &inputs, size_t nframes) {
    if (!(initialized || recording))
      return;

    if (active_control.getClean()) { // mute/unmute
      if (isActive())
        disable();
      else
        enable();
    }

    const size_t from = frame;
    frame += nframes;

    pan.orientation = pan_control.get() - 0.5f;

    pan.updateOutput();

    if (!initialized || (frame < length)) {
      do_step(outputs, inputs, from, frame);
    } else if (frame >= length) {
      do_step(outputs, inputs, from, length);
      frame = frame % length;
      do_step(outputs, inputs, 0, frame);
    }
  }
};

} // namespace Modules