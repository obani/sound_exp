#pragma once

#include "../debug.h"
#include "looptrack.h"

namespace Modules {

template <size_t CHANNELS> struct Tracks {
  static constexpr size_t NUM_TRACKS = 9;
  using Track = LoopTrack<CHANNELS>;
  using Array = std::array<Track, NUM_TRACKS>;

private:
  Slice<JackPort> &inputs;
  Slice<JackPort> &outputs;

  size_t curr = 0;
  Array tracks;
  size_t tracksOn = 0;

  void select(size_t t) {
    Track &track = current();
    if (t != curr) {
      track.stopRecord();
      track.stopRecord(); // In case it was a first recording and we
                          // need to cancel double recording
    }

    curr = t;
  }

  static constexpr const char tracksOnStates[2] = {' ', '['};
  static constexpr const char selectedStates[2] = {' ', '>'};
  static constexpr const char *recordStates[2] = {"Not Recording",
                                                  "Recording    "};
  static constexpr const char *activeStates[2] = {"|>", "||"};

public:
  Tracks(float sr, Slice<JackPort> &i, Slice<JackPort> &o, MidiInput &midi)
      : inputs(i), outputs(o),
        tracks{Track(sr, midi), Track(sr, midi), Track(sr, midi),
               Track(sr, midi), Track(sr, midi), Track(sr, midi),
               Track(sr, midi), Track(sr, midi), Track(sr, midi)} {}

  Track &current() { return tracks[curr]; }
  const Track &current() const { return tracks[curr]; }
  size_t selected() const { return curr; }

  void setLinks(size_t i, const Option<ssize_t> &v, const Option<ssize_t> &p,
                const Option<ssize_t> &a, const Option<ssize_t> &c) {
    tracks[i].setLinks(v, p, a, c);
  }

  void prev() {
    if (curr != 0)
      select(curr - 1);
  }

  void next() {
    if (curr != (NUM_TRACKS - 1))
      select(curr + 1);
  }

  void trackBackwardBy(int p) { current().backwardBy(pow(2, p)); }
  void trackForwardBy(int p) { current().forwardBy(pow(2, p)); }

  void start() {
    for (Track &track : tracks)
      track.start();
  }

  void stop() {
    for (Track &track : tracks)
      track.disable();
  }

  void reset() {
    for (Track &track : tracks)
      track.reset(true);
  }

  void resetCurrent() {
    for (size_t i = 0; i < NUM_TRACKS; i++) {
      if (i != curr && tracks[i].isInitialized()) {
        current().reset(false);
        return; // Reset current
      }
    }

    reset(); // Reset everything is current is the last track otherwise
  }

  bool record() {
    Track &track = current();
    if (track.isRecording()) {
      size_t l = track.stopRecord();
      if (l != 0)
        for (Track &t : tracks)
          t.sync(l);
    } else {
      tracksOn |= 0b1 << curr;
      track.startRecord();
    }

    return track.isRecording();
  }

  void step(jack_nframes_t nframes) {
    for (size_t i = 0; i < NUM_TRACKS; ++i) {
      Track &track = tracks[i];
      const size_t mask = 0b1 << i;
      if (track.clear()) {
        tracksOn &= ~mask;
        track.reset(tracksOn == 0);
      }

      debugPrintln("%c%c %ld: %s", tracksOnStates[(tracksOn & mask) >> i],
                   selectedStates[i == curr], i + 1,
                   recordStates[track.isRecording()],
                   activeStates[track.isActive()]);
    }

    for (Track &track : tracks)
      track.step(outputs, inputs, nframes);
  }
};

} // namespace Modules