#include "../module.h"

#include "../../synth/bank/harmonics.h"
#include "../../synth/synth.h"

#include "../../util/math.h"
#include <fftw3.h>

namespace Modules {

struct Tunor : Module {
private:
  static constexpr size_t CHANNELS = 1;
  static constexpr int16_t MAX_SIZE = 512 << 4;
  static constexpr int16_t L = MAX_SIZE >> 1;

  static constexpr size_t POLYPHONY = 10;
  static constexpr size_t BANK_SIZE = 3;

  fftw_complex fftw_input[MAX_SIZE];
  fftw_complex fftw_output[MAX_SIZE];
  float freqs[MAX_SIZE];
  fftw_plan plan = fftw_plan_dft_1d(MAX_SIZE, fftw_input, fftw_output,
                                    FFTW_FORWARD, FFTW_ESTIMATE);

  size_t step = 0;

  using HBank = Harmonics<CHANNELS, BANK_SIZE>;
  Synth<HBank, POLYPHONY> synth;

public:
  static constexpr const char *name = "tunor";
  static constexpr ModuleBuffers properties = {.audioInputs = 1,
                                               .audioOutputs = CHANNELS};

  Tunor(JackClient &c, int id, MidiInput &midi, bool initBuffers)
      : Module(c, name, id, properties, initBuffers), synth(audioOutputs) {
    for (HBank &b : synth.synths) {
      HBank s;
      b.type = Wave_Sine;
      for (size_t i = 0; i < BANK_SIZE; ++i) {
        const float idx = i + 1;
        const float div = 1.f / idx;
        b.waves[i].rand(0.3f * div, 0.5f * div, 0.02f);
        b.waves[i].freq_factor *= idx;
      }
    }
  }

  ~Tunor() override { fftw_destroy_plan(plan); }

  void process(jack_nframes_t nframes, bool moduleFocus) override {
    float max = -10.0f;

    for (jack_nframes_t i = 0; i < nframes; step++, i++) {
      fftw_input[step][0] = audioInputs[0][i];
      fftw_input[step][1] = 0.0f;

      if (fftw_input[step][0] > max)
        max = fftw_input[step][0];
    }

    if (abs(max) < 0.05f)
      step = 0;

    if (step >= MAX_SIZE) { // buffer filled
      step = 0;

      fftw_execute(plan);

      max = -10.0f;
      int16_t max_idx = 0;
      for (int16_t i = 0; i < L; i++) {
        freqs[i] = sqrt(fftw_output[i][0] * fftw_output[i][0] +
                        fftw_output[i][1] * fftw_output[i][1]);
        if (freqs[i] > max) {
          max = freqs[i];
          max_idx = i;
        }
      }

      if (max_idx != 0)
        synth.play({max_idx, float(max_idx) / L, max / 16.f}, true);
    }

    synth.step(nframes);
  }
};

} // namespace Modules