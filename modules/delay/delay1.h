#include "../module.h"

#include "../../util/ifloat.h"

namespace Modules {

struct Delay : Module {
private:
  static constexpr size_t MAX_SAMPLES = 48000 * 5;
  static constexpr size_t ARRAY_SIZE = MAX_SAMPLES + 4096;
  static constexpr size_t MIN_DELAY = 128;
  static constexpr size_t IFLOAT_TIME = MIN_DELAY;

  MidiController dry, wet, delay, feedback;
  MidiButton deleteRest;

  IFloat dry_ifloat, wet_ifloat, fb_ifloat;

  size_t old_delay_samples = MIN_DELAY;
  size_t pos = 0;
  using Array = std::array<float, ARRAY_SIZE>;
  using SamplesArray = vector<Array>;
  SamplesArray samples;

  template <bool useIFloat>
  static void _wetLoop(JackPort &out, Array &samples, size_t i, size_t j,
                       size_t goal, IFloat &f) {
    for (; i < goal; ++i, ++j) {
      if constexpr (useIFloat)
        out[i] += samples[j] * f.next();
      else
        out[i] += samples[j] * f.get();
    }
  }

  static void writeWet(Slice<JackPort> &outs, SamplesArray &samples,
                       size_t fromO, size_t fromS, size_t nsamples, IFloat &f) {
    const size_t goal = fromO + nsamples;

    if (fromO >= IFLOAT_TIME) {
      for (size_t c = 0; c < outs.size(); ++c)
        _wetLoop<false>(outs[c], samples[c], fromO, fromS, goal, f);
    } else if (goal > IFLOAT_TIME) {
      IFloat ifloat;
      for (size_t c = 0; c < outs.size(); ++c) {
        ifloat = f;
        _wetLoop<true>(outs[c], samples[c], fromO, fromS, IFLOAT_TIME, ifloat);
        _wetLoop<false>(outs[c], samples[c], IFLOAT_TIME,
                        fromS + (IFLOAT_TIME - fromO), goal, ifloat);
      }
      f = ifloat;
    } else {
      IFloat ifloat;
      for (size_t c = 0; c < outs.size(); ++c) {
        ifloat = f;
        _wetLoop<true>(outs[c], samples[c], fromO, fromS, goal, ifloat);
      }
      f = ifloat;
    }
  }

  template <bool useIFloat, typename C>
  static void _delayLoop(Array &samples, C &in, size_t i, size_t j, size_t goal,
                         IFloat &fb, IFloat &t) {
    for (; i < goal; ++i, ++j) {
      if constexpr (useIFloat)
        samples[j] = (samples[j] + in[i]) * fb.next() * t.next();
      else
        samples[j] = (samples[j] + in[i]) * fb.get() * t.next();
    }
  }

  template <typename C>
  static void writeDelay(SamplesArray &samples, C &ins, size_t fromS,
                         size_t fromI, size_t nsamples, IFloat &feedback,
                         IFloat &trans) {
    const size_t goal = fromI + nsamples;

    IFloat t;
    if (fromI > IFLOAT_TIME) {
      for (size_t c = 0; c < ins.size(); ++c) {
        t = trans;
        _delayLoop<false>(samples[c], ins[c], fromI, fromS, goal, feedback, t);
      }
    } else if (goal > IFLOAT_TIME) {
      IFloat fb;
      for (size_t c = 0; c < ins.size(); ++c) {
        t = trans;
        fb = feedback;
        _delayLoop<true>(samples[c], ins[c], fromI, fromS, IFLOAT_TIME, fb, t);
        _delayLoop<false>(samples[c], ins[c], IFLOAT_TIME,
                          fromS + (IFLOAT_TIME - fromI), goal, fb, t);
      }
      feedback = fb;
    } else {
      IFloat fb;
      for (size_t c = 0; c < ins.size(); ++c) {
        t = trans;
        fb = feedback;
        _delayLoop<false>(samples[c], ins[c], fromI, fromS, goal, fb, t);
      }
      feedback = fb;
    }

    trans = t;
  }

  static void writeTrans(SamplesArray &samples, size_t writeStart,
                         size_t readStart, size_t nsamples, float fstart,
                         float fend) {
    IFloat ifloatNew(fstart);
    IFloat ifloatOld(fend);
    ifloatNew.set(fend, nsamples);
    ifloatOld.set(fstart, nsamples);

    const size_t writeEnd = writeStart + nsamples;
    for (size_t c = 0; c < samples.size(); ++c) {
      IFloat f1 = ifloatNew;
      IFloat f2 = ifloatOld;
      for (size_t w = writeStart, r = readStart; w < writeEnd; ++w, ++r) {
        samples[c][w] *= f2.next();
        samples[c][w] += samples[c][r] * f1.next();
      }
    }
  }

  vector<std::array<float, 4096>> tmp;

public:
  static constexpr const char *name = "delay1";
  static constexpr ModuleBuffers properties = {.audioOutputs = 1};

  Delay(JackClient &c, int id, MidiInput &midi, bool initBuffers)
      : Module(c, name, id, properties, initBuffers), dry(midi), wet(midi),
        delay(midi), feedback(midi), deleteRest(midi),
        samples(audioOutputs.size()), tmp(audioOutputs.size()) {
    const CSV csv(CSVName.str());

    for (std::array<float, 4096> &a : tmp)
      for (float &v : a)
        v = 0.f;

    if (csv.isLoaded()) {
      dry.setLink(csv.getInt(0, 0));
      wet.setLink(csv.getInt(0, 1));
      delay.setLink(csv.getInt(0, 2));
      feedback.setLink(csv.getInt(0, 3));
      deleteRest.setLink(csv.getInt(1, 0));
    } else {
      writeStringToFile(CSVName.str(),
                        "// Delay config file\n"
                        "\n"
                        "// Dry, Wet, Delay, Feedback\n"
                        "0,0,0,0\n"
                        "\n"
                        "// Delete leftovers\n"
                        "0\n",
                        true);
    }
  }

  void process(jack_nframes_t nframes, bool moduleFocus) override {
    constexpr size_t DELAY_FACTOR = MAX_SAMPLES - MIN_DELAY;

    const float dry_ratio = sqrtf(dry.get());
    const float wet_ratio = sqrtf(wet.get());
    const size_t delay_samples = MIN_DELAY + cube(delay.get()) * DELAY_FACTOR;
    const float fb_ratio = sqrtf(feedback.get());

    IFloat trans(1.f);

    if (old_delay_samples != delay_samples) {
      constexpr size_t TRANS_TIME = MIN_DELAY;
      if (old_delay_samples < delay_samples) {
        writeTrans(samples, old_delay_samples, 0, TRANS_TIME, 1.f, 0.f);
        writeTrans(samples, delay_samples - TRANS_TIME,
                   old_delay_samples - TRANS_TIME, TRANS_TIME, 0.f, 1.f);
      } else {
        writeTrans(samples, old_delay_samples - TRANS_TIME, 0, TRANS_TIME, 0.f,
                   1.f);
      }

      trans.jump(0.f);
      trans.set(1.f, nframes);
    }

    old_delay_samples = delay_samples;
    debugPrintln("Dry: %.4f Wet: %.4f Fb: %.4f Samples: %.4fs", dry_ratio,
                 wet_ratio, fb_ratio, float(delay_samples) / getSampleRate());

    if (deleteRest.getClean()) {
      for (size_t c = 0; c < audioOutputs.size(); ++c)
        for (size_t i = delay_samples; i < ARRAY_SIZE; ++i)
          samples[c][i] = 0.f;
    }

    dry_ifloat.set(dry_ratio, IFLOAT_TIME);
    wet_ifloat.set(wet_ratio, IFLOAT_TIME);
    fb_ifloat.set(fb_ratio, IFLOAT_TIME);

    // Dry
    IFloat a;
    for (size_t c = 0; c < audioOutputs.size(); ++c) {
      a = dry_ifloat;
      size_t i = 0;
      for (; i < IFLOAT_TIME; ++i) {
        tmp[c][i] = audioOutputs[c][i];
        audioOutputs[c][i] *= a.next();
      }

      for (; i < nframes; ++i) {
        tmp[c][i] = audioOutputs[c][i];
        audioOutputs[c][i] *= a.get();
      }
    }

    dry_ifloat = a;

    // Delay
    pos = pos % delay_samples;

    if (nframes > delay_samples) {
      size_t i = 0;
      while (nframes > 0) {
        // if pos is different from 0 at the start, we won't clip
        // if nframes is smaller than delay_samples, we won't overwrite
        size_t nframes2 = std::min(size_t(nframes), delay_samples - pos);
        writeWet(audioOutputs, samples, i, pos, nframes2, wet_ifloat);
        writeDelay(samples, tmp, pos, i, nframes2, fb_ifloat, trans);

        // this should always result to 0 or be offset on the last step (if
        // nframes isn't a multiplier of delay_samples)
        pos = (pos + nframes2) % delay_samples;
        nframes -= nframes2;
        i += nframes2;
      }

      writeWet(audioOutputs, samples, i, 0, nframes, wet_ifloat);
      writeDelay(samples, tmp, 0, i, nframes, fb_ifloat, trans);

      pos += nframes;
    } else if (delay_samples > (pos + nframes)) {
      writeWet(audioOutputs, samples, 0, pos, nframes, wet_ifloat);
      writeDelay(samples, tmp, pos, 0, nframes, fb_ifloat, trans);

      pos += nframes;
    } else {
      size_t nframes1 = delay_samples - pos;
      size_t nframes2 = nframes - nframes1;

      writeWet(audioOutputs, samples, 0, pos, nframes1, wet_ifloat);
      writeWet(audioOutputs, samples, nframes1, 0, nframes2, wet_ifloat);

      writeDelay(samples, tmp, pos, 0, nframes1, fb_ifloat, trans);
      writeDelay(samples, tmp, 0, nframes1, nframes2, fb_ifloat, trans);

      pos = nframes2;
    }
  }
};

} // namespace Modules