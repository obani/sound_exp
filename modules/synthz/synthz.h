#include "../debug.h"
#include "../module.h"

#include "../../input/linkedinput.h"
#include "../../synth/bank/additive.h"
#include "../../synth/bank/harmonics.h"
#include "../../synth/bank/mod.h"
#include "../../synth/synth.h"

#include "synth_states.h"

namespace Modules {

class Synthz : Module {
  static constexpr size_t CHANNELS = 2;
  static constexpr size_t POLYPHONY = 10;
  static constexpr size_t BANK_SIZE = 4;

  int synthType = Synth_Additive;

  MidiInput &midi;
  SynthStates<CHANNELS, POLYPHONY, BANK_SIZE> synth;

  MidiButton random, sin, tri, saw, squ;
  MidiControllerChanged attack, decay, sustain, release, volume, freq_range;

  LinkedValue<int, LinkedFuncs::add_mod<int, Synth_Count>> switchSynth{
      KEY_INSERT, synthType};

  static constexpr const char *synthNames[Synth_Count] = {
      "Additive ", "Harmonics", "Mod      "};

public:
  static constexpr const char *name = "synthz";
  static constexpr ModuleBuffers properties = {.audioOutputs = CHANNELS};

  Synthz(JackClient &c, int id, MidiInput &midi, bool initBuffers)
      : Module(c, name, id, properties, initBuffers), midi(midi),
        synth(audioOutputs), random(midi), sin(midi), tri(midi), saw(midi),
        squ(midi), attack(midi), decay(midi), sustain(midi), release(midi),
        volume(midi), freq_range(midi) {
    const CSV csv(CSVName.str());

    if (csv.isLoaded()) {
      random.setLink(csv.getInt(0, 0));
      sin.setLink(csv.getInt(0, 1));
      tri.setLink(csv.getInt(0, 2));
      saw.setLink(csv.getInt(0, 3));
      squ.setLink(csv.getInt(0, 4));

      volume.setLink(csv.getInt(1, 0));
      freq_range.setLink(csv.getInt(1, 1));

      attack.setLink(csv.getInt(2, 0));
      decay.setLink(csv.getInt(2, 1));
      sustain.setLink(csv.getInt(2, 2));
      release.setLink(csv.getInt(2, 3));
    } else {
      writeStringToFile(CSVName.str(),
                        "// Synthz config file\n"
                        "\n"
                        "// Buttons:\n"
                        "// Randomize, Sine, Triangle, Saw, Square\n"
                        "0,0,0,0,0\n"
                        "\n"
                        "// Sliders:\n"
                        "// Volume, Freq range\n"
                        "0,0\n"
                        "\n"
                        "// A, D, S, R\n"
                        "0,0,0,0\n",
                        true);
    }
  }

  void process(jack_nframes_t nframes, bool focused) override {
    if (focused)
      switchSynth.checkPressedClean(keyboardEvents);

    float sr = getSampleRate();
    if (random.getClean())
      synth.randomize(sr, attack.get(), decay.get(), sustain.get(),
                      release.get(), volume.get(), freq_range.get());

    if (sin.getClean())
      synth.switchWave(Wave_Sine);

    if (tri.getClean())
      synth.switchWave(Wave_Triangle);

    if (saw.getClean())
      synth.switchWave(Wave_Saw);

    if (squ.getClean())
      synth.switchWave(Wave_Square);

    if (attack.changed() || decay.changed() || sustain.changed() ||
        release.changed())
      synth.updateADSR(sr, attack.get(), decay.get(), sustain.get(),
                       release.get());

    if (volume.changed())
      synth.updateVolume(volume.get());

    if (freq_range.changed())
      synth.updateFreq(freq_range.get());

    debugPrintln(synthNames[synthType]);
    debugPrintln("%.4f %.4f %.4f %.4f %.4f %.4f", volume.get(),
                 freq_range.get(), attack.get(), decay.get(), sustain.get(),
                 release.get());
    synth.step(nframes, midi, synthType);
  }
};

} // namespace Modules