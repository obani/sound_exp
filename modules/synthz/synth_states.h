#pragma once

#include "../debug.h"

#include "../../midi/midi.h"
#include "../../synth/bank/additive.h"
#include "../../synth/bank/harmonics.h"
#include "../../synth/bank/mod.h"
#include "../../synth/synth.h"

namespace Modules {

enum { Synth_Additive, Synth_Harmonics, Synth_Mod, Synth_Count };

template <size_t CHANNELS, size_t POLYPHONY, size_t BANK_SIZE>
class SynthStates {
  struct ADSRState {
    float attack = 0.f, decay = 0.f, release = 0.f;

    void randomize() {
      attack = randomized.Float01();
      decay = randomized.Float01();
      release = randomized.Float01();
    }
  };

  struct WaveState {
    uint32_t wave;
    float volume, freq;

    void randomize() {
      wave = randomized.U32();
      volume = randomized.Float01();
      freq = randomized.Float11();
    }
  };

  struct DifferentADSR {
    std::array<ADSRState, BANK_SIZE> envelopes;
    std::array<WaveState, BANK_SIZE> waves;

    void randomize() {
      for (ADSRState &env : envelopes)
        env.randomize();
      for (WaveState &wave : waves)
        wave.randomize();
    }
  };

  struct SameADSR {
    ADSRState envelope;
    std::array<WaveState, BANK_SIZE> waves;

    void randomize() {
      envelope.randomize();
      for (WaveState &wave : waves)
        wave.randomize();
    }
  };

  using Wave = PeriodicWave<CHANNELS>;

  using ABank = Additive<CHANNELS, BANK_SIZE>;
  using HBank = Harmonics<CHANNELS, BANK_SIZE>;
  using MBank = Mod<CHANNELS, BANK_SIZE>;

  using AddSynth = Synth<ABank, POLYPHONY>;
  using HarSynth = Synth<HBank, POLYPHONY>;
  using ModSynth = Synth<MBank, POLYPHONY>;

  using AddProp = std::array<DifferentADSR, POLYPHONY>;
  using HarProp = std::array<SameADSR, POLYPHONY>;
  using ModProp = std::array<SameADSR, POLYPHONY>;

  AddProp additiveProperties;
  HarProp harmonicsProperties;
  ModProp modProperties;

  AddSynth additiveSynth;
  HarSynth harmonicsSynth;
  ModSynth modSynth;

  static constexpr float ATTACK_RANGE = 15.f;
  static constexpr float DECAY_RANGE = 15.f;
  static constexpr float RELEASE_RANGE = 30.f;
  static constexpr float FREQ_RANGE = 0.5f;

  static constexpr float MAX_VOLUME = 0.375f;

  uint32_t wavesEnabled = 0b1111;

  template <typename S, typename P>
  static void _updateFreq(S &s, P &prop, float range) {
    for (size_t p = 0; p < s.synths.size(); ++p)
      for (size_t i = 0; i < s.synths[p].waves.size(); ++i)
        s.synths[p].waves[i].freq_factor = 1.f + range * prop[p].waves[i].freq;
  }

  template <typename S> static void _randPan(S &s) {
    for (size_t p = 0; p < s.synths.size(); ++p)
      for (size_t i = 0; i < s.synths[p].waves.size(); ++i)
        s.synths[p].waves[i].randPan();
  }

  static constexpr size_t NUM_WAVES = 4;

  WaveType checkType(uint32_t type) {
    if (wavesEnabled == 0)
      return Wave_Count;

    uint32_t v = type % NUM_WAVES;
    while (!(wavesEnabled & (0b1 << v)))
      v = (v + 1) % NUM_WAVES;
    return WaveType(v);
  }

  static constexpr const char *WAVE_NAMES[5] = {
      "        ", "Sine    ", "Triangle", "Saw     ", "Square   "};

  const char *getWaveName(WaveType w) {
    if (wavesEnabled & (0b1 << w))
      return WAVE_NAMES[w + 1];
    return WAVE_NAMES[0];
  }

public:
  SynthStates(Slice<JackPort> &out)
      : additiveSynth(out), harmonicsSynth(out), modSynth(out) {
    randomize(0.f, 0.f, 0.f, 0.f, 0.f, 0.f, 0.f);
  }

  void switchWave(uint32_t num) {
    uint32_t flag = 0b1 << num;
    if (wavesEnabled & flag)
      wavesEnabled &= ~flag;
    else
      wavesEnabled |= flag;

    updateWave();
  }

  void randomize(float sr, float a, float d, float s, float r, float v,
                 float f) {
    for (DifferentADSR &d : additiveProperties)
      d.randomize();
    for (SameADSR &d : harmonicsProperties)
      d.randomize();
    for (SameADSR &d : modProperties)
      d.randomize();

    _randPan(additiveSynth);
    _randPan(harmonicsSynth);
    _randPan(modSynth);

    updateADSR(sr, a, d, s, r);
    updateVolume(v);
    updateFreq(f);
    updateWave();
  }

  void updateADSR(float sample_rate, float attack, float decay, float sustain,
                  float release) {
    constexpr auto setADSR = [](ADSR &a, ADSRState &s, float sr, float atk,
                                float dec, float sus, float rel) {
      a.set(sr, s.attack * atk, s.decay * dec, sus, s.release * rel);
    };

    const float a = square(attack) * ATTACK_RANGE;
    const float d = square(decay) * DECAY_RANGE;
    const float s = square(sustain);
    const float r = square(release) * RELEASE_RANGE;

    for (size_t p = 0; p < additiveSynth.synths.size(); ++p) {
      ABank &b = additiveSynth.synths[p];
      for (size_t i = 0; i < b.waves.size(); ++i)
        setADSR(b.prop[i].env, additiveProperties[p].envelopes[i], sample_rate,
                a, d, s, r);
    }

    for (size_t p = 0; p < harmonicsSynth.synths.size(); ++p)
      setADSR(harmonicsSynth.synths[p].envelope,
              harmonicsProperties[p].envelope, sample_rate, a, d, s, r);

    for (size_t p = 0; p < modSynth.synths.size(); ++p)
      setADSR(modSynth.synths[p].envelope, modProperties[p].envelope,
              sample_rate, a, d, s, r);
  }

  void updateVolume(float volume) {
    const float basis = sqrtf(volume) * MAX_VOLUME;

    for (size_t p = 0; p < additiveSynth.synths.size(); ++p) {
      ABank &b = additiveSynth.synths[p];
      for (size_t i = 0; i < b.waves.size(); ++i)
        b.waves[i].volume =
            basis - additiveProperties[p].waves[i].volume * basis;
    }

    constexpr float HFACTOR = 1.f / BANK_SIZE;
    float basis_har = HFACTOR * basis;
    for (size_t p = 0; p < harmonicsSynth.synths.size(); ++p) {
      HBank &b = harmonicsSynth.synths[p];
      for (size_t i = 0; i < b.waves.size(); ++i)
        b.waves[i].volume = (basis_har / (i * 0.5f + 1.f)) *
                            harmonicsProperties[p].waves[i].volume;
    }

    float basis_mod = 3.f * basis;
    for (size_t p = 0; p < modSynth.synths.size(); ++p) {
      MBank &b = modSynth.synths[p];
      size_t last = b.waves.size() - 1;
      for (size_t i = 0; i < last; ++i)
        b.waves[i].volume = 1.f;
      b.waves[last].volume =
          basis_mod - modProperties[p].waves[last].volume * basis_mod;
    }
  }

  void updateFreq(float freq_range) {
    const float r = square(square(freq_range)) * FREQ_RANGE;

    _updateFreq<AddSynth, AddProp>(additiveSynth, additiveProperties, r);

    for (size_t p = 0; p < harmonicsSynth.synths.size(); ++p) {
      HBank &b = harmonicsSynth.synths[p];
      for (size_t i = 0; i < b.waves.size(); ++i) {
        float idx = i + 1.f;
        b.waves[i].freq_factor =
            idx + idx * r * harmonicsProperties[p].waves[i].freq;
      }
    }

    _updateFreq<ModSynth, ModProp>(modSynth, modProperties, r);
  }

  void updateWave() {
    for (size_t p = 0; p < additiveSynth.synths.size(); ++p) {
      ABank &b = additiveSynth.synths[p];
      for (size_t i = 0; i < b.prop.size(); ++i)
        b.prop[i].type = checkType(additiveProperties[p].waves[i].wave);
    }

    for (size_t p = 0; p < harmonicsSynth.synths.size(); ++p)
      harmonicsSynth.synths[p].type =
          checkType(harmonicsProperties[p].waves[0].wave);

    for (size_t p = 0; p < modSynth.synths.size(); ++p) {
      MBank &b = modSynth.synths[p];
      for (size_t i = 0; i < b.types.size(); ++i)
        b.types[i] = checkType(modProperties[p].waves[i].wave);
    }
  }

  void step(jack_nframes_t nframes, MidiInput &midi, int synthType) {
    debugPrintln("%s %s %s %s", getWaveName(Wave_Sine),
                 getWaveName(Wave_Triangle), getWaveName(Wave_Saw),
                 getWaveName(Wave_Square));

    if (synthType == Synth_Additive)
      additiveSynth.step(nframes, midi);
    else if (synthType == Synth_Harmonics)
      harmonicsSynth.step(nframes, midi);
    else if (synthType == Synth_Mod)
      modSynth.step(nframes, midi);
  }
};

} // namespace Modules