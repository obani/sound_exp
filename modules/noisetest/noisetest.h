#include "../module.h"

#include "../dubatone/osc.h"

namespace Modules {

struct NoiseTest : Module {
private:
  static constexpr float MORPH = 0.999f;
  static constexpr float AMP = 0.0625f;

  NoiseOscillator noise;

public:
  static constexpr const char *name = "noisetest";
  static constexpr const ModuleBuffers properties = {.audioOutputs = 1};

  NoiseTest(JackClient &c, int id, MidiInput &midi, bool initBuffers)
      : Module(c, name, id, properties, initBuffers) {}

  void process(jack_nframes_t nframes, bool moduleFocus) override {
    for (jack_nframes_t i = 0; i < nframes; ++i)
      audioOutputs[0][i] = noise.step(0.5f, MORPH) * AMP;
  }
};

} // namespace Modules