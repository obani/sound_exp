#pragma once

#include "../audio/jackclient.h"
#include "../util/string.h"

namespace Modules {

struct Module {
private:
  using Ports = Slice<JackPort>;
  char IDholder[3] = {};

protected:
  JackClient &client;
  const int ID;
  const String internal_name;
  const String idString;
  const String internal_name_with_id;
  const String CSVName;

  Ports audioInputs;
  Ports audioOutputs;
  Ports midiInputs;
  Ports midiOutputs;

public:
  struct ModuleBuffers {
    size_t audioInputs = 0, audioOutputs = 0;
    bool midiOutput = false;
  };

  Module(JackClient &c, const char *name, int id, ModuleBuffers b,
         bool initBuffers)
      : client(c), ID(id), internal_name(name),
        idString([](int i, char str[3]) {
          snprintf(str, 3, "%d", i);
          return String(str);
        }(id, IDholder)),
        internal_name_with_id(internal_name + idString),
        CSVName([](const char *in, const String &id) {
          String name(getConfigSubPath(in));
          name += id;
          return name += ".csv";
        }(name, idString)) {
    midiInputs = client.getJackPorts(JackPortType_Midi, JackPortIsInput);

    if (initBuffers) {
      audioInputs =
          c.initJackPorts(b.audioInputs, JackPortType_Audio, JackPortIsInput,
                          internal_name_with_id.str());
      audioOutputs =
          c.initJackPorts(b.audioOutputs, JackPortType_Audio, JackPortIsOutput,
                          internal_name_with_id.str());
      if (b.midiOutput)
        midiOutputs = c.initJackPorts(1, JackPortType_Midi, JackPortIsOutput,
                                      internal_name_with_id.str());
    } else {
      audioInputs = client.getJackPorts(JackPortType_Audio, JackPortIsInput);
      audioOutputs = client.getJackPorts(JackPortType_Audio, JackPortIsOutput);
      midiOutputs = client.getJackPorts(JackPortType_Midi, JackPortIsOutput);
    }

    client.readConfig(internal_name.str(), idString.str());
  }

  virtual ~Module() {}

  virtual void process(jack_nframes_t nframes, bool moduleFocus) { return; }

  constexpr float getSampleRate() { return client.getSampleRate(); }

  void writeConfig() {
    String out, file_name(client.getConfigFileName(internal_name.str(),
                                                   idString.str()));

    JackClient::writePortConnections(audioInputs, out);
    JackClient::writePortConnections(audioOutputs, out);
    JackClient::writePortConnections(midiInputs, out);
    JackClient::writePortConnections(midiOutputs, out);

    printf("Storing connections to %s\n", file_name.str());
    writeStringToFile(file_name.str(), out, true);
  }
};

} // namespace Modules