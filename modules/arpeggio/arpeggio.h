#include "../debug.h"
#include "../module.h"

#include "../shared/tone.h"

#include "../../files/csv.h"

#include "../../midi/metronome.h"
#include "../../midi/midi.h"

#include "../../util/random.h"

namespace Modules {

template <bool OUT> class Arpeggio : Module {
  static constexpr int MAX_NOTES = 16;

  int numLastPressed = 0;
  std::array<int, MAX_NOTES> lastPressed;

  ToneController<true> tone;
  MidiController note, range, bpm, speed, min_notes, max_notes;
  Metronome tick;

  MidiInput &midi;

public:
  static constexpr const char *name = OUT ? "arpeggioOut" : "arpeggioIn";
  static constexpr ModuleBuffers properties = {.midiOutput = OUT};

  Arpeggio(JackClient &c, int id, MidiInput &m, bool initBuffers)
      : Module(c, name, id, properties, initBuffers), tone(m), note(m),
        range(m), bpm(m), speed(m), min_notes(m), max_notes(m), midi(m) {
    const CSV csv(CSVName.str());

    if (csv.isLoaded()) {
      note.setLink(csv.getInt(0, 0));
      range.setLink(csv.getInt(0, 1));

      tone.basis.setLink(csv.getInt(1, 0));
      tone.scale.setLink(csv.getInt(1, 1));
      tone.apply.setLink(csv.getInt(1, 2));

      bpm.setLink(csv.getInt(2, 0));
      speed.setLink(csv.getInt(2, 1));

      min_notes.setLink(csv.getInt(3, 0));
      max_notes.setLink(csv.getInt(3, 1));
    } else {
      writeStringToFile(CSVName.str(),
                        "// Arpeggio config file\n"
                        "\n"
                        "// Note, Range\n"
                        "0,0\n"
                        "\n"
                        "// Basis, Scale, Apply\n"
                        "0,0,0\n"
                        "\n"
                        "// BPM, Speed factor\n"
                        "0,0\n"
                        "\n"
                        "// Min Notes, Max Notes\n"
                        "0,0\n",
                        true);
    }
  }

  void process(jack_nframes_t nframes, bool focused) override {
    constexpr float NOTE_MIN = 15.f / 127.f;
    constexpr float NOTE_FACTOR = 1.f - NOTE_MIN;
    constexpr int POW_MIN = -4;
    constexpr float POW_RANGE = 7.f;

    const float BPM = std::floor((120.f + 120.f * bpm.get())) *
                      std::pow(2, POW_MIN + int(speed.get() * POW_RANGE));
    const int min_note = min_notes.get() * MAX_NOTES;
    const int max_note = max_notes.get() * MAX_NOTES;

    tick.length = BPMToSamples(client.getSampleRate(), BPM);
    auto [b, s] = tone.update();

    debugPrintln("BPM: %7.2f, min: %2d max: %2d", BPM, min_note, max_note);
    debugPrintln("note: %.3f range: %.3f", note.get(), range.get());

    if (tick.tick(nframes)) {
      const int num_notes = min_note == max_note ? min_note
                            : min_note < max_note
                                ? randomized.Int(min_note, max_note)
                                : randomized.Int(max_note, min_note);

      const float r = square(range.get()) * 0.5f;
      const float n = NOTE_MIN + note.get() * NOTE_FACTOR;

      if constexpr (OUT) {
        static jack_midi_data_t signal[] = {MidiSignal_Released, 0, 127};
        JackPort &out = midiOutputs[0];
        uint8_t &signal_type = signal[0];
        uint8_t &signal_id = signal[1];
        signal_type = MidiSignal_Released;

        for (int i = 0; i < numLastPressed; ++i) {
          signal_id = lastPressed[i];

          out.writeMidi(i, signal, 3);
        }

        signal_type = MidiSignal_Pressed;
        for (int i = 0; i < num_notes; ++i) {
          float idx = clamp(NOTE_MIN, n + randomized.Float11() * r, 1.f);
          int id =
              b < A0 ? midiNoteFrom01(idx) : midiNoteFrom01Scale(idx, b, s);
          lastPressed[i] = id;
          signal_id = id;

          out.writeMidi(numLastPressed + i, signal, 3);
        }
      } else {
        for (int i = 0; i < numLastPressed; ++i)
          midi.releaseKey(lastPressed[i]);

        for (int i = 0; i < num_notes; ++i) {
          float idx = clamp(NOTE_MIN, n + randomized.Float11() * r, 1.f);
          int id =
              b < A0 ? midiNoteFrom01(idx) : midiNoteFrom01Scale(idx, b, s);
          lastPressed[i] = id;
          midi.pressKey(id, 1.f);
        }
      }

      numLastPressed = num_notes;
    }
  }
};

} // namespace Modules