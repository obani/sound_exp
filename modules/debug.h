#pragma once

#include <stdio.h>

namespace Modules {

void debugMovePrint(int print_rate);
void debugMovePrintDown();
void debugClearPrint(int print_rate);
void debugPrint(const char *s, ...);
void debugPrintln(const char *s = "", ...);

} // namespace Modules