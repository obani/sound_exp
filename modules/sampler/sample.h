#pragma once

#include "../debug.h"

#include "../../audio/jackclient.h"
#include "../../util/ifloat.h"

#include <array>

namespace Modules {

template <size_t CHANNELS> class Sample {
  static constexpr size_t MAX_SAMPLES = 48000 * 20;

  class MKP {
    uint8_t key = 0;
    MidiInput &midi;

  public:
    MKP(MidiInput &m) : midi(m) {}

    void setLink(uint8_t v) { key = v; }

    bool get() { return midi.isPressed(key); }
  };

  std::array<std::array<float, MAX_SAMPLES>, CHANNELS> data;
  bool was_pressed = false;
  bool recorded = false;
  bool stopped = false;

  size_t pos = 0;
  size_t size = 0;

  template <typename C1, typename C2>
  static inline void dataAdd(C1 &to, C2 &from, size_t toStart, size_t fromStart,
                             size_t nframes) {
    size_t end = fromStart + nframes;
    for (size_t c = 0; c < CHANNELS; ++c)
      for (size_t i = fromStart, j = toStart; i < end; ++i, ++j)
        to[c][j] += from[c][i];
  }

  template <typename C1, typename C2>
  static inline void dataAdd(C1 &to, C2 &from, size_t toStart, size_t fromStart,
                             size_t nframes, float startF, float endF) {
    IFloat ifloat(startF);
    ifloat.set(endF, nframes);

    size_t end = fromStart + nframes;
    for (size_t c = 0; c < CHANNELS; ++c)
      for (size_t i = fromStart, j = toStart; i < end; ++i, ++j)
        to[c][j] += from[c][i] * ifloat.next();
  }

public:
  MKP link;

  Sample(MidiInput &midi) : link(midi) {}

  void process(Slice<JackPort> &in, Slice<JackPort> &out, size_t nframes) {
    debugPrint("%s ", recorded ? "V" : "X");

    constexpr size_t FADE = 32;

    bool pressed = link.get();
    if (pressed) {
      if (stopped)
        return;

      if (recorded) {
        size_t end = nframes;
        if ((pos + nframes) >= size) {
          stopped = true;
          end = pos + nframes - size;
        }

        dataAdd(out, data, 0, pos, end);
      } else {
        size_t start = 0;

        if (!was_pressed) {
          dataAdd(data, in, pos, 0, FADE, 0.f, 1.f);
          start += FADE;
        }

        dataAdd(data, in, pos + start, start, nframes - start);
      }

      pos += nframes;
    } else if (was_pressed) {
      if (recorded) {
        dataAdd(out, data, 0, pos, FADE, 1.f, 0.f);
      } else {
        dataAdd(data, in, pos, 0, FADE, 1.f, 0.f);
        size = pos + FADE;
        recorded = true;
      }

      stopped = false;
      pos = 0;
    }

    was_pressed = pressed;
  }
};

} // namespace Modules