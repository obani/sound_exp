#include "../module.h"

#include "../../files/common.h"

#include "sample.h"

namespace Modules {

template <size_t CHANNELS> struct Sampler : Module {
private:
  static constexpr size_t NUM_SAMPLES = 12;
  Sample<CHANNELS> samples[NUM_SAMPLES];

public:
  static constexpr const char *name =
      CHANNELS == 1 ? "monosampler" : "stereosampler";
  static constexpr ModuleBuffers properties = {.audioInputs = CHANNELS,
                                               .audioOutputs = CHANNELS};

  Sampler(JackClient &c, int id, MidiInput &midi, bool initBuffers)
      : Module(c, name, id, properties, initBuffers),
        samples{midi, midi, midi, midi, midi, midi,
                midi, midi, midi, midi, midi, midi} {
    const CSV csv(CSVName.str());

    if (csv.isLoaded()) {
      for (size_t i = 0; i < NUM_SAMPLES; ++i)
        samples[i].link.setLink(csv.getInt(0, i).val);
    } else {
      writeStringToFile(CSVName.str(),
                        "// Sampler config file\n"
                        "\n"
                        "0,0,0,0,0,0,0,0,0,0,0,0",
                        true);
    }
  }

  void process(jack_nframes_t nframes, bool moduleFocus) override {
    for (size_t i = 0; i < NUM_SAMPLES; ++i)
      samples[i].process(audioInputs, audioOutputs, nframes);

    debugPrintln();
  }
};

} // namespace Modules