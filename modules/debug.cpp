#include "debug.h"

#include "../util/math.h"
#include "../util/string.h"
#include <stdarg.h>

namespace Modules {

constexpr bool DEBUG = true;

static int num_lines = 0;
static int max_lines = 0;

static bool should_print = false;

bool checkPrintRate(int print_rate) {
  static int i = print_rate;

  ++i;
  should_print = i > print_rate;
  bool p = should_print && num_lines != 0;

  if (p) {
    i = 1;
    max_lines = Max(num_lines, max_lines);
  }
  return p;
}

void debugMovePrint(int print_rate) {
  if constexpr (DEBUG) {
    if (checkPrintRate(print_rate)) {
      printf(ANSI_MOVE_UP_FMT, num_lines);
      num_lines = 0;
    }
  }
}

void debugMovePrintDown() {
  if constexpr (DEBUG) {
    int i = max_lines - num_lines;
    for (; i; --i)
      printf("\n");
    max_lines = 0;
    num_lines = 0;
  }
}

void debugClearPrint(int print_rate) {
  if constexpr (DEBUG) {
    if (checkPrintRate(print_rate)) {
      for (; num_lines; --num_lines)
        printf(ANSI_MOVE_UP_DELETE);
    }
  }
}

void debugPrint(const char *s, ...) {
  if constexpr (DEBUG) {
    if (should_print) {
      va_list args;
      va_start(args, s);
      vprintf(s, args);
      va_end(args);
    }
  }
}

void debugPrintln(const char *s, ...) {
  if constexpr (DEBUG) {
    if (should_print) {
      va_list args;
      va_start(args, s);
      vprintf(s, args);
      va_end(args);
      printf("\n");
      ++num_lines;
    }
  }
}

} // namespace Modules