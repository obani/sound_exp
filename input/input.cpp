#include "input.h"
#include <termios.h>

InputData input[INPUT_TOTAL] = {
    {{}, "mouse", "", 3, NULL, false},
    {{}, "kbd", "", 24, NULL, false},
    {{}, "joystick", "", 8, NULL, false},
};

KeyboardEvents keyboardEvents;
MouseInput mouseEvents;
JoystickInput joystickEvents;

MouseInput getMouseInput() {
  return {
      input[INPUT_MOUSE].data.u[DATA_MOUSE_TYPE],
      input[INPUT_MOUSE].data.s[DATA_MOUSE_X],
      input[INPUT_MOUSE].data.s[DATA_MOUSE_Y],
  };
}

KeyboardInput getKeyboardInput() {
  return {
      (bool)input[INPUT_KEYBOARD].data.u[DATA_KEY_PRESSED],
      !input[INPUT_KEYBOARD].data.u[DATA_KEY_PRESSED],
      input[INPUT_KEYBOARD].data.u[DATA_KEY_INPUT],
  };
}

JoystickInput getJoystickInput() {
  if (input[INPUT_JOYSTICK].file.fd == 0) {
    return {};
  }

  return {(bool)(input[INPUT_JOYSTICK].data.u[DATA_JOY_ANALOG] & IS_ANALOG),
          (input[INPUT_JOYSTICK].data.u[DATA_JOY_ANALOG] & NOT_ANALOG) &&
              (input[INPUT_JOYSTICK].data.u[DATA_JOY_PRESSED]),
          input[INPUT_JOYSTICK].data.ss[DATA_JOY_ANALOG_SIGNAL],
          input[INPUT_JOYSTICK].data.u[DATA_JOY_BUTTON],
          input[INPUT_JOYSTICK].changed};
}

// update KeyboardEvents with a KeyboardInput event
// set/reset the 'event.btn' bit depending of it was pressed (set to 1) or
// released (reset to 0)
void KeyboardEvents::update(const KeyboardInput &event) {
  if (event.pressed) {
    set(event.btn);
  } else {
    clean(event.btn);
  }
}

static struct termios term_stored;
static struct termios term_new;
static int stdin_dup = -1;

static void disable_echo() { tcsetattr(stdin_dup, TCSANOW, &term_new); }

static void enable_echo() { tcsetattr(stdin_dup, TCSANOW, &term_stored); }

bool initInputData(
    const char *base_dir, InputData &input,
    const vector<const char *> &forbidden = vector<const char *>()) {
  if (stdin_dup == -1) {
    tcgetattr(STDIN_FILENO, &term_stored);
    memcpy(&term_new, &term_stored, sizeof(struct termios));

    stdin_dup = dup(STDIN_FILENO);
    term_new.c_lflag &= ~(ECHO | ICANON);
    term_stored.c_lflag |= ECHO | ICANON;
  }

  enable_echo();

  struct dirent *de;
  input.dir = base_dir;

  DIR *dr = opendir(base_dir);

  if (dr == NULL) {
    printf("Could not open '%s'\n directory", base_dir);
    disable_echo();
    return false;
  }

  if (input.dir.back() != '/')
    input.dir += "/";

  if (forbidden.size() != 0) {
    printf("Are forbidden for input '%s' the devices containing these keywords "
           ":\n",
           input.type);
    for (size_t i = 0; i < forbidden.size(); i++)
      printf("- '%s'\n", forbidden[i]);
  }

  vector<String> file_names;
  while ((de = readdir(dr)) != NULL) {
    if (strstr(de->d_name, input.type) != NULL) {
      bool can_append = true;
      for (size_t i = 0; i < forbidden.size(); i++) {
        if (strstr(de->d_name, forbidden[i]) != NULL) {
          can_append = false;
          break;
        }
      }

      if (can_append)
        file_names.push_back(de->d_name);
    }
  }

  if (file_names.size() == 0) {
    printf("Didn't find any input device for '%s'\n", input.type);
    disable_echo();
    return false;
  }

  if (file_names.size() == 1) {
    printf("Only input device found for '%s' was :\n", input.type);
    printf(" - '%s'\n", file_names[0].str());
    input.dir += file_names[0];
  } else {
    printf("These input devices were found for '%s' input type :\n",
           input.type);

    for (size_t i = 0; i < file_names.size(); i++)
      printf("%ld : %s\n", i, file_names[i].str());
    printf("Select (0 - %ld)\n", file_names.size() - 1);

    size_t idx = inputGetLong(0, file_names.size() - 1);

    printf("You selected '%s'\n", file_names[idx].str());
    input.dir += file_names[idx];
  }

  input.data.u = new uint8_t[input.data_size];
  input.file.fd = open(input.dir.str(), O_RDONLY | O_NONBLOCK);

  if (input.file.fd < 0) {
    printf("can't read '%s'\n", input.dir.str());
    printf("maybe try to run this as super user ?\n");

    exit(1);
  }
  input.file.events = POLLIN;

  disable_echo();
  return true;
}

bool readInputData(InputData &in) {
  if (in.file.fd == 0 || in.file.fd == input[INPUT_KEYBOARD].file.fd)
    return false;

  in.changed = false;
  int ret = poll(&in.file, 1, 0);

  if (ret > 0) {
    if (in.file.revents) {
      read(in.file.fd, in.data.u, in.data_size);
      in.changed = true;
      return true;
    } else {
      printf("error\n");
    }
  } else {
    return true;
  }

  return false;
}

bool readKeyboardInputData() {
  InputData &keyInput = input[INPUT_KEYBOARD];
  if (keyInput.file.fd == 0)
    return false;

  keyInput.changed = false;
  int ret;
  while ((ret = poll(&keyInput.file, 1, 0))) {
    if (ret < 0)
      return false;

    if (keyInput.file.revents) {
      read(keyInput.file.fd, keyInput.data.u, keyInput.data_size);
      switch (keyInput.data.u[DATA_KEY_TYPE]) {
      case EV_KEY:
        keyboardEvents.update(getKeyboardInput());
        break;
      case EV_SYN:
      case EV_MSC:
        break;
      default: {
#ifdef DEBUG_INPUT
        printf("Got unexpected key signal %d\n",
               keyInput.data.u[DATA_KEY_TYPE]);
        printAllData(keyInput);
#endif
      } break;
      }
      keyInput.changed = true;
    } else {
      printf("error\n");
    }
  }

  return true;
}

bool readMouseInputData() {
  bool b = readInputData(input[INPUT_MOUSE]);
  mouseEvents = getMouseInput();
  return b;
}

bool readJoystickInputData() {
  bool b = readInputData(input[INPUT_JOYSTICK]);
  joystickEvents = getJoystickInput();
  return b;
}

void printAllData(InputData &in) {
  for (size_t i = 0; i < in.data_size; i++) {
    printf("%02X ", in.data.u[i]);
  }
  printf("\n");
}

static void clean_stdin() {
  int stdin_copy = dup(STDIN_FILENO);
  /* remove garbage from stdin */
  tcdrain(stdin_copy);
  tcflush(stdin_copy, TCIFLUSH);
  close(stdin_copy);
}

void initKeyboardInput() {
  initInputData(LINUX_INPUT_PATH, input[INPUT_KEYBOARD]);
}

void initMouseInput() {
  vector<const char *> forbidden_inputs;
  forbidden_inputs.push_back("event");
  initInputData(LINUX_INPUT_PATH, input[INPUT_MOUSE], forbidden_inputs);
}

void initJoystickInput() {
  initInputData(LINUX_INPUT_PATH, input[INPUT_JOYSTICK]);
}

void shutdownInputs() {
  for (int i = 0; i < INPUT_TOTAL; i++) {
    close(input[i].file.fd);
  }
  clean_stdin();
  enable_echo();
  close(stdin_dup);
}

ssize_t inputGetLong(ssize_t min, ssize_t max) {
  constexpr uint8_t STR_SIZE = 12;
  char strl[STR_SIZE];

  char *endptr = NULL;
  ssize_t idx = 0;

  clean_stdin();
  enable_echo();
  while (endptr == NULL || endptr == strl || *endptr != '\n' || *strl == '\n' ||
         idx < min || idx > max) {
    fgets(strl, STR_SIZE, stdin);
    idx = strtol(strl, &endptr, 10);
  }
  disable_echo();

  return idx;
}

void inputGetString(char *str, size_t STR_SIZE) {
  clean_stdin();
  enable_echo();
  while (fgets(str, STR_SIZE, stdin) == NULL)
    ;
  size_t i;
  for (i = 0; str[i] != '\n'; i++)
    ;
  str[i] = '\0';

  disable_echo();
}

constexpr int BASE = 60;
constexpr int midi_to_key[256] = {
    0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
    0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
    0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
    0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
    0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
    0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
    KEY_Z, KEY_S, KEY_X, KEY_D, KEY_C, KEY_V, KEY_G, KEY_B, KEY_H, KEY_N,
    KEY_J, KEY_M, KEY_Q, KEY_2, KEY_W, KEY_3, KEY_E, KEY_R, KEY_5, KEY_T,
    KEY_6, KEY_Y, KEY_7, KEY_U, KEY_I, KEY_9, KEY_O, KEY_0, KEY_P,
};

constexpr int key_to_midi[256] = {
    0,         0, 0,
    BASE + 13, // KEY_2
    BASE + 15, // KEY_3
    0,
    BASE + 18, // KEY_5
    BASE + 20, // KEY_6
    BASE + 22, // KEY_7
    0,
    BASE + 25, // KEY_9
    BASE + 27, // KEY_0
    0,         0, 0, 0,
    BASE + 12, // KEY_Q
    BASE + 14, // KEY_W
    BASE + 16, // KEY_E
    BASE + 17, // KEY_R
    BASE + 19, // KEY_T
    BASE + 21, // KEY_Y
    BASE + 23, // KEY_U
    BASE + 24, // KEY_I
    BASE + 26, // KEY_O
    BASE + 28, // KEYP
    0,         0, 0, 0, 0,
    BASE + 1, // KEY_S
    BASE + 3, // KEY_D
    0,
    BASE + 6,  // KEY_G
    BASE + 8,  // KEY_H
    BASE + 10, // KEY_J
    0,         0, 0, 0, 0, 0, 0,
    BASE + 0,  // KEY_Z
    BASE + 2,  // KEY_X
    BASE + 4,  // KEY_C
    BASE + 5,  // KEY_V
    BASE + 7,  // KEY_B
    BASE + 9,  // KEY_N
    BASE + 11, // KEY_M
};

constexpr const char *key_names[] = {
    "",
    "ESC",
    "1",
    "2",
    "3",
    "4",
    "5",
    "6",
    "7",
    "8",
    "9",
    "0",
    "MINUS",
    "EQUAL",
    "BACKSPACE",
    "TAB",
    "Q",
    "W",
    "E",
    "R",
    "T",
    "Y",
    "U",
    "I",
    "O",
    "P",
    "LEFTBRACE",
    "RIGHTBRACE",
    "ENTER",
    "LEFTCTRL",
    "A",
    "S",
    "D",
    "F",
    "G",
    "H",
    "J",
    "K",
    "L",
    "SEMICOLON",
    "APOSTROPHE",
    "GRAVE",
    "LEFTSHIFT",
    "BACKSLASH",
    "Z",
    "X",
    "C",
    "V",
    "B",
    "N",
    "M",
    "COMMA",
    "DOT",
    "SLASH",
    "RIGHTSHIFT",
    "KPASTERISK",
    "LEFTALT",
    "SPACE",
    "CAPSLOCK",
    "F1",
    "F2",
    "F3",
    "F4",
    "F5",
    "F6",
    "F7",
    "F8",
    "F9",
    "F10",
    "NUMLOCK",
    "SCROLLLOCK",
    "KP7",
    "KP8",
    "KP9",
    "KPMINUS",
    "KP4",
    "KP5",
    "KP6",
    "KPPLUS",
    "KP1",
    "KP2",
    "KP3",
    "KP0",
    "KPDOT",
    "",
    "ZENKAKUHANKAKU",
    "102ND",
    "F11",
    "F12",
    "RO",
    "KATAKANA",
    "HIRAGANA",
    "HENKAN",
    "KATAKANAHIRAGANA",
    "MUHENKAN",
    "KPJPCOMMA",
    "KPENTER",
    "RIGHTCTRL",
    "KPSLASH",
    "SYSRQ",
    "RIGHTALT",
    "LINEFEED",
    "HOME",
    "UP",
    "PAGEUP",
    "LEFT",
    "RIGHT",
    "END",
    "DOWN",
    "PAGEDOWN",
    "INSERT",
    "DELETE",
    "MACRO",
    "MUTE",
    "VOLUMEDOWN",
    "VOLUMEUP",
    "POWER",
    "KPEQUAL",
    "KPPLUSMINUS",
    "PAUSE",
    "SCALE",
    "KPCOMMA",
    "HANGEUL",
    "HANGUEL",
    "HANJA",
    "YEN",
    "LEFTMETA",
    "RIGHTMETA",
    "COMPOSE",
    "STOP",
    "AGAIN",
    "PROPS",
    "UNDO",
    "FRONT",
    "COPY",
    "OPEN",
    "PASTE",
    "FIND",
    "CUT",
    "HELP",
    "MENU",
    "CALC",
    "SETUP",
    "SLEEP",
    "WAKEUP",
    "FILE",
    "SENDFILE",
    "DELETEFILE",
    "XFER",
    "PROG1",
    "PROG2",
    "WWW",
    "MSDOS",
    "COFFEE",
    "SCREENLOCK",
    "ROTATE_DISPLAY",
    "DIRECTION",
    "CYCLEWINDOWS",
    "MAIL",
    "BOOKMARKS",
    "COMPUTER",
    "BACK",
    "FORWARD",
    "CLOSECD",
    "EJECTCD",
    "EJECTCLOSECD",
    "NEXTSONG",
    "PLAYPAUSE",
    "PREVIOUSSONG",
    "STOPCD",
    "RECORD",
    "REWIND",
    "PHONE",
    "ISO",
    "CONFIG",
    "HOMEPAGE",
    "REFRESH",
    "EXIT",
    "MOVE",
    "EDIT",
    "SCROLLUP",
    "SCROLLDOWN",
    "KPLEFTPAREN",
    "KPRIGHTPAREN",
    "NEW",
    "REDO",
    "F13",
    "F14",
    "F15",
    "F16",
    "F17",
    "F18",
    "F19",
    "F20",
    "F21",
    "F22",
    "F23",
    "F24",
    "PLAYCD",
    "PAUSECD",
    "PROG3",
    "PROG4",
    "DASHBOARD",
    "SUSPEND",
    "CLOSE",
    "PLAY",
    "FASTFORWARD",
    "BASSBOOST",
    "PRINT",
    "HP",
    "CAMERA",
    "SOUND",
    "QUESTION",
    "EMAIL",
    "CHAT",
    "SEARCH",
    "CONNECT",
    "FINANCE",
    "SPORT",
    "SHOP",
    "ALTERASE",
    "CANCEL",
    "BRIGHTNESSDOWN",
    "BRIGHTNESSUP",
    "MEDIA",
    "SWITCHVIDEOMODE",
    "KBDILLUMTOGGLE",
    "KBDILLUMDOWN",
    "KBDILLUMUP",
    "SEND",
    "REPLY",
    "FORWARDMAIL",
    "SAVE",
    "DOCUMENTS",
    "BATTERY",
    "BLUETOOTH",
    "WLAN",
    "UWB",
    "UNKNOWN",
    "VIDEO_NEXT",
    "VIDEO_PREV",
    "BRIGHTNESS_CYCLE",
    "BRIGHTNESS_AUTO",
    "BRIGHTNESS_ZERO",
    "DISPLAY_OFF",
    "WWAN",
    "WIMAX",
    "RFKILL",
    "MICMUTE",
};

uint8_t keyToMidi(uint8_t key) { return key_to_midi[key]; }
uint8_t MidiToKey(uint8_t midi) { return midi_to_key[midi]; }
const char *keyName(uint8_t key) { return key_names[key]; }