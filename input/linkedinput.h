#pragma once

#include <stdint.h>
#include <type_traits>

#include "input.h"

template <void (*f)()> struct LinkedInput {
  uint16_t input;

  LinkedInput(uint16_t i) : input{i} {}

  void checkPressed(KeyboardEvents &ev) {
    if (ev.pressed(input))
      f();
  }

  void checkPressedClean(KeyboardEvents &ev) {
    if (ev.pressedClean(input))
      f();
  }
};

template <typename T, void (*f)(T &)> struct LinkedValue {
  T &val;
  uint16_t input;

  LinkedValue(uint16_t i, T &v) : val{v}, input{i} {}

  void checkPressed(KeyboardEvents &ev) {
    if (ev.pressed(input))
      f(val);
  }

  void checkPressedClean(KeyboardEvents &ev) {
    if (ev.pressedClean(input))
      f(val);
  }
};

template <typename T, void (*f)(T &, int16_t)> struct LinkedSignal {
  T &val;
  uint16_t input;

  LinkedSignal(T &v) : val{v} {}
  LinkedSignal(uint16_t i, T &v) : val{v}, input{i} {}
  LinkedSignal(LinkedSignal &cpy) : val{cpy.val}, input{cpy.input} {}

  void update(int16_t sig) { f(val, sig); }
  void check(uint16_t i, int16_t sig) {
    if (i == input)
      f(val, sig);
  }
};

namespace LinkedFuncs {
template <typename T> void sub(T &v) { --v; }
template <typename T> void add(T &v) { ++v; }
template <typename T> void zero(T &v) { v = 0; }
template <typename T, int MIN> void sub_limited(T &v) {
  if (v != MIN)
    --v;
}
template <typename T, int MAX> void add_limited(T &v) {
  if (v != MAX)
    ++v;
}
template <typename T, int MOD> void add_mod(T &v) { v = (v + 1) % MOD; }

void boolFalse(bool &b);
void boolTrue(bool &b);
void boolSwitch(bool &b);
} // namespace LinkedFuncs