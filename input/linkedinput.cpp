#include "linkedinput.h"

void LinkedFuncs::boolFalse(bool &b) { b = false; }
void LinkedFuncs::boolTrue(bool &b) { b = true; }
void LinkedFuncs::boolSwitch(bool &b) { b = !b; }