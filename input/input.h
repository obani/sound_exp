#pragma once

#include <dirent.h>
#include <fcntl.h>
#include <poll.h>
#include <stdint.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>

#include "../include/input-event-codes.h"
#include "../util/keyvector.h"
#include "../util/string.h"
#include "../util/vector.h"

#define DATA_MOUSE_TYPE 0
#define DATA_MOUSE_X 1
#define DATA_MOUSE_Y 2

#define MOUSE_LEFT 0b00000001
#define MOUSE_RIGHT 0b00000010
#define MOUSE_MIDDLE 0b00000100

#define DATA_KEY_TYPE 16
#define DATA_KEY_INPUT 18
#define DATA_KEY_PRESSED 20

#define KEY_INPUT 0b00000100
#define KEY_REPEAT 0b00000001

// joystick was tested for ps3 controller on linux
#define DATA_JOY_PRESSED 4
#define DATA_JOY_ANALOG 6
#define DATA_JOY_BUTTON 7
#define DATA_JOY_ANALOG_SIGNAL 2 // needs to use data.ss

#define NOT_ANALOG 0b00000001
#define IS_ANALOG 0b00000010

#define JOY_A 0
#define JOY_B 1
#define JOY_Y 2
#define JOY_X 3

#define JOY_LB 4
#define JOY_RB 5
#define JOY_LT 6
#define JOY_RT 7
#define JOY_SELECT 8
#define JOY_START 9
#define JOY_HOME 10

#define JOY_L3 11
#define JOY_R3 12

#define JOY_UP 13
#define JOY_DOWN 14
#define JOY_LEFT 15
#define JOY_RIGHT 16

#define JOY_AXIS_LX 0
#define JOY_AXIS_LY 1
#define JOY_AXIS_LT 2

#define JOY_AXIS_RX 3
#define JOY_AXIS_RY 4
#define JOY_AXIS_RT 5

#define JOY_AXIS_DPAD_X 6
#define JOY_AXIS_DPAD_Y 7

enum {
  INPUT_MOUSE,
  INPUT_KEYBOARD,
  INPUT_JOYSTICK,

  INPUT_TOTAL,
};

#define LINUX_INPUT_PATH "/dev/input/by-path"

struct InputData {
  struct pollfd file;
  const char *type;
  String dir;
  size_t data_size;
  union Data { // we want to use signed and unsigned sometimes
    uint8_t *u;
    int8_t *s;

    uint16_t *us;
    int16_t *ss; // no bad joke here

    double *d; // not safe
    float *f;
  } data;
  bool changed;
};

struct MouseInput {
  uint8_t btn;
  int8_t x;
  int8_t y;
};

struct KeyboardInput {
  bool pressed;
  bool released;
  uint8_t btn;
};

struct JoystickInput {
  bool analog;
  bool pressed;
  int16_t analog_signal;
  uint8_t btn;
  bool changed;
};

class KeyboardEvents : public KeyVector {
public:
  void update(const KeyboardInput &event);
};

extern InputData input[INPUT_TOTAL];
extern KeyboardEvents keyboardEvents;
extern MouseInput mouseEvents;
extern JoystickInput joystickEvents;

MouseInput getMouseInput();
KeyboardInput getKeyboardInput();
JoystickInput getJoystickInput();

bool initInputData(const char *base_dir, InputData &input,
                   const vector<const char *> &forbidden);
bool readInputData(InputData &input);
bool readKeyboardInputData();
bool readMouseInputData();
bool readJoystickInputData();

void printAllData(InputData &data);

void initKeyboardInput();
void initMouseInput();
void initJoystickInput();
void shutdownInputs();

ssize_t inputGetLong(ssize_t min, ssize_t max);
void inputGetString(char *str, size_t STR_SIZE);

uint8_t keyToMidi(uint8_t key);
uint8_t MidiToKey(uint8_t midi);
const char *keyName(uint8_t key);