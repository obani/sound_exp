#!/bin/bash

for i in $@
do
	i2=${i::-4}'.bmp' 
	magick "$i" "$i2"
done