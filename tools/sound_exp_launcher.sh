#!/bin/sh

TMP_PATH=$1
shift
PROG=$1
shift

make -C "$TMP_PATH" && ninja -C "$TMP_PATH" "$PROG" && "$TMP_PATH"/"$PROG" "$@"