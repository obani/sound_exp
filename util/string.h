#pragma once

#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include "vector.h"

constexpr const char ANSI_RESET[] = "\033[0m";

constexpr const char ANSI_WHITE[] = "\033[0;37m";
constexpr const char ANSI_GREEN[] = "\033[0;32m";
constexpr const char ANSI_BOLD_INTENSE_GREEN[] = "\033[1;92m";
constexpr const char ANSI_BOLD_INTENSE_BLUE[] = "\033[1;94m";
constexpr const char ANSI_BOLD_INTENSE_RED[] = "\033[1;91m";

constexpr const char ANSI_MOVE_UP[] = "\x1b[1A";
constexpr const char ANSI_MOVE_UP_FMT[] = "\x1b[%dA";
constexpr const char ANSI_SCROLL_UP_FMT[] = "\x1b[%dS";
constexpr const char ANSI_DELETE_LINE[] = "\x1b[2K";
constexpr const char ANSI_MOVE_UP_DELETE[] = "\x1b[1A\x1b[2K";

constexpr bool strEqual(const char *s1, const char *s2) {
  char c1 = *s1;
  char c2 = *s2;

  while (c1 == c2 && c1 != '\0') {
    c1 = *(++s1);
    c2 = *(++s2);
  }

  return c1 == c2;
}

constexpr size_t strLen(const char *s) {
  const char *end = s;
  for (; *(end++) != '\0';)
    ;
  return (size_t)(end - s);
}

bool hasSuffix(const char *str, const char *suffix);
bool hasPrefix(const char *str, const char *prefix);
bool isEmpty(const char *str);

class String {
private:
  vector<char> data;

public:
  String() : data{1} { data[0] = '\0'; }

  String(const char *s) : data(strlen(s) + 1) {
    for (size_t i = 0; i < data.size(); ++i)
      data[i] = s[i];
  }

  String(const String &s) { data = s.data; }

  String &operator=(const char *s) {
    data.clear();
    size_t len = strlen(s);
    for (size_t i = 0; i < len; i++)
      data.push_back(s[i]);
    data.push_back('\0');
    return *this;
  }

  String &operator+=(char c) {
    data[data.size() - 1] = c;
    data.push_back('\0');
    return *this;
  }

  String &operator+=(const char *s) {
    size_t len = strlen(s);
    data.set_size(data.size() - 1); // Remove '\0'
    for (size_t i = 0; i < len; i++)
      data.push_back(s[i]);
    data.push_back('\0');
    return *this;
  }

  String &operator+=(const String &s) {
    data.set_size(data.size() - 1);
    for (size_t i = 0; i < s.data.size(); i++)
      data.push_back(s.data[i]);
    return *this;
  }

  bool operator==(const char *s) const { return strEqual(str(), s); }
  bool operator==(const String &s) const { return strEqual(str(), s.str()); }

  ssize_t toInt() const { return atoll(str()); }

  float toFloat() const { return strtod(str(), NULL); }

  friend String operator+(const String &s1, const String &s2) {
    String out = s1;

    out.data.set_size(out.data.size() - 1);
    for (size_t i = 0; i < s2.data.size(); ++i)
      out.data.push_back(s2.data[i]);
    out.data.push_back('\0');

    return out;
  }

  friend String operator+(const String &s1, const char *s2) {
    String out = s1;
    size_t len = strLen(s2);

    out.data.set_size(out.data.size() - 1);
    for (size_t i = 0; i < len; i++) {
      out.data.push_back(s2[i]);
    }
    out.data.push_back('\0');

    return out;
  }

  friend String operator+(const char *s1, const String &s2) {
    String out = s1;

    out.data.set_size(out.data.size() - 1);
    for (size_t i = 0; i < s2.size(); i++)
      out.data.push_back(s2[i]);
    out.data.push_back('\0');

    return out;
  }

  char &operator[](size_t at) { return data[at]; }

  const char &operator[](size_t at) const { return data[at]; }

  char back() const { return data[data.size() - 2]; }

  size_t size() const { return data.size() - 1; }

  vector<String> split(const char *delim) {
    vector<String> out;

    if (delim == NULL) {
      out.push_back(*this);
      return out;
    }

    const char *split = strtok(str(), delim);
    if (split == NULL)
      return out;

    out.push_back(String(split));
    while ((split = strtok(NULL, delim)) != NULL) {
      out.push_back(String(split));
    }

    return out;
  }

  char *str() { return data.data(); }
  const char *str() const { return data.data(); }
};