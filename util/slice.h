#pragma once

#include <assert.h>

template <typename T> class Slice {
  T *begin_;
  T *end_;

public:
  Slice() : begin_{0}, end_{0} {}
  Slice(T *data, size_t b, size_t e) : begin_{data + b}, end_{data + e} {}

  Slice(const Slice &cpy) : begin_(cpy.begin_), end_(cpy.end_) {}
  Slice &operator=(const Slice &rhs) {
    begin_ = rhs.begin_;
    end_ = rhs.end_;
    return *this;
  }

  size_t size() { return end_ - begin_; }

  T &operator[](size_t at) {
    assert(at < (size_t)(end_ - begin_));
    return begin_[at];
  }

  const T &operator[](size_t at) const {
    assert(at < (size_t)(end_ - begin_));
    return begin_[at];
  }

  T *begin() { return begin_; }
  const T *begin() const { return begin_; }
  T *end() { return end_; }
  const T *end() const { return end_; }
};