#pragma once

#include "iterators.h"
#include <cstdint>
struct pfloat {
  static constexpr uint64_t size = 1UL << 32;

  constexpr pfloat() : v{0} {}
  constexpr pfloat(float f) : v{uint32_t(f * size)} {}
  constexpr pfloat(const pfloat &p) : v{p.v} {}

  constexpr pfloat &operator+=(const pfloat &a) {
    v += a.v;
    return *this;
  }
  constexpr pfloat &operator-=(const pfloat &a) {
    v -= a.v;
    return *this;
  }
  constexpr pfloat &operator*=(float a) {
    v *= a;
    return *this;
  }
  constexpr pfloat &operator/=(float a) {
    v /= a;
    return *this;
  }

  constexpr pfloat &operator=(const pfloat &rhs) {
    v = rhs.v;
    return *this;
  }

  constexpr bool operator==(const pfloat &rhs) const { return v == rhs.v; }
  constexpr bool operator!=(const pfloat &rhs) const { return v != rhs.v; }
  constexpr bool operator<(const pfloat &rhs) const { return v < rhs.v; }
  constexpr bool operator>(const pfloat &rhs) const { return v > rhs.v; }
  constexpr bool operator<=(const pfloat &rhs) const { return v <= rhs.v; }
  constexpr bool operator>=(const pfloat &rhs) const { return v >= rhs.v; }

  constexpr uint32_t raw() const { return v; }
  constexpr float f() const { return ((float)v) / ((float)(size)); }

  friend constexpr pfloat operator+(const pfloat &, const pfloat &);
  friend constexpr pfloat operator-(const pfloat &, const pfloat &);
  friend constexpr pfloat operator*(const pfloat &, float);
  friend constexpr pfloat operator*(float, const pfloat &);
  friend constexpr pfloat operator/(const pfloat &, float);
  friend constexpr pfloat rawPfloat(uint32_t raw);

private:
  uint32_t v;

  constexpr pfloat(uint32_t raw) : v{raw} {}
};

constexpr pfloat operator+(const pfloat &a, const pfloat &b) {
  return pfloat(a.v + b.v);
}
constexpr pfloat operator-(const pfloat &a, const pfloat &b) {
  return pfloat(a.v - b.v);
}
constexpr pfloat operator*(const pfloat &a, float b) {
  return pfloat((uint32_t)(a.v * b));
}
constexpr pfloat operator*(float a, const pfloat &b) {
  return pfloat((uint32_t)(a * b.v));
}
constexpr pfloat operator/(const pfloat &a, float b) {
  return pfloat((uint32_t)(a.v / b));
}

constexpr pfloat rawPfloat(uint32_t raw) { return pfloat(raw); }

template <uint32_t BITS> class PhaseCache {
  static constexpr size_t SIZE = 1 << BITS;

  float data[SIZE];

public:
  constexpr PhaseCache(float (*phase)(float)) {
    for (size_t i : range(SIZE))
      data[i] = phase(((float)i) / ((float)SIZE));
  }

  constexpr float operator[](pfloat at) const {
    return data[at.raw() >> (uint32_t(32) - BITS)];
  }
};