#pragma once

struct ArgcHelpOption {
  const char *opt;
  const char *altopt;
  const char *desc;
};

int HasArg(int argc, char *argv[], const char *option, bool return_next);
int HasArg(int argc, char *argv[], const char *option, const char *altoption,
           bool return_next);
void ArgcHandler(bool cond, int argc, char *argv[], const char *expected,
                 const ArgcHelpOption *options);