#include "math.h"

size_t BPMToSamples(float sample_rate, float BPM) {
  return (sample_rate * 60.f) / BPM;
}

static const PhaseCache<20> _sineCache([](float p) { return sinf(p * M_TAU); });
float Sine(pfloat f) { return _sineCache[f]; }
