#include "string.h"

bool hasSuffix(const char *str, const char *suffix) {
  if (str == NULL)
    return false;
  if (suffix == NULL)
    return true;

  return strEqual(str + strLen(str) - strLen(suffix), suffix);
}

bool hasPrefix(const char *str, const char *prefix) {
  if (str == NULL)
    return false;
  if (prefix == NULL)
    return true;

  const char *pos = strstr(str, prefix);

  return pos == str;
}

bool isEmpty(const char *s) {
  char c = *s;

  while (c != '\0' && (c == '\n' || c == '\r' || c == '\t' || c == ' '))
    c = *(++s);

  return c == '\0';
}