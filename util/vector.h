#pragma once

#include <assert.h>
#include <stdio.h>
#include <unistd.h>

#include "iterators.h"
#include <initializer_list>

constexpr size_t MIN_VEC_SIZE = 4;

template <typename T> class vector {
private:
  T *_data;
  size_t length;
  size_t real_size;

  void _update_size(size_t former_size) {
    T *tmp = _data;
    _data = new T[real_size];
    for (size_t i = 0; i < former_size; i++) {
      _data[i] = tmp[i];
    }
    delete[] tmp;
  }

public:
  vector() : length{0}, real_size{MIN_VEC_SIZE} { _data = new T[MIN_VEC_SIZE]; }
  vector(size_t size)
      : length{size}, real_size{size < MIN_VEC_SIZE ? MIN_VEC_SIZE : size} {
    _data = new T[real_size];
  }

  vector(size_t size, T val) : vector(size) {
    for (size_t i = 0; i < size; ++i)
      _data[i] = val;
  }

  vector(const vector<T> &cpy) : vector(cpy.length) {
    const T *d = cpy.data();
    for (size_t i = 0; i < length; ++i)
      _data[i] = d[i];
  }

  vector(T *elems, size_t n) : vector(n) {
    for (size_t i = 0; i < n; i++)
      _data[i] = elems[i];
  }

  explicit vector(std::initializer_list<T> init) : vector(init.size()) {
    size_t i = 0;
    for (const T &e : init)
      _data[i++] = e;
  }

  vector &operator=(const vector &rhs) {
    set_size(rhs.length);
    const T *d = rhs.data();
    for (size_t i = 0; i < length; ++i)
      _data[i] = d[i];
    return *this;
  }

  virtual ~vector() { delete[] _data; }

  void push_back(const T &elem) {
    if (real_size == length) {
      size_t former_size = real_size;
      real_size *= 1.5;
      _update_size(former_size);
    }

    _data[length] = elem;
    length++;
  }

  void set_size(size_t s) {
    length = s;
    if (real_size < length) {
      size_t former_size = real_size;
      real_size = length;
      _update_size(former_size);
    }
  }

  void set_size(size_t s, const T &cst) {
    length = s;
    if (real_size < length) {
      size_t former_size = real_size;
      real_size = length;
      _update_size(former_size);

      for (size_t i = former_size; i < real_size; i++) {
        _data[i] = cst;
      }
    }
  }

  void clear() { set_size(0); }

  void set(T val) {
    for (size_t i = 0; i < length; i++) {
      _data[i] = val;
    }
  }

  void remove_back() { length--; }

  void remove(size_t i) {
    _data[i] = _data[length - 1];
    length--;
  }

  T pop_back() {
    length--;
    return _data[length];
  }

  size_t size() const { return length; }

  T &operator[](size_t at) {
    assert(at < length);
    return _data[at];
  }

  const T &operator[](size_t at) const {
    assert(at < length);
    return _data[at];
  }

  T &first() { return _data[0]; }
  T &last() { return _data[length - 1]; }

  T *data() { return _data; }
  const T *data() const { return _data; }

  T *begin() { return data(); }
  const T *begin() const { return data(); }
  T *end() { return data() + length; }
  const T *end() const { return data() + length; }
};