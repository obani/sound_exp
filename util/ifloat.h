#pragma once

template <typename T>
concept hasNext = requires(T &t) { t.next(); };

template <typename T>
concept incrementable = requires(T &t) {
  t += t;
  t = T(0);
};

template <incrementable T> struct NextType {
private:
  T v = 0.f;

public:
  constexpr NextType(T a) : v(a) {}
  constexpr NextType(NextType &cpy) : v(cpy.v) {}
  constexpr NextType(NextType &&mv) : v(mv.v) {}
  NextType &operator=(const NextType &rhs) {
    v = rhs.v;
    return *this;
  }

  constexpr T next() const { return v; }
};

template <incrementable T> struct IType {
  static constexpr T zero = T(0);

  explicit IType(T value) noexcept : value_(value), increment_(zero) {}
  IType() noexcept : value_(zero), increment_(zero) {}

  IType &operator=(const IType &rhs) {
    value_ = rhs.value_;
    increment_ = rhs.increment_;
    return *this;
  }

  T next() noexcept { return value_ += increment_; }

  IType &set(T value, int time) noexcept {
    increment_ = (value - value_) * (1.f / T(time + 1));
    return *this;
  }

  IType &set(NextType<T> v, int time) noexcept { return set(v.next(), time); }

  // Instantly sets the value
  void jump(T value) noexcept {
    value_ = value;
    increment_ = zero;
  }

  void jump(NextType<T> value) noexcept { jump(value.next()); }

  void reset() { increment_ = zero; }

  // returns the current value
  T get() const { return value_; }
  T getIncrement() const { return increment_; }
  int size() const { return -1; }

private:
  T value_, increment_;
};

using NextFloat = NextType<float>;
using NextDouble = NextType<double>;
using IFloat = IType<float>;
using IDouble = IType<double>;