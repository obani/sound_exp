#include "args.h"
#include "string.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int HasArg(int argc, char *argv[], const char *option, bool return_next) {
  for (int i = 1; i < argc; i++)
    if (strEqual(argv[i], option))
      return return_next ? i + 1 : i;
  return 0;
}

int HasArg(int argc, char *argv[], const char *option, const char *altoption,
           bool return_next = true) {
  for (int i = 1; i < argc; i++)
    if (strEqual(argv[i], option) || strEqual(argv[i], altoption))
      return return_next ? i + 1 : i;
  return 0;
}

void ArgcHandler(bool cond, int argc, char *argv[], const char *expected,
                 const ArgcHelpOption *options) {
  if (!cond || HasArg(argc, argv, "-h", false) ||
      HasArg(argc, argv, "--help", false)) {
    if (!cond)
      printf("Wrong arguments\n");

    printf("Usage: %s %s\n\nOptions:\n", argv[0], expected);

    printf("\t-h --help: Display this message\n");
    for (int i = 0;
         options != NULL && options[i].opt != NULL && options[i].desc != NULL;
         i++) {
      if (options[i].altopt != NULL && *options[i].altopt != '\0') {
        printf("\t%s %s: %s\n", options[i].opt, options[i].altopt,
               options[i].desc);
      } else {
        printf("\t%s: %s\n", options[i].opt, options[i].desc);
      }
    }
    exit(0);
  }
}
