#pragma once

#include <stdint.h>
#include <unistd.h>

class KeyVector {
protected:
  static constexpr size_t CELL_SIZE = sizeof(uint64_t) * 8;
  static constexpr size_t NUM_BITS = 0x100;
  static constexpr size_t ARRAY_SIZE = NUM_BITS / CELL_SIZE;

  // We store the NUM_BITS possible key inputs in a bit vector of size NUM_BITS
  // / CELL_SIZE (NUM_BITS bits)
  uint64_t bit_vector[ARRAY_SIZE] = {};

public:
  KeyVector();
  KeyVector(const KeyVector &cpy);
  KeyVector operator=(const KeyVector &rhs);

  void set(uint8_t btn);
  void clean(uint8_t btn);
  void clean(uint8_t btn, bool cond);
  void clear();
  bool empty();
  bool pressed(uint8_t btn) const;
  bool pressedClean(uint8_t btn);
  uint8_t iter(uint8_t last) const;
  uint8_t riter(uint8_t last) const;
};