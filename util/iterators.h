#pragma once

#include <cstddef>
#include <type_traits>

template <typename T, size_t N> constexpr size_t LEN(const T (&arr)[N]) {
  return N;
}

template <typename T, typename M, size_t N> constexpr size_t LEN(M (T::*)[N]) {
  return N;
}

template <typename T>
concept integer = std::is_integral_v<T> || !std::is_same_v<bool, T>;

template <integer T> class RangeIterator {
  const T begin_;
  const T end_;

  struct Iterator {
    T i;

    constexpr Iterator(T v) : i(v) {}

    bool operator!=(const Iterator &it) { return i != it.i; }
    T &operator++() { return ++i; }
    T &operator*() { return i; }
  };

public:
  constexpr RangeIterator(T n) : begin_(0), end_(n) {}
  constexpr RangeIterator(T from, T to) : begin_(from), end_(to) {}

  constexpr Iterator begin() { return Iterator(begin_); }
  constexpr Iterator end() { return Iterator(end_); }
};

template <integer I> constexpr RangeIterator<I> range(I n) {
  return RangeIterator<I>(n);
}

template <integer I> constexpr RangeIterator<I> range(I from, I to) {
  return RangeIterator<I>(from, to);
}