#pragma once

#include <random>

namespace Rand {

static uint32_t defaultRandom(uint32_t state) {
  return (state * 214013 + 2531011) % 7067967631UL;
}

using F = uint32_t (*)(uint32_t);

} // namespace Rand

template <Rand::F F> struct Generator {
  static constexpr float UINT32_FACTOR = 1.f / (1UL << 32);

  constexpr Generator()
      : state([]() {
          std::random_device dev;
          return dev();
        }()) {}
  constexpr Generator(uint32_t seed) : state(seed) {}

  constexpr uint32_t U32() { return next(); }
  constexpr int Int(int max) { return U32() % max; }

  constexpr int Int(int min, int max) { return U32() % (max - min) + min; }

  constexpr bool Bool() { return U32() % 2 == 0; }

  constexpr float Float01() { return float(next()) * UINT32_FACTOR; }
  constexpr float Float11() { return Float01() * 2.f - 1.f; }
  constexpr float Float(float min, float max) {
    return Float01() * (max - min) + min;
  }

private:
  constexpr uint32_t next() { return state = F(state); }

  uint32_t state;
};

static Generator<Rand::defaultRandom> randomized;