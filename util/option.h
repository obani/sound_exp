#pragma once

template <typename T> struct Option {
  bool exists;
  T val;

  constexpr Option() : exists(false) {}
  constexpr Option(const T &x) : exists(true), val(x) {}
  constexpr Option(const Option &o) : exists(o.exists), val(o.val) {}
  constexpr Option &operator=(const T &x) {
    exists = true;
    val = x;
    return *this;
  }

  constexpr Option &operator=(const Option &o) {
    exists = o.exists;
    val = o.val;
    return *this;
  }

  constexpr bool operator==(const Option &a) {
    return exists && a.exists && val == a.val;
  }
  constexpr bool operator==(const T &a) { return exists && val == a; }

  constexpr bool operator!=(const Option &a) {
    return exists && a.exists && val != a.val;
  }
  constexpr bool operator!=(const T &a) { return exists && val != a; }

  constexpr bool operator<(const Option &a) {
    return exists && a.exists && val < a.val;
  }
  constexpr bool operator<(const T &a) { return exists && val < a; }

  constexpr bool operator<=(const Option &a) {
    return exists && a.exists && val <= a.val;
  }
  constexpr bool operator<=(const T &a) { return exists && val <= a; }

  constexpr bool operator>(const Option &a) {
    return exists && a.exists && val > a.val;
  }
  constexpr bool operator>(const T &a) { return exists && val > a; }

  constexpr bool operator>=(const Option &a) {
    return exists && a.exists && val >= a.val;
  }
  constexpr bool operator>=(const T &a) { return exists && val >= a; }
};