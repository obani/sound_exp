#pragma once

#include <math.h>
#include <stdlib.h>
#include <time.h>

#include "pfloat.h"

#include <type_traits>

template <typename T> T Min(const T &a, const T &b) { return a < b ? a : b; }
template <typename T> T Max(const T &a, const T &b) { return a > b ? a : b; }

template <typename T>
concept isSigned = std::is_same_v<T, float> || std::is_same_v<T, double> ||
                   std::is_same_v<T, char> || std::is_same_v<T, short int> ||
                   std::is_same_v<T, int> || std::is_same_v<T, long int> ||
                   std::is_same_v<T, long long int>;

template <isSigned T> T sign(const T &a) {
  constexpr T zero = T(0);
  constexpr T one = T(1);
  constexpr T minus_one = T(-1);
  return a < zero ? minus_one : a > zero ? one : zero;
}
template <typename T> T clamp(const T &min, const T &val, const T &max) {
  return val < min ? min : val > max ? max : val;
}

template <typename T> constexpr T square(const T &v) { return v * v; }
template <typename T> constexpr T cube(const T &v) { return v * v * v; }

constexpr int8_t operator""_s8(unsigned long long int n) {
  return static_cast<int8_t>(n);
}
constexpr uint8_t operator""_u8(unsigned long long int n) { return n; }

template <typename T> T distance(const T &x, const T &y) {
  T val = x * x + y * y;
  return val <= 0 ? 0 : sqrt(val);
}

constexpr float M_TAU = 2 * M_PI;

size_t BPMToSamples(float sample_rate, float BPM);
float Sine(pfloat f);