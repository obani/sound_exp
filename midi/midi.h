#pragma once

#include <jack/midiport.h>
#include <pthread.h>
#include <stdio.h>

#include <array>

#include "../audio/jackclient.h"

#include "../util/classes.h"
#include "../util/keyvector.h"
#include "../util/option.h"
#include "../util/pfloat.h"

enum {
  A0 = 21,
  As0,
  B0,
  C1,
  Cs1,
  D1,
  Ds1,
  E1,
  F1,
  Fs1,
  G1,
  Gs1,
  A1,
  As1,
  B1,
  C2,
  Cs2,
  D2,
  Ds2,
  E2,
  F2,
  Fs2,
  G2,
  Gs2,
  A2,
  As2,
  B2,
  C3,
  Cs3,
  D3,
  Ds3,
  E3,
  F3,
  Fs3,
  G3,
  Gs3,
  A3,
  As3,
  B3,
  C4,
  Cs4,
  D4,
  Ds4,
  E4,
  F4,
  Fs4,
  G4,
  Gs4,
  A4,
  As4,
  B4,
  C5,
  Cs5,
  D5,
  Ds5,
  E5,
  F5,
  Fs5,
  G5,
  Gs5,
  A5,
  As5,
  B5,
  C6,
  Cs6,
  D6,
  Ds6,
  E6,
  F6,
  Fs6,
  G6,
  Gs6,
  A6,
  As6,
  B6,
  C7,
  Cs7,
  D7,
  Ds7,
  E7,
  F7,
  Fs7,
  G7,
  Gs7,
  A7,
  As7,
  B7,
  C8,
  Cs8,
  D8,
  Ds8,
  E8,
  F8,
  Fs8,
  G8,
  Gs8,
  A8,
  As8,
  B8,
  C9,
  Cs9,
  D9,
  Ds9,
  E9,
  F9,
  Fs9,
  G9,
  Gs9,
};

struct MidiKey {
  int16_t id;
  pfloat freq;
  float velocity;
};

struct Scale {
  const char *name;
  vector<int> data;

  Scale(const char *n, const vector<int> &d) : name(n), data(d) {}
};

struct Scales {
  enum {
    Minor,
    Major,
    PentatonicMinor,
    PentatonicMajor,
    PentatonicJapanese,

    Count
  };

private:
  const std::array<Scale, Count> scales{
      Scale("Minor              ", vector<int>{2, 3, 5, 7, 8, 10}),
      Scale("Major              ", vector<int>{2, 4, 5, 7, 9, 11}),
      Scale("Pentatonic Minor   ", vector<int>{3, 5, 7, 10}),
      Scale("Pentatonic Major   ", vector<int>{2, 4, 7, 9}),
      Scale("Pentatonic Japanese", vector<int>{1, 5, 7, 8})};

public:
  const Scale &operator[](int i) const {
    assert(i < Count);
    return scales[i];
  }
};

void setSampleRate(float sample_rate);

const Scale &scaleFrom01(float pos);
int midiNoteFrom01(float pos);
int midiNoteFrom01Scale(float pos, int scale, const Scale &);
float midiFreqFrom01(float pos);
float midiFreqFrom01Floor(float pos);
float midiFreqFrom01FloorScale(float pos, int scale, const Scale &);

void initMidi(float sample_rate);
const char *getKeyName(uint8_t key, bool show_octave);
const char *getKeyNameDisplay(int b, bool show_octave);
float midiToRange(uint8_t midi, float from, float to);

namespace _Internal {
void mtxLock();
void mtxUnlock();
} // namespace _Internal

MidiKey getMidiKey(uint8_t note, float vel);
void initMidi(float sample_rate);

constexpr size_t MAX_KEYS_PRESSED = 12;

enum MidiSignal : uint8_t {
  MidiSignal_Released = 0x80,
  MidiSignal_Pressed = 0x90,
  MidiSignal_Controller = 0xB0,
  MidiSignal_Wheel = 0xE0,
  MidiSignal_SystemSignal = 0xF0,
};

constexpr uint8_t Channel_All = 255;

class MidiInput : NotCopyable {
  template <typename T, size_t SIZE> struct MidiEventData : NotCopyable {
  private:
    static constexpr T DEFAULT = T(0);
    std::array<T, SIZE> data;

  public:
    uint8_t channel, channel_mask;

    constexpr MidiEventData() {
      for (T &v : data)
        v = DEFAULT;
      setChannel(Channel_All);
    }

    T &operator[](size_t at) { return data[at]; }
    const T &operator[](size_t at) const { return data[at]; }

    void clear() { data.fill(DEFAULT); }

    void setChannel(uint8_t chan) {
      if (chan == Channel_All) {
        channel = 0;
        channel_mask = 0x00;
      } else {
        channel = chan;
        channel_mask = 0x0f;
      }
    }
  };

  enum MidiOverride {
    MidiOverride_None,

    MidiOverride_On,
    MidiOverride_Off,
  };

  JackPort &input;

  uint8_t channel, channel_mask;

public:
  KeyVector keyboard;
  KeyVector instantRelease;
  MidiEventData<float, 128> keys;
  MidiEventData<float, 128> controllers;
  MidiEventData<bool, 128> buttons;
  MidiEventData<float, 16> wheels;

  MidiInput(JackPort &in) : input{in} { keyboardChannel(Channel_All); }

  void keyboardChannel(uint8_t chan) {
    if (chan == Channel_All) {
      channel = 0;
      channel_mask = 0x00;
    } else {
      channel = chan;
      channel_mask = 0x0f;
    }
  }

  uint8_t learnIndex(MidiSignal signal) {
    jack_midi_event_t in_event;
    uint8_t output = 0;
    uint8_t index = 1;

    uint8_t chan, chan_mask;
    switch (signal) {
    case MidiSignal_Pressed:
    case MidiSignal_Released: {
      chan = channel;
      chan_mask = channel_mask;
    } break;
    case MidiSignal_Controller: {
      chan = controllers.channel;
      chan_mask = controllers.channel_mask;
    } break;
    case MidiSignal_Wheel: {
      chan = wheels.channel;
      chan_mask = wheels.channel_mask;
    } break;
    default: {
      chan = Channel_All;
      chan_mask = 0x00;
    } break;
    }

    void *port_buf = input.midi_data();
    jack_nframes_t event_count = jack_midi_get_event_count(port_buf);

    for (jack_nframes_t i = 0; i < event_count; i++) {
      jack_midi_event_get(&in_event, port_buf, i);
      uint8_t s = *(in_event.buffer) & 0xf0;

      if (s == MidiSignal_Wheel) {
        output = *(in_event.buffer) & 0x0f;
        break;
      }

      if (s == signal && (*(in_event.buffer) & chan_mask) == chan) {
        output = *(in_event.buffer + index);
        break;
      }
    }

    return output;
  }

  void pressKey(uint8_t k, float v) {
    keyboard.set(k);
    keys[k] = v;
  }

  void releaseKey(uint8_t k) {
    keyboard.clean(k);
    instantRelease.set(k);
  }

  void update() {
    constexpr float div127 = 1 / 127.f;
    constexpr float div64 = 1 / 64.f;
    jack_midi_event_t in_event;

    void *port_buf = input.midi_data();
    jack_nframes_t event_count = jack_midi_get_event_count(port_buf);

    _Internal::mtxLock();

    instantRelease.clear();
    for (jack_nframes_t i = 0; i < event_count; i++) {
      jack_midi_event_get(&in_event, port_buf, i);
      uint8_t signal = *(in_event.buffer) & 0xf0;

      if (signal == MidiSignal_Pressed &&
          (*(in_event.buffer) & channel_mask) == channel) {
        pressKey(*(in_event.buffer + 1), (*(in_event.buffer + 2)) * div127);
      } else if (signal == MidiSignal_Released &&
                 (*(in_event.buffer) & channel_mask) == channel) {
        releaseKey(*(in_event.buffer + 1));
      } else if (signal == MidiSignal_Controller &&
                 (*(in_event.buffer) & controllers.channel_mask) ==
                     controllers.channel) {
        const uint8_t idx = *(in_event.buffer + 1);
        const uint8_t v = *(in_event.buffer + 2);
        controllers[idx] = ((float)(v)) * div127;
        buttons[idx] = (bool)v;
      } else if (signal == MidiSignal_Wheel &&
                 (*(in_event.buffer) & wheels.channel_mask) == wheels.channel) {
        wheels[wheels.channel] =
            ((*(in_event.buffer + 1) & 0b1) + (*(in_event.buffer + 2) & 0x7F)) *
                div64 -
            1.f;
      }
    }

    _Internal::mtxUnlock();
  }

  bool isPressed(uint8_t key) { return keyboard.pressed(key); }

  void clear() {
    keyboard.clear();
    controllers.clear();
    wheels.clear();
  }
};

static float __NULL_ORIG = 0.f;

enum MidiLinkType { Controller, Button };

template <MidiLinkType TYPE> struct MidiLink {
private:
  size_t held_frames = 0;

protected:
  uint8_t controller = 0;
  using T = std::conditional_t<TYPE == MidiLinkType::Button, bool, float>;
  T *orig = (T *)&__NULL_ORIG;

  MidiInput &midi;

public:
  MidiLink(MidiInput &m) : midi(m) {}

  void setLink(uint8_t c) {
    controller = c;
    if constexpr (TYPE == MidiLinkType::Controller)
      orig = &midi.controllers[controller];
    else
      orig = &midi.buttons[controller];
  }

  void setLink(const Option<ssize_t> &opt) {
    if (!opt.exists)
      return;

    controller = opt.val;
    if constexpr (TYPE == MidiLinkType::Controller)
      orig = &midi.controllers[controller];
    else
      orig = &midi.buttons[controller];
  }

  uint8_t getLink() const { return controller; }
  T get() const { return *orig; }
  T getClean() {
    constexpr T zero = T(0);
    T v = *orig;
    *orig = zero;
    return v;
  }

  bool held(jack_nframes_t nframes)
    requires(TYPE == MidiLinkType::Button)
  {
    constexpr size_t FRAMES_HELD = 44100 * 2;
    if (get()) {
      held_frames += nframes;
    } else {
      held_frames = 0;
    }

    return held_frames >= FRAMES_HELD;
  }

  // This means release check if it was just released but to actually release
  // you have to check "held()"
  bool released()
    requires(TYPE == MidiLinkType::Button)
  {
    return held_frames != 0 && !get();
  }
};

using MidiController = MidiLink<MidiLinkType::Controller>;
using MidiButton = MidiLink<MidiLinkType::Button>;

struct MidiControllerChanged : MidiController {
private:
  float value = 0.f;

public:
  MidiControllerChanged(MidiInput &m) : MidiLink(m) {}

  bool changed() {
    if (value == get())
      return false;
    value = get();
    return true;
  }
};

template <bool ALLOW_LINK = true> struct MidiControllerLink : MidiController {
private:
  bool linking = false;

public:
  MidiControllerLink(MidiInput &m) : MidiLink(m) {}

  uint8_t update() {
    if constexpr (ALLOW_LINK) {
      if (!linking)
        return 0;

      uint8_t v = midi.learnIndex(MidiSignal_Controller);
      if (v != 0) {
        setLink(v);
        linking = false;
      }

      return controller;
    }

    return 0;
  }

  void link() { linking = ALLOW_LINK; }
};