#pragma once

#include "metronome.h"

class Sequence {
  int i = 0;

public:
  Metronome step;
  int num_steps = 1;

  void set(int j) { i = j; }

  int step(jack_nframes_t nframes) {
    if (step.tick(nframes)) {
      i = (i + 1) % num_steps;
      return i;
    }

    return -1;
  }
};