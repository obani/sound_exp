#pragma once

#include <jack/jack.h>

class Metronome {
  jack_nframes_t frame = 0;
  double offset = 0.0;

public:
  double length = 0.0;

  bool tick(jack_nframes_t nframes) {
    frame += nframes;

    if (frame >= length) {
      double startFrame = frame - length;
      frame = startFrame;
      offset += startFrame - frame;

      if (offset >= 1.0) {
        offset -= 1.f;
        ++frame;
      }

      return true;
    }

    return false;
  }
};