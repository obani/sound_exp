#include "midi.h"

#include "../audio/filters.h"
#include "../util/math.h"

constexpr size_t OCTAVE_SIZE = A1 - A0;

constexpr const char keyNames[][3] = {"A ", "A#", "B ", "C ", "C#", "D ",
                                      "D#", "E ", "F ", "F#", "G ", "G#"};

constexpr const char fullKeyNames[][4] = {
    "A0 ", "A#0", "B0 ", "C1 ", "C#1", "D1 ", "D#1", "E1 ", "F1 ", "F#1", "G1 ",
    "G#1", "A1 ", "A#1", "B1 ", "C2 ", "C#2", "D2 ", "D#2", "E2 ", "F2 ", "F#2",
    "G2 ", "G#2", "A2 ", "A#2", "B2 ", "C3 ", "C#3", "D3 ", "D#3", "E3 ", "F3 ",
    "F#3", "G3 ", "G#3", "A3 ", "A#3", "B3 ", "C4 ", "C#4", "D4 ", "D#4", "E4 ",
    "F4 ", "F#4", "G4 ", "G#4", "A4 ", "A#4", "B4 ", "C5 ", "C#5", "D5 ", "D#5",
    "E5 ", "F5 ", "F#5", "G5 ", "G#5", "A5 ", "A#5", "B5 ", "C6 ", "C#6", "D6 ",
    "D#6", "E6 ", "F6 ", "F#6", "G6 ", "G#6", "A6 ", "A#6", "B6 ", "C7 ", "C#7",
    "D7 ", "D#7", "E7 ", "F7 ", "F#7", "G7 ", "G#7", "A7 ", "A#7", "B7 ", "C8 ",
    "C#8", "D8 ", "D#8", "E8 ", "F8 ", "F#8", "G8 ", "G#8", "A8 ", "A#8", "B8 ",
    "C9 ", "C#9", "D9 ", "D#9", "E9 ", "F9 ", "F#9", "G9 ", "G#9"};

const char *getKeyName(uint8_t key, bool show_octave) {
  return show_octave ? key < A0 || key > Gs9 ? "" : fullKeyNames[key - A0]
                     : keyNames[(key - A0) % OCTAVE_SIZE];
}

const char *getKeyNameDisplay(int b, bool show_octave) {
  return b < A0 ? (show_octave ? "   " : "  ") : getKeyName(b, show_octave);
}

float midiToRange(uint8_t midi, float from, float to) {
  constexpr float div127 = 1.f / 127.f;
  return from + (float(midi) * div127) * to;
}

constexpr uint8_t NUM_NOTES = 128;
static pthread_mutex_t MIDI_MTX;
static float MIDI_FREQ_FLOAT[NUM_NOTES] = {};
static pfloat MIDI_FREQ[NUM_NOTES];

void _Internal::mtxLock() {
  if (pthread_mutex_lock(&MIDI_MTX))
    printf("Error on midi mutex lock\n");
}

void _Internal::mtxUnlock() {
  if (pthread_mutex_unlock(&MIDI_MTX))
    printf("Error on midi mutex unlock\n");
}

MidiKey getMidiKey(uint8_t note, float vel) {
  return {note, MIDI_FREQ[note], vel};
}

void initMidi(float sample_rate) {
  if (pthread_mutex_init(&MIDI_MTX, NULL) != 0)
    printf("Error on creating midi mutex\n");

  _Internal::mtxLock();
  for (uint8_t i = 1; i < NUM_NOTES; i++) {
    MIDI_FREQ_FLOAT[i] =
        (2.f * 440.f / 32.f) * pow(2, ((float(i) - 9.f) / 12.f)) / sample_rate;
    MIDI_FREQ[i] = pfloat(MIDI_FREQ_FLOAT[i]);
  }
  _Internal::mtxUnlock();
}

static const Scales scales;

const Scale &scaleFrom01(float pos) {
  constexpr float SCALES_MULT = (Scales::Count - 1);
  return scales[int(pos * SCALES_MULT)];
}

int midiNoteFrom01(float pos) { return pos * 127.f; }
int midiNoteFrom01Scale(float pos, int basis, const Scale &scale) {
  int note = midiNoteFrom01(pos);
  int octave = note / OCTAVE_SIZE;

  // TO FIX
  note = note % OCTAVE_SIZE;
  basis = basis % OCTAVE_SIZE;
  if (note < basis) {
    octave -= 1;
    note += OCTAVE_SIZE;
  }

  note -= basis;

  int outNote = basis + octave * OCTAVE_SIZE + OCTAVE_SIZE;
  for (int i : scale.data) {
    if (i >= note) {
      outNote = i + basis + octave * OCTAVE_SIZE;
      break;
    }
  }

  return clamp(0, outNote, 127);
}

float midiFreqFrom01(float range) {
  float pos = range * 127.f;
  int preIdx = pos;
  int postIdx = preIdx + 1;

  return crossfade(MIDI_FREQ_FLOAT[preIdx], MIDI_FREQ_FLOAT[postIdx],
                   (pos - floor(pos)));
}

float midiFreqFrom01Floor(float pos) {
  return MIDI_FREQ_FLOAT[midiNoteFrom01(pos)];
}

float midiFreqFrom01FloorScale(float pos, int basis, const Scale &scale) {
  return MIDI_FREQ_FLOAT[midiNoteFrom01Scale(pos, basis, scale)];
}